//
//  TodayViewController.swift
//  FMWidget
//
//  Created by vikseriq on 20.09.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit
import NotificationCenter

class TodayViewController: UIViewController, NCWidgetProviding {
        
    @IBOutlet weak var buttonRun: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buttonRun.layer.cornerRadius = 4
    }
    
    @IBAction func fireRun(sender: AnyObject) {
        if let url = NSURL(string: "fatigue://?run"){
            extensionContext?.openURL(url, completionHandler: nil)
        }
    }
    
    func widgetMarginInsetsForProposedMarginInsets(defaultMarginInsets: UIEdgeInsets) -> UIEdgeInsets {
        return UIEdgeInsetsZero
    }
    
    func widgetPerformUpdateWithCompletionHandler(completionHandler: ((NCUpdateResult) -> Void)) {
        completionHandler(NCUpdateResult.NoData)
    }
    
}
