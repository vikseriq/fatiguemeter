//
//  FMLinearSegue.swift
//  FatigueMeter
//
//  Created by vikseriq on 24.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class FMLinearSegue: UIStoryboardSegue {
   
    override func perform() {
        self.sourceViewController.presentViewController(self.destinationViewController, animated: false, completion: nil)
    }
    
}
