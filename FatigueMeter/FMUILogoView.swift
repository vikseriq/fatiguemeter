//
//  FMUILogoView.swift
//  FatigueMeter
//
//  Created by vikseriq on 15.15.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class FMUILogoView: UIView {

    var timer: NSTimer!
    var angle: Double = 0
    let angleMin: Double = -43
    let angleMax: Double = 43
    var angleStep: Double = 1
    var totalSteps: Double = 1
    var percentage: Double = 0
    var percentageStep: Double = 1
    
    var bezierSpinPath, bezierLetterPath, bezierArchPath, bezierGroup: UIBezierPath!
    var bezierFrame: CGRect!
    
    let whiteColor = UIColor.whiteColor()
    let greenColor = UIColor(rgba: "#6EB089")
    let lightGreenColor = UIColor(rgba: "#B8D8C5")
    var gradient: CGGradient!
    var gradientCenter: CGPoint!
    
    override func didMoveToSuperview() {
        let colors: CFArray = [greenColor.CGColor, whiteColor.CGColor]
        gradient = CGGradientCreateWithColors(CGColorSpaceCreateDeviceRGB(), colors, nil)
        gradientCenter = CGPointMake(self.frame.width / 2, self.frame.height / 2)
        setPosition(0.33)
        initPathes(frame: self.frame)
    }
    
    func setPosition(percentage: Double){
        self.percentage = percentage
        angle = (angleMax - angleMin) * self.percentage + angleMin
    }
    
    func runAnimation(duration: Double){
        totalSteps = (angleMax - angleMin) / abs(angleStep)
        percentageStep = 1 / totalSteps
        percentage = (angle - angleMin) / (angleMax - angleMin)
        let interval: NSTimeInterval = duration / totalSteps
        timer = NSTimer(timeInterval: interval, target: self, selector: Selector("tick"), userInfo: nil, repeats: true)
        NSRunLoop.mainRunLoop().addTimer(timer, forMode: NSRunLoopCommonModes)
    }
    
    func tick(){
        angle += angleStep
        percentage += percentageStep
        self.setNeedsDisplay()
        if angle >= angleMax || angle <= angleMin {
            timer.invalidate()
            timer = nil
            angleStep *= -1
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if !userInteractionEnabled {
            return
        }
        if timer == nil {
            runAnimation(0.750)
        }
    }
    
    override func drawRect(rect: CGRect) {
        drawLogo(frame: rect, spinAngle: angle)
    }
  
    func drawLogo(frame frame: CGRect, spinAngle: Double) {
        let context = UIGraphicsGetCurrentContext()
        
        if bezierFrame != frame {
            initPathes(frame: frame)
        }
        
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, frame.minX + 0.50254 * frame.width, frame.minY + 0.36867 * frame.height)
        CGContextRotateCTM(context, CGFloat(-spinAngle * M_PI / 180))
        lightGreenColor.setFill()
        bezierSpinPath.fill()
        CGContextRestoreGState(context)
        
        whiteColor.setFill()
        bezierArchPath.fill()
        
        whiteColor.setFill()
        bezierLetterPath.fill()
        
        CGContextSaveGState(context)
        bezierGroup.addClip()
        let radius = CGFloat(1 - abs(percentage)) * self.frame.width * 1.1
        CGContextDrawRadialGradient(UIGraphicsGetCurrentContext(), gradient, gradientCenter, 0, gradientCenter, radius, CGGradientDrawingOptions.DrawsBeforeStartLocation)
        CGContextRestoreGState(context)
    }
    
    func initPathes(frame frame: CGRect){
        bezierFrame = frame
        
        // path for spin
        bezierSpinPath = UIBezierPath()
        bezierSpinPath.moveToPoint(CGPointMake(-0, 51.84))
        bezierSpinPath.addLineToPoint(CGPointMake(12.47, 70.84))
        bezierSpinPath.addLineToPoint(CGPointMake(-12.48, 70.84))
        bezierSpinPath.addLineToPoint(CGPointMake(-0, 51.84))
        bezierSpinPath.closePath()
        bezierSpinPath.miterLimit = 4;
        
        // path for arch
        bezierArchPath = UIBezierPath()
        bezierArchPath.moveToPoint(CGPointMake(frame.minX + 0.95174 * frame.width, frame.minY + 0.70849 * frame.height))
        bezierArchPath.addCurveToPoint(CGPointMake(frame.minX + 0.89474 * frame.width, frame.minY + 0.65136 * frame.height), controlPoint1: CGPointMake(frame.minX + 0.95174 * frame.width, frame.minY + 0.67694 * frame.height), controlPoint2: CGPointMake(frame.minX + 0.92622 * frame.width, frame.minY + 0.65136 * frame.height))
        bezierArchPath.addCurveToPoint(CGPointMake(frame.minX + 0.84794 * frame.width, frame.minY + 0.67590 * frame.height), controlPoint1: CGPointMake(frame.minX + 0.87535 * frame.width, frame.minY + 0.65136 * frame.height), controlPoint2: CGPointMake(frame.minX + 0.85824 * frame.width, frame.minY + 0.66107 * frame.height))
        bezierArchPath.addLineToPoint(CGPointMake(frame.minX + 0.84748 * frame.width, frame.minY + 0.67551 * frame.height))
        bezierArchPath.addCurveToPoint(CGPointMake(frame.minX + 0.49539 * frame.width, frame.minY + 0.84207 * frame.height), controlPoint1: CGPointMake(frame.minX + 0.76384 * frame.width, frame.minY + 0.77722 * frame.height), controlPoint2: CGPointMake(frame.minX + 0.63717 * frame.width, frame.minY + 0.84207 * frame.height))
        bezierArchPath.addCurveToPoint(CGPointMake(frame.minX + 0.14319 * frame.width, frame.minY + 0.67530 * frame.height), controlPoint1: CGPointMake(frame.minX + 0.35353 * frame.width, frame.minY + 0.84207 * frame.height), controlPoint2: CGPointMake(frame.minX + 0.22682 * frame.width, frame.minY + 0.77712 * frame.height))
        bezierArchPath.addCurveToPoint(CGPointMake(frame.minX + 0.09681 * frame.width, frame.minY + 0.65136 * frame.height), controlPoint1: CGPointMake(frame.minX + 0.13285 * frame.width, frame.minY + 0.66082 * frame.height), controlPoint2: CGPointMake(frame.minX + 0.11594 * frame.width, frame.minY + 0.65136 * frame.height))
        bezierArchPath.addCurveToPoint(CGPointMake(frame.minX + 0.04696 * frame.width, frame.minY + 0.70849 * frame.height), controlPoint1: CGPointMake(frame.minX + 0.06533 * frame.width, frame.minY + 0.65136 * frame.height), controlPoint2: CGPointMake(frame.minX + 0.04696 * frame.width, frame.minY + 0.67694 * frame.height))
        bezierArchPath.addCurveToPoint(CGPointMake(frame.minX + 0.05019 * frame.width, frame.minY + 0.74134 * frame.height), controlPoint1: CGPointMake(frame.minX + 0.04696 * frame.width, frame.minY + 0.72072 * frame.height), controlPoint2: CGPointMake(frame.minX + 0.04366 * frame.width, frame.minY + 0.73204 * frame.height))
        bezierArchPath.addLineToPoint(CGPointMake(frame.minX + 0.05009 * frame.width, frame.minY + 0.74142 * frame.height))
        bezierArchPath.addCurveToPoint(CGPointMake(frame.minX + 0.05231 * frame.width, frame.minY + 0.74417 * frame.height), controlPoint1: CGPointMake(frame.minX + 0.05083 * frame.width, frame.minY + 0.74234 * frame.height), controlPoint2: CGPointMake(frame.minX + 0.05157 * frame.width, frame.minY + 0.74325 * frame.height))
        bezierArchPath.addCurveToPoint(CGPointMake(frame.minX + 0.05309 * frame.width, frame.minY + 0.74513 * frame.height), controlPoint1: CGPointMake(frame.minX + 0.05257 * frame.width, frame.minY + 0.74449 * frame.height), controlPoint2: CGPointMake(frame.minX + 0.05283 * frame.width, frame.minY + 0.74481 * frame.height))
        bezierArchPath.addCurveToPoint(CGPointMake(frame.minX + 0.49528 * frame.width, frame.minY + 0.95597 * frame.height), controlPoint1: CGPointMake(frame.minX + 0.15759 * frame.width, frame.minY + 0.87378 * frame.height), controlPoint2: CGPointMake(frame.minX + 0.31685 * frame.width, frame.minY + 0.95597 * frame.height))
        bezierArchPath.addCurveToPoint(CGPointMake(frame.minX + 0.93216 * frame.width, frame.minY + 0.75155 * frame.height), controlPoint1: CGPointMake(frame.minX + 0.67071 * frame.width, frame.minY + 0.95597 * frame.height), controlPoint2: CGPointMake(frame.minX + 0.82762 * frame.width, frame.minY + 0.87651 * frame.height))
        bezierArchPath.addCurveToPoint(CGPointMake(frame.minX + 0.95174 * frame.width, frame.minY + 0.70849 * frame.height), controlPoint1: CGPointMake(frame.minX + 0.94415 * frame.width, frame.minY + 0.74108 * frame.height), controlPoint2: CGPointMake(frame.minX + 0.95174 * frame.width, frame.minY + 0.72568 * frame.height))
        bezierArchPath.closePath()
        bezierArchPath.miterLimit = 4;
        
        // path for letter
        bezierLetterPath = UIBezierPath()
        bezierLetterPath.moveToPoint(CGPointMake(frame.minX + 0.35833 * frame.width, frame.minY + 0.04156 * frame.height))
        bezierLetterPath.addCurveToPoint(CGPointMake(frame.minX + 0.30157 * frame.width, frame.minY + 0.09721 * frame.height), controlPoint1: CGPointMake(frame.minX + 0.32660 * frame.width, frame.minY + 0.04156 * frame.height), controlPoint2: CGPointMake(frame.minX + 0.30157 * frame.width, frame.minY + 0.06620 * frame.height))
        bezierLetterPath.addLineToPoint(CGPointMake(frame.minX + 0.30157 * frame.width, frame.minY + 0.55811 * frame.height))
        bezierLetterPath.addCurveToPoint(CGPointMake(frame.minX + 0.35833 * frame.width, frame.minY + 0.61375 * frame.height), controlPoint1: CGPointMake(frame.minX + 0.30157 * frame.width, frame.minY + 0.58911 * frame.height), controlPoint2: CGPointMake(frame.minX + 0.32660 * frame.width, frame.minY + 0.61375 * frame.height))
        bezierLetterPath.addCurveToPoint(CGPointMake(frame.minX + 0.41544 * frame.width, frame.minY + 0.55811 * frame.height), controlPoint1: CGPointMake(frame.minX + 0.38847 * frame.width, frame.minY + 0.61375 * frame.height), controlPoint2: CGPointMake(frame.minX + 0.41544 * frame.width, frame.minY + 0.58911 * frame.height))
        bezierLetterPath.addLineToPoint(CGPointMake(frame.minX + 0.41544 * frame.width, frame.minY + 0.40788 * frame.height))
        bezierLetterPath.addLineToPoint(CGPointMake(frame.minX + 0.61715 * frame.width, frame.minY + 0.40788 * frame.height))
        bezierLetterPath.addCurveToPoint(CGPointMake(frame.minX + 0.66633 * frame.width, frame.minY + 0.35037 * frame.height), controlPoint1: CGPointMake(frame.minX + 0.64412 * frame.width, frame.minY + 0.40788 * frame.height), controlPoint2: CGPointMake(frame.minX + 0.66633 * frame.width, frame.minY + 0.37820 * frame.height))
        bezierLetterPath.addCurveToPoint(CGPointMake(frame.minX + 0.61715 * frame.width, frame.minY + 0.29311 * frame.height), controlPoint1: CGPointMake(frame.minX + 0.66633 * frame.width, frame.minY + 0.32255 * frame.height), controlPoint2: CGPointMake(frame.minX + 0.64412 * frame.width, frame.minY + 0.29311 * frame.height))
        bezierLetterPath.addLineToPoint(CGPointMake(frame.minX + 0.41544 * frame.width, frame.minY + 0.29311 * frame.height))
        bezierLetterPath.addLineToPoint(CGPointMake(frame.minX + 0.41544 * frame.width, frame.minY + 0.15681 * frame.height))
        bezierLetterPath.addLineToPoint(CGPointMake(frame.minX + 0.64786 * frame.width, frame.minY + 0.15681 * frame.height))
        bezierLetterPath.addCurveToPoint(CGPointMake(frame.minX + 0.70061 * frame.width, frame.minY + 0.09918 * frame.height), controlPoint1: CGPointMake(frame.minX + 0.67483 * frame.width, frame.minY + 0.15681 * frame.height), controlPoint2: CGPointMake(frame.minX + 0.70061 * frame.width, frame.minY + 0.12701 * frame.height))
        bezierLetterPath.addCurveToPoint(CGPointMake(frame.minX + 0.64786 * frame.width, frame.minY + 0.04156 * frame.height), controlPoint1: CGPointMake(frame.minX + 0.70061 * frame.width, frame.minY + 0.07136 * frame.height), controlPoint2: CGPointMake(frame.minX + 0.67483 * frame.width, frame.minY + 0.04156 * frame.height))
        bezierLetterPath.addLineToPoint(CGPointMake(frame.minX + 0.35833 * frame.width, frame.minY + 0.04156 * frame.height))
        bezierLetterPath.closePath()
        bezierLetterPath.miterLimit = 4;
        
        // join pathes for gradient clip
        bezierGroup = bezierLetterPath.copy() as! UIBezierPath
        bezierGroup.appendPath(bezierArchPath)
    }


}
