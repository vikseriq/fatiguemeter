//
//  FMSettings.swift
//  FatigueMeter
//
//  Created by vikseriq on 30.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class FMSettings {
    
    static var pushOn: Bool {
        get { return FMSettings.getBool("pushEnabled") }
        set { FMSettings.setBool(newValue, forKey: "pushEnabled") }
    }
    
    static var previewOn: Bool {
        get { return FMSettings.getBool("previewEnabled") }
        set { FMSettings.setBool(newValue, forKey: "previewEnabled") }
    }
    
    static var pushSuggested: Bool {
        get { return FMSettings.getBool("pushSuggested") }
        set { FMSettings.setBool(newValue, forKey: "pushSuggested") }
    }
    
    static var skipIntro: Bool {
        get { return FMSettings.getBool("skipIntro") }
        set { FMSettings.setBool(newValue, forKey: "skipIntro") }
    }
    
    static var alertCount: Int {
        get { return FMSettings.getInt("alertCount") }
        set { FMSettings.setInt(newValue, forKey: "alertCount") }
    }
    
    static var alertTimeFrom: Int {
        get { return FMSettings.getInt("alertTimeFrom") }
        set { FMSettings.setInt(newValue, forKey: "alertTimeFrom") }
    }
    
    static var alertTimeTo: Int {
        get { return FMSettings.getInt("alertTimeTo") }
        set { FMSettings.setInt(newValue, forKey: "alertTimeTo") }
    }
    
    static var alertWeekdays: [Int] {
        get {
            var result: [Int] = []
            let mask = FMSettings.getInt("alertWeekdays")
            for i in 0..<7 {
                if (mask & 1 << i) >> i == 1 {
                    result.append(i)
                }
            }
            return result
        }
        set {
            var mask: Int = 0
            for i in newValue {
                mask = mask | 1 << i
            }
            FMSettings.setInt(mask, forKey: "alertWeekdays")
        }
    }
    
    static var soundMode: SoundMode {
        get { return SoundMode(rawValue: FMSettings.getInt("soundMode")) ?? SoundMode.Enabled }
        set { FMSettings.setInt(newValue.rawValue, forKey: "soundMode") }
    }
    
    enum SoundMode: Int {
        case Enabled = 0, Vibro, Silent
        
        func short() -> String {
            let codes = ["On", "Vibro", "Off"]
            return codes[self.rawValue] ?? "-"
        }
    }
    
    static var units: MeasureUnits {
        get {
            if let units = FMSettings.getObject("units") as? Int {
                return MeasureUnits(rawValue: units) ?? MeasureUnits.Imperial
            } else {
                // Fetching actual device's units system
                let isMetric = NSLocale.currentLocale().objectForKey(NSLocaleUsesMetricSystem) as? Bool ?? false
                let _units = isMetric ? MeasureUnits.Metric : MeasureUnits.Imperial
                FMSettings.units = _units
                return _units
            }
        }
        set { FMSettings.setInt(newValue.rawValue, forKey: "units") }
    }
    
    enum MeasureUnits: Int {
        case Imperial = 0, Metric
        
        func short() -> String {
            let codes = ["Imperial", "Metric"]
            return codes[self.rawValue] ?? "-"
        }
    }
    
    static var recommendLastCount: Int {
        get { return FMSettings.getInt("recommendLastCount") }
        set { FMSettings.setInt(newValue, forKey: "recommendLastCount") }
    }
    
    static var trialsTotal: Int {
        get { return FMSettings.getInt("trialsTotal") }
        set { FMSettings.setInt(newValue, forKey: "trialsTotal") }
    }
    
    static func incTrials() -> Int {
        let i = trialsTotal + 1
        trialsTotal = i
        return i
    }
    
    static func setBool(value: Bool, forKey: String){
        let settings = NSUserDefaults.standardUserDefaults()
        settings.setBool(value, forKey: forKey)
        settings.synchronize()
    }
    
    static func getBool(key: String) -> Bool {
        let settings = NSUserDefaults.standardUserDefaults()
        return settings.boolForKey(key)
    }
    
    static func setInt(value: Int, forKey: String){
        let settings = NSUserDefaults.standardUserDefaults()
        settings.setInteger(value, forKey: forKey)
        settings.synchronize()
    }
    
    static func getInt(key: String) -> Int {
        let settings = NSUserDefaults.standardUserDefaults()
        return settings.integerForKey(key)
    }
    
    static func getObject(key: String) -> AnyObject? {
        let settings = NSUserDefaults.standardUserDefaults()
        return settings.objectForKey(key)
    }
    
    
}
