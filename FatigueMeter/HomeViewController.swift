//
//  HomeViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 24.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UIAlertViewDelegate, FMStateBackgroundDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBOutlet weak var briefLast: FMUIBriefButton!
    @IBOutlet weak var briefTimer: FMUIBriefButton!
    @IBOutlet weak var chart: FMUIGlassyChartView!
    @IBOutlet weak var recommend: FMUIBadgedButton!
    
    let kAlertReminder = 0x010
    let kAlertOneMoreRun = 0x020
    let formatDate = "MMMdd"
    static let flagTestFinished: Int = 0x1001
    var navigationControllerRef: UINavigationController!
    var latestTrial: TrialEventManaged!
    var today: NSDate!
    var chartDate: NSDate?
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        let prevState = FMStateful.now()
        FMStateful.setState(.Home)
        
        if prevState == .Load2Home {
            // from loading - if it's first time, go to profile setup and first trial
            let profile = ProfileModel()
            if profile.lastUpdate.timeIntervalSince1970 == 0 {
                MenuViewController.unwrap(self)?.performStateChange(.ProfileSetup)
                return
            }
        }
        
        navigationItem.title = "Home"
        NaviViewController.showMenu(self)
        navigationControllerRef = self.navigationController
        FMLocalNotification.resetBadge()
        if FMLocalNotification.launchedFromNotification {
            FMLocalNotification.launchedFromNotification = false
            Flurry.logEvent("launchedFromNotification")
        }
        today = NSDate()
        
        updateScheduler()
        
        let totalEvents = TrialEvent.fetchTotalEvents()
        
        var res = TrialEvent.fetchLastActualDay()
        if totalEvents >= 1 {
            latestTrial = res[0]
            let format = RecommendModel.getByFatigue(latestTrial.valueForKey("fatigue") as! Double)
            briefLast.setTitle(format.title, color: format.color)
            briefLast.briefDate = (latestTrial.valueForKey("date") as! NSDate).asString(format: formatDate)
            RecommendModel.updateRecommendLastCount(TrialEvent(managed: latestTrial))
        } else {
            briefLast.setTitle("One more run", color: UIColor.whiteColor())
            briefLast.briefDate = "to get results"
            recommend.hidden = true
        }
        recommend.badgeNumber = -1 // force chevron
        
        var chartPoints = TrialEvent.fetchLatest(count: 4)
        if chartPoints.last?.date.midnight() == today.midnight(){
            chartPoints = TrialEvent.fetchDay(today)
        }
        chart.setupView(FMUtility.eventsToPoints(chartPoints), toDate: today.midnight())
        chart.headlineTitle = "Your progress"
        if totalEvents >= 1 {
            chartDate = chartPoints[0].valueForKey("date") as? NSDate
            chart.headlineDetail = chartDate!.asString(.ShortStyle, timeStyle: .NoStyle)
            chart.onHeadlineTouchAction(self, action: Selector("goCalendar"))
            chart.onChartTouchAction(self, action: Selector("goFromChart"))
        } else {
            chart.headlineDetail = "No data"
            chart.onChartTouchAction(self, action: Selector("goTrial"))
        }
        
        
        if totalEvents > 1 {
            if let flag = FMStateful.getData() as? Int {
                if flag == HomeViewController.flagTestFinished {
                    FMStateful.setData(nil)
                    if !AccountModel.isActive() && totalEvents % 4 - 3 == 0 {
                        // right after each fourth test staring from third - suggest login/register if not done
                        MenuViewController.unwrap(self)?.performStateChange(.AccountLogin);
                    } else if !FMSettings.pushSuggested && totalEvents > 1 && totalEvents % 2 == 0 {
                        // otherwise suggest setup notifications after each second
                        suggestSetupNotifications()
                    }
                }
            }
        }
        
        FMStateful.machine.backgroundDelegate = self
    }
    
    func stateBackgroundChanged(inBackground: Bool) {
        if !inBackground {
            updateScheduler()
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        if navigationControllerRef != nil {
            navigationControllerRef = nil
        }
        FMStateful.machine.backgroundDelegate = nil
    }
    
    func updateScheduler() {
        let alertDate = FMLocalNotification.getNextFireDate()
        if alertDate.timeIntervalSince1970 > NSDate().timeIntervalSince1970 {
            briefTimer.briefDate = alertDate.asString(format: formatDate)
            briefTimer.setTitle(alertDate.asString(.NoStyle, timeStyle: .ShortStyle) , color: UIColor.whiteColor())
        } else {
            briefTimer.setTitle("Not scheduled", color: UIColor.whiteColor())
            briefTimer.briefDate = "No reminders"
        }
        briefTimer.setNeedsDisplay()
    }
    
    func suggestSetupNotifications(){
        let title = "Schedule fatigue metering"
        let text = "Setup notifications and don't miss next test. Performing tests on a regular basis give more precise results"
        let buttonOk = "Ok"
        let buttonDismiss = "Later"
        
        if #available(iOS 8.0, *) {
            let alertController = UIAlertController(title: title, message: text, preferredStyle: .Alert)
            alertController.addAction(UIAlertAction(title: buttonOk, style: .Default, handler: { action in
                Flurry.logEvent("showSettings", withParameters: ["source": "reminder"])
                MenuViewController.unwrap(self)?.performStateChange(.Settings)
            }))
            alertController.addAction(UIAlertAction(title: buttonDismiss, style: .Cancel, handler: { action in   }))
            self.presentViewController(alertController, animated: true, completion: nil)
        } else {
            let alertView = UIAlertView(title: title, message: text, delegate: self, cancelButtonTitle: buttonDismiss, otherButtonTitles: buttonOk)
            alertView.alertViewStyle = .Default
            alertView.tag = kAlertReminder
            alertView.show()
        }
    }
   
    func askToRunAgain(){
        let title = "One more run"
        let text = "At least you have to do the test two times to display the result"
        let buttonOk = "Run test"
        let buttonDismiss = "Dismiss"
        
        if #available(iOS 8.0, *) {
            let alertController = UIAlertController(title: title, message: text, preferredStyle: .Alert)
            alertController.addAction(UIAlertAction(title: buttonOk, style: .Default, handler: { action in
                Flurry.logEvent("showTrial", withParameters: ["source": "alertFirst"])
                MenuViewController.unwrap(self)?.performStateChange(.TrialGo)
            }))
            alertController.addAction(UIAlertAction(title: buttonDismiss, style: .Cancel, handler: { action in   }))
            self.presentViewController(alertController, animated: true, completion: nil)
        } else {
            let alertView = UIAlertView(title: title, message: text, delegate: self, cancelButtonTitle: buttonDismiss, otherButtonTitles: buttonOk)
            alertView.alertViewStyle = .Default
            alertView.tag = kAlertOneMoreRun
            alertView.show()
        }
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 1 {
            if alertView.tag == kAlertReminder {
                Flurry.logEvent("showSettings", withParameters: ["source": "reminder"])
                MenuViewController.unwrap(self)?.performStateChange(.Settings)
            } else if alertView.tag == kAlertOneMoreRun {
                Flurry.logEvent("showTrial", withParameters: ["source": "alertFirst"])
                MenuViewController.unwrap(self)?.performStateChange(.TrialGo)
            }
        }
    }
    
    func goFromChart(){
        if let date = chartDate {
            Flurry.logEvent("showDay", withParameters: ["source": "homeChart"])
            FMStateful.setData(date)
            FMStateful.setState(.Home2Detail)
            MenuViewController.unwrap(self)?.performStateChange(.ProgressDay)
        }
    }
    
    func goCalendar(){
        if latestTrial != nil {
            FMStateful.setData(latestTrial.valueForKey("date") as! NSDate)
            FMStateful.setState(.Home2Detail)
            MenuViewController.unwrap(self)?.performStateChange(.Calendar)
        }
    }
    
    func goTrial(){
        Flurry.logEvent("showTrial", withParameters: ["source": "homeFirst"])
        MenuViewController.unwrap(self)?.performStateChange(.TrialGo)
    }
    
    @IBAction func btnRun(sender: AnyObject) {
        Flurry.logEvent("showTrial", withParameters: ["source": "home"])
        MenuViewController.unwrap(self)?.performStateChange(.TrialGo)
    }
    
    @IBAction func fireLast(sender: AnyObject) {
        if latestTrial != nil {
            Flurry.logEvent("showReport", withParameters: ["source": "home"])
            FMStateful.setData(latestTrial)
            FMStateful.setState(.Home2Detail)
            MenuViewController.unwrap(self)?.performStateChange(.ProgressDetail)
        } else {
            goTrial()
        }
    }
    
    @IBAction func fireTimer(sender: AnyObject) {
        Flurry.logEvent("showSettings", withParameters: ["source": "home"])
        FMStateful.setState(.Home2Detail)
        MenuViewController.unwrap(self)?.performStateChange(.Settings)
    }
    
    @IBAction func btnRecommend(sender: FMUIBadgedButton) {
        if latestTrial != nil {
            FMStateful.setData(TrialEvent(managed: latestTrial))
            FMStateful.setState(.Home2Detail)
            Flurry.logEvent("showRecommend", withParameters: ["source": "homeButton"])
            MenuViewController.unwrap(self)?.performStateChange(.RecommendLast)
        }
    }
    
}
