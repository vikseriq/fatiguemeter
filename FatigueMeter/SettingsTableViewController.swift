//
//  SettingsTableViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 24.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController, UIPickerViewDelegate, UIPickerViewDataSource, FMUIPickerStateDelegate, UIActionSheetDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        picker = FMUIPickerView()
        picker.delegate = self
        picker.stateDelegate = self
        picker.assignToTextField(textHid)
        
        pickerValues = Dictionary(minimumCapacity: 3)
        
        var numbers: [String] = []
        for i in 1...8 {
            numbers.append("\(i)")
        }
        pickerValues[PickerType.Number] = [numbers]
        formatter.timeStyle = NSDateFormatterStyle.ShortStyle
        
        var times: [[String]] = [[], ["-"], []]
        let halfs = Int(24 * 60 * 60 / timeSteps)
        for j in 0..<halfs {
            let s = formatter.stringFromDate(NSDate(timeIntervalSince1970: Double(j * timeSteps - tzOffset)))
            times[0].append(s)
            times[2].append(s)
        }
        pickerValues[PickerType.TimeInterval] = times
    }
    
    @IBOutlet var gradientTableView: FMUIGradientTableView!
    @IBOutlet weak var switchPreview: UISwitch!
    @IBOutlet weak var switchPushes: UISwitch!
    @IBOutlet weak var textHid: UITextField!
    @IBOutlet weak var labelPushNumber: UILabel!
    @IBOutlet weak var labelPushDays: UILabel!
    @IBOutlet weak var labelPushTime: UILabel!
    @IBOutlet weak var labelSounds: UILabel!
    @IBOutlet weak var labelUnits: UILabel!
    @IBOutlet weak var labelAccountName: UILabel!
    @IBOutlet weak var viewResetOption: UIControl!
    
    var picker: FMUIPickerView!
    var pickerType: PickerType = .None
    var pickerValues: [PickerType: [[String]]]!
    var selectedIndexes: [Int]!
    var vsType: ValueSelectType!
    let timeSteps = 15 * 60
    let formatter = NSDateFormatter()
    let tzOffset = NSTimeZone.systemTimeZone().secondsFromGMT
    let weekdays = ["Every Sunday", "Every Monday", "Every Tuesday", "Every Wednesday", "Every Thursday", "Every Friday", "Every Saturday"]
    let weekdaysShort = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
    let soundTitles = ["Enable sounds", "Vibro only", "Silent mode"]
    let unitsTitles = ["Imperial", "Metric"]
    var pushChanged = false
    var initPreview = false
    var keepPushSettings = false
    let colorSemiTransparent = UIColor(rgba: "#ffffff20")
    
    override func viewWillAppear(animated: Bool) {
        FMStateful.setState(.Settings)
        
        // process settings save
        if vsType != nil {
            switch vsType! {
            case .Days:
                if let si = selectedIndexes {
                    FMSettings.alertWeekdays = si
                    pushChanged = true
                    keepPushSettings = true
                }
            case .Sound:
                if let si = selectedIndexes {
                    FMSettings.soundMode = FMSettings.SoundMode(rawValue: si[0]) ?? FMSettings.SoundMode.Enabled
                    Flurry.logEvent("settingsSound", withParameters: ["value": FMSettings.soundMode.short()])
                }
            case .Units:
                if let si = selectedIndexes {
                    FMSettings.units = FMSettings.MeasureUnits(rawValue: si[0]) ?? FMSettings.MeasureUnits.Imperial
                    Flurry.logEvent("settingsUnits", withParameters: ["value": FMSettings.units.short()])
                }
            }
        }
        
        // synch notifications state with real situation
        let pushScheduled = FMLocalNotification.getNextFireDate().timeIntervalSince1970 != 0
        if FMSettings.pushOn != pushScheduled && !keepPushSettings {
            FMSettings.pushOn = pushScheduled
        }
        
        if keepPushSettings {
            pushChanged = true
            keepPushSettings = false
        }
        
        // setup values
        switchPushes.on = FMSettings.pushOn
        switchPreview.on = FMSettings.previewOn
        initPreview = FMSettings.previewOn
        
        updatePushLabels()
        
        let days_int = FMSettings.alertWeekdays
        var days: [String] = []
        for i in days_int {
            days.append(weekdaysShort[i])
        }
        if days.count == 0 {
            days = ["No repeats"]
        } else if days.count == 7 {
            days = ["Every day"]
        }
        labelPushDays.text = days.joinWithSeparator(", ")
        
        labelSounds.text = soundTitles[FMSettings.soundMode.rawValue]
        labelUnits.text = unitsTitles[FMSettings.units.rawValue]
        
        if AccountModel.isActive() {
            labelAccountName.text = AccountModel.sharedInstance.login
        } else {
            labelAccountName.text = "Sign in"
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        FMSettings.pushOn = switchPushes.on
        FMSettings.previewOn = switchPreview.on
        
        if initPreview != FMSettings.previewOn {
            Flurry.logEvent("settingsPreview", withParameters: ["value": FMSettings.previewOn ? "on" : "off"])
        }
        
        if pushChanged && !keepPushSettings {
            FMLocalNotification.removeAll()
            pushChanged = false
            if FMSettings.pushOn {
                FMLocalNotification.sheduleNext()
                Flurry.logEvent("settingsNotifications", withParameters: [
                    "count": FMSettings.alertCount,
                    "from": NSDate(timeIntervalSince1970: Double(FMSettings.alertTimeFrom - tzOffset)).asString(format: "HHmm"),
                    "to": NSDate(timeIntervalSince1970: Double(FMSettings.alertTimeTo - tzOffset)).asString(format: "HHmm"),
                    "weekdays": FMSettings.alertWeekdays.count
                    ])
            }
        }
    }
    
    func alertGrantAccess(){
        let title = "Notifications disabled"
        let text = "To enable Notifications goto Settings > Notifications > FatigueMeter and check \"Allow Notifications\""
        let buttonDismiss = "Ok"
        
        if #available(iOS 8.0, *) {
            let alertController = UIAlertController(title: title, message: text, preferredStyle: .Alert)
            alertController.addAction(UIAlertAction(title: buttonDismiss, style: .Cancel, handler: { action in   }))
            self.presentViewController(alertController, animated: true, completion: nil)
        } else {
            let alertView = UIAlertView(title: title, message: text, delegate: self, cancelButtonTitle: buttonDismiss)
            alertView.alertViewStyle = .Default
            alertView.show()
        }
    }
    
    func updatePushLabels(){
        if !switchPushes.on {
            return
        }
        labelPushNumber.text = "\(FMSettings.alertCount)"
        let timeFrom = FMSettings.alertTimeFrom
        let timeTo = FMSettings.alertTimeTo
        labelPushTime.text =
            formatter.stringFromDate(NSDate(timeIntervalSince1970: Double(timeFrom - tzOffset)))
            + " - "
            + formatter.stringFromDate(NSDate(timeIntervalSince1970: Double(timeTo - tzOffset)))
    }
    
    @IBAction func fireResetOption(sender: AnyObject) {
        requestResetSettings()
    }
    
    func requestResetSettings(){
        let textTitle = "This will reset app settings to defaults and wipe all your progress. FatigueMeter account data not changed."
        let textResetAll = "Reset data and settings"
        let textResetData = "Reset only data"
        let textCancel = "Cancel"
        
        if #available(iOS 8.0, *) {
            let alertController = UIAlertController(title: nil, message: textTitle, preferredStyle: .ActionSheet)
            let cancelAction = UIAlertAction(title: textCancel, style: .Cancel, handler: nil)
            alertController.addAction(cancelAction)
            
            let resetAllAction = UIAlertAction(title: textResetAll, style: .Destructive, handler: { (action) -> Void in
                self.doReset(true)
            })
            alertController.addAction(resetAllAction)
            
            let resetDataAction = UIAlertAction(title: textResetData, style: .Destructive, handler: { (action) -> Void in
                self.doReset(false)
            })
            alertController.addAction(resetDataAction)
            
            self.presentViewController(alertController, animated: true, completion: { () -> Void in })
        } else {
            let actionSheet = UIActionSheet(title: textTitle, delegate: self, cancelButtonTitle: nil, destructiveButtonTitle: nil)
            actionSheet.actionSheetStyle = UIActionSheetStyle.Default
            actionSheet.addButtonWithTitle(textResetAll)
            actionSheet.addButtonWithTitle(textResetData)
            actionSheet.addButtonWithTitle(textCancel)
            actionSheet.destructiveButtonIndex = 0
            actionSheet.cancelButtonIndex = 2
            actionSheet.showInView(self.tableView)
        }
    }
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        switch buttonIndex {
        case 0:
            doReset(true)
        case 1:
            doReset(false)
        default:
            break
        }
    }
    
    func doReset(fullReset: Bool){
        Flurry.logEvent("reset", withParameters: ["fullReset": fullReset])
        FMStateful.resetAppData(fullReset)
        
        if fullReset {
            pushChanged = false
        }

        MenuViewController.unwrap(self)?.flipToHome()
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        let ri = cell?.reuseIdentifier ?? ""
        if ri == "cellProfile" {
            MenuViewController.unwrap(self)?.performStateChange(.Profile)
            return
        } else if ri == "cellAccount" {
            MenuViewController.unwrap(self)?.performStateChange(.AccountLogin)
            return
        } else if ri == "pushNumber" {
            showPicker(.Number)
        } else if ri == "pushTime" {
            showPicker(.TimeInterval)
        } else if ri == "cellReset" {
            requestResetSettings()
        }
        
    }
    
    override func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return section == 2 ? 44 : 0
    }
    
    override func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 2 {
            viewResetOption.frame = CGRectMake(0, 0, tableView.bounds.width, 44)
            viewResetOption.clipsToBounds = false
            return viewResetOption
        } else {
            return UIView(frame: CGRectZero)
        }
    }
    
    override func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let v = view as? UITableViewHeaderFooterView {
            v.textLabel!.textColor = UIColor.whiteColor()
        }
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        //cell.backgroundColor = colorSemiTransparent
        cell.accessoryView?.tintColor = UIColor.greenColor()
        cell.accessoryView?.backgroundColor = UIColor.blueColor()
        cell.tintColor = UIColor.redColor()
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if !switchPushes.on {
            // only hardcode, due we can't call cellForRowAtIndexPath – recursion loop
            if indexPath.section == 0 && indexPath.row > 0 {
                return 0
            }
        }
        return 44
    }
    
    @IBAction func togglePushes(sender: AnyObject) {
        let hide = !switchPushes.on
        
        if !hide && !AppDelegate.iOS7 {
            FMLocalNotification.requestPermissions()
            if FMLocalNotification.permissionRequestCount > 1 {
                if !FMLocalNotification.hasPermissions() {
                    alertGrantAccess()
                    switchPushes.on = false
                    pushChanged = false
                    return
                }
            }
        }
        
        self.tableView.beginUpdates()
        
        for i in 1...3 {
            let cell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: i, inSection: 0))
            cell?.hidden = hide
        }
        
        self.tableView.endUpdates()
        
        if !FMSettings.pushSuggested {
            // now user knew about notification feature
            FMSettings.pushSuggested = true
        }
        
        updatePushLabels()
        pushChanged = true
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return pickerValues[pickerType]!.count
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerValues[pickerType]![component].count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerValues[pickerType]![component][row]
    }
    
    func pickerView(picker: FMUIPickerView, dismissWithValue: Bool) {
        if dismissWithValue {
            switch pickerType {
            case .Number:
                FMSettings.alertCount = picker.selectedRowInComponent(0) + 1
                updatePushLabels()
                pushChanged = true
            case .TimeInterval:
                let from = picker.selectedRowInComponent(0)
                let to = picker.selectedRowInComponent(2)
                if from <= to {
                    FMSettings.alertTimeFrom = from * timeSteps
                    FMSettings.alertTimeTo = to * timeSteps
                    updatePushLabels()
                    pushChanged = true
                }
            default:
                break
            }
        }
        textHid.resignFirstResponder()
    }
 
    func showPicker(pickerType: PickerType){
        self.pickerType = pickerType
        picker.dataSource = nil
        picker.dataSource = self
        picker.dismissOnTap(self.view)
        switch pickerType {
        case .Number:
            picker.selectRow(FMSettings.alertCount - 1, inComponent: 0, animated: false)
        case .TimeInterval:
            picker.selectRow(Int(FMSettings.alertTimeFrom / timeSteps), inComponent: 0, animated: false)
            picker.selectRow(Int(FMSettings.alertTimeTo / timeSteps), inComponent: 2, animated: false)
        default:
            break
        }
        textHid.becomeFirstResponder()
    }
    
    enum PickerType: Int {
        case None = 0, Number, TimeInterval
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let vs = segue.destinationViewController as? ValueSelectTableViewController {
            vs.multiselect = false
            vs.parentController = self
            
            if segue.identifier == "optionDays" {
                keepPushSettings = true
                vsType = .Days
                vs.tableTitle = "Repeats"
                vs.values = weekdays
                vs.multiselect = true
                vs.selectedIndexes = FMSettings.alertWeekdays
            } else if segue.identifier == "optionSounds" {
                vsType = .Sound
                vs.tableTitle = "Sounds"
                vs.values = soundTitles
                vs.selectedIndexes = [FMSettings.soundMode.rawValue]
            } else if segue.identifier == "optionUnits" {
                vsType = .Units
                vs.tableTitle = "Units"
                vs.values = unitsTitles
                vs.selectedIndexes = [FMSettings.units.rawValue]
            } else if segue.identifier == "segueAbout" {
                Flurry.logEvent("showAbout")
            }
        }
    }
    
    enum ValueSelectType {
        case Days, Sound, Units
    }
    
}
