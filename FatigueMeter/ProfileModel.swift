//
//  ProfileModel.swift
//  FatigueMeter
//
//  Created by vikseriq on 27.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class ProfileModel: NSObject {
    
    var age: Int
    var gender: Int
    var height: Double
    var weight: Double
    var units: ProfileUnits
    var lastUpdate: NSDate
    let settingsKeys = ["userGender", "userAge", "userHeight", "userWeight", "userUpdated"]
    static let ft2m: Double = 0.3048
    static let lbs2kg: Double = 0.4535
    static let rangeAges: [Int] = [12, 80]
    static let rangeHeight: [Double] = [3.5, 7.2]
    static let rangeWeight: [Double] = [55, 530]
    let imperial = FMSettings.units == FMSettings.MeasureUnits.Imperial
    
    override init(){
        let settings = NSUserDefaults.standardUserDefaults()
        
        gender = settings.integerForKey(settingsKeys[0])
        age = settings.integerForKey(settingsKeys[1])
        height = settings.doubleForKey(settingsKeys[2])
        weight = settings.doubleForKey(settingsKeys[3])
        units = .Imperial
        lastUpdate = NSDate(timeIntervalSince1970: settings.doubleForKey(settingsKeys[4]))
        
        super.init()
    }
    
    func save(){
        lastUpdate = NSDate()
        let settings = NSUserDefaults.standardUserDefaults()
        settings.setInteger(gender, forKey: settingsKeys[0])
        settings.setInteger(age, forKey: settingsKeys[1])
        settings.setDouble(height, forKey: settingsKeys[2])
        settings.setDouble(weight, forKey: settingsKeys[3])
        settings.setDouble(lastUpdate.timeIntervalSince1970, forKey: settingsKeys[4])
        settings.synchronize()
        
        Flurry.setAge(Int32(age))
        Flurry.setGender(gender == 0 ? "m" : "f")
    }
    
    func stringForKey(key: String) -> String {
        var v = ""
        
        let x: AnyObject? = self.valueForKey(key)
        
        if key == "age" {
            v = String(age)
        } else if key == "height" {
            if imperial {
                v = String(format: "%.0f' %.0f\"", floor(height), floor((height % 1 + 0.01) * 12))
            } else {
                let h = height * ProfileModel.ft2m
                v = String(format: "%.2f", h)
            }
        } else if key == "weight" {
            if imperial {
                v = String(format: "%.0f", weight)
            } else {
                let w = weight * ProfileModel.lbs2kg
                v = String(format: "%.1f", round(w * 2) / 2)
            }
        } else if key == "gender" {
            v = gender == 0 ? "Male" : "Female"
        } else {
            v = x as? String ?? ""
        }
        
        return v
    }
    
    override func setValue(value: AnyObject?, forKey key: String) {
        if key == "gender" {
            if let x = value as? String {
                gender = x == "Male" ? 0 : 1
            }
        } else if key == "height" {
            if let x = value as? String {
                if imperial {
                    var ftinch = x.regexMatches("(\\d+).+?(\\d+)")
                    if ftinch.count >= 3 {
                        let ft = NSString(string: ftinch[1]).doubleValue
                        let inch = NSString(string: ftinch[2]).doubleValue
                        height = ft + inch / 12
                    } else {
                        height = NSString(string: x).doubleValue
                    }
                } else {
                    height = NSString(string: x).doubleValue / ProfileModel.ft2m
                }
            }
        } else if key == "age" {
            if let x = value as? String {
                age = NSString(string: x).integerValue
            }
        } else if key == "weight" {
            if let x = value as? String {
                if imperial {
                    weight = NSString(string: x).doubleValue
                } else {
                    weight = NSString(string: x).doubleValue / ProfileModel.lbs2kg
                }
            }
        }
    }
    
    enum ProfileUnits {
        case Metric, Imperial
    }
    
}
