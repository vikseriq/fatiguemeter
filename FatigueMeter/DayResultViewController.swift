//
//  DayResultViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 25.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit
import CoreData

class DayResultViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "My progress"
    }
    
    @IBOutlet weak var tableResult: UITableView!
    @IBOutlet weak var chart: FMUIGlassyChartView!
    var results: [TrialEventManaged]!
    var selectedRow: Int = 0
    var currentDate: NSDate!
    
    override func viewWillAppear(animated: Bool) {
        currentDate = NSDate()
        if let date = FMStateful.getData() as? NSDate {
            currentDate = date
            FMStateful.setData(nil)
            results = TrialEvent.fetchDay(currentDate)
        } else {
            results = TrialEvent.fetchLastActualDay()
            if results.count > 0 {
                currentDate = results[0].date
            }
        }
        
        tableResult.reloadData()
        setupPoints()
        
        chart.headlineTitle = "Date"
        chart.headlineDetail = currentDate.asString(.ShortStyle, timeStyle: .NoStyle)
        chart.opaque = false
        chart.backgroundColor = UIColor.clearColor()
        
        FMStateful.setState(.ProgressDay)
    }
    
    func setupPoints(){
        chart.setupView(FMUtility.eventsToPoints(results))
    }
    
    func selectDate(){
        FMStateful.setData(currentDate)
        MenuViewController.unwrap(self)?.performStateChange(.Calendar)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let _cell = tableView.dequeueReusableCellWithIdentifier("dayCell")
        var cell: UITableViewCell
        if _cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "dayCell")
            cell.accessoryType = UITableViewCellAccessoryType.DetailDisclosureButton
        } else {
            cell = _cell!
        }
        let ob = results[indexPath.row]
        let date: NSDate = ob.valueForKey("date") as! NSDate
        let fatigue = ob.valueForKey("fatigue") as! Double
        cell.textLabel?.text = date.asString(.NoStyle, timeStyle: .ShortStyle)
        let rc = RecommendModel.getByFatigue(fatigue)
        cell.detailTextLabel?.text = rc.title
        cell.detailTextLabel?.textColor = rc.color
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        selectedRow = indexPath.row
        FMStateful.setData(results[selectedRow])
        MenuViewController.unwrap(self)?.performStateChange(.ProgressDetail)
    }
    
    @available(iOS 8.0, *)
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: "Delete") { (action, indexPath) -> Void in
            TrialEvent.remove(self.results[indexPath.row])
            self.results.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
            self.setupPoints()
        }
        
        return [deleteAction]
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == UITableViewCellEditingStyle.Delete {
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
        }
    }
}
