//
//  FMStateful
//  FatigueMeter
//
//  Created by vikseriq on 24.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class FMStateful {
   
    static let machine = FMStateful()
    
    var state: FMStates
    
    var background: Bool = false {
        didSet {
            if backgroundDelegate != nil {
                backgroundDelegate.stateBackgroundChanged(background)
            }
        }
    }
    
    var backgroundDelegate: FMStateBackgroundDelegate!
    var exchangeData: AnyObject?
    
    init(){
        state = .New
    }
    
    class func isBackground() -> Bool {
        return FMStateful.machine.background
    }
    
    class func now() -> FMStates {
        return FMStateful.machine.state
    }
    
    class func setState(newState: FMStates){
        FMStateful.machine.state = newState
    }
    
    class func setData(data: AnyObject?){
        let that = FMStateful.machine
        that.exchangeData = data
    }
    
    class func getData() -> AnyObject? {
        return FMStateful.machine.exchangeData
    }
    
    class func resetAppData(fullReset: Bool){
        TrialEvent.clearDatabase()

        if fullReset {
            AccountModel.sharedInstance.logoff()
            FMLocalNotification.removeAll()
            NSUserDefaults.standardUserDefaults().removePersistentDomainForName(NSBundle.mainBundle().bundleIdentifier!)
        }
    }
    
}

protocol FMStateBackgroundDelegate {
    func stateBackgroundChanged(inBackground: Bool)
}

enum FMStates: Int {
    case New = 0, Loading, Home, Load2Home, Home2Detail,
    MoreRun,
    TourOnce,
    Tour,
    ProfileSetup,
    Profile,
    Settings,
    AccountLogin,
    Faq, FaqItem,
    Help,
    HelpTrial,
    RecommendSet,
    RecommendItem,
    RecommendLast,
    Calendar,
    ProgressDay,
    ProgressLast,
    ProgressDetail,
    SubscriptionLanding,
    TrialGo,
    Trialling,
    TrialCompleted
}