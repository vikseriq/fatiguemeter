//
//  TrialReactionViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 09.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class TrialReactionViewController: TrialBaseViewController {

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        trialType = TrialType.Light
        needSoundableClick = true
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBOutlet weak var viewTest: UIView!
    @IBOutlet weak var labelTotal: UILabel!
    @IBOutlet weak var labelWait: UILabel!
    
    var dismissTimer: NSTimer!
    var showTimer: NSTimer!
    var phaseStartedI: CFTimeInterval = CFAbsoluteTimeGetCurrent()
    var phaseActive: Bool = false
    var phaseId: Int = 0
    var phasesTime: [Double]!
    
    func reset(){
        phaseId = -1
        phaseActive = false
        phasesTime = [0, 0, 0]
        if dismissTimer != nil {
            dismissTimer.invalidate()
        }
        if showTimer != nil {
            showTimer.invalidate()
        }
        dismissTimer = nil
        showTimer = nil
        labelWait.hidden = true
        labelTotal.hidden = false
        labelTotal.alpha = 1
        
        trialRunning = false
    }
    
    override func runTrial() {
        if !trialRunning {
            return
        }
        super.runTrial()
        reset()
        trialRunning = true
        nextPhase()
    }
    
    func scheduleNext(extraDelay: Double){
        let delay = FMUtility.random(0.8, to: 1.6) + extraDelay
        showTimer?.invalidate()
        showTimer = NSTimer.scheduledTimerWithTimeInterval(delay, target: self, selector: Selector("showScreen"), userInfo: nil, repeats: false)
    }
    
    /// Scheduling for a new phase to start after small delay
    func nextPhase(){
        phaseId++
        labelTotal.text = String(format: "%i of %i", phaseId + 1, phasesTime.count)
        labelWait.hidden = false
        scheduleNext(0)
    }
    
    /// Starting new test phase and scheduling timeout for autodismiss
    func showScreen(){
        if !trialRunning {
            return
        }
        labelWait.hidden = true
        viewTest.hidden = false
        labelTotal.textColor = UIColor.blackColor()
        phaseStartedI = CFAbsoluteTimeGetCurrent()
        
        showTimer?.invalidate()
        dismissTimer?.invalidate()
        
        dismissTimer = NSTimer.scheduledTimerWithTimeInterval(2.0, target: self, selector: Selector("screenTimeout"), userInfo: nil, repeats: false)
        phaseActive = true
    }

    /// Handles interaction
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if (phaseActive){
            hideScreen(false)
        } else if trialRunning {
            scheduleNext(curbTouches())
        }
        clickSound()
    }
    
    /// Finalizing phase
    func hideScreen(byTimeout: Bool){
        phasesTime[phaseId] = CFAbsoluteTimeGetCurrent() - phaseStartedI
        
        phaseActive = false
        dismissTimer.invalidate()
        
        viewTest.hidden = true
        labelTotal.textColor = UIColor.whiteColor()
        
        if phaseId < phasesTime.count - 1 {
            nextPhase()
        } else {
            finishTrial()
        }
    }
    
    /// Method called when phase should be dissmissed due timeout
    func screenTimeout(){
        if !trialRunning {
            return
        }
        if phaseActive {
            hideScreen(true)
        }
    }
    
    override func finishTrial(){
        trialResults = [phasesTime.average() * 1000]
        super.finishTrial()
    }
    
    
    override func stopTrial() {
        super.stopTrial()
        reset()
    }
    
}
