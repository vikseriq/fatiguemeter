//
//  TrialMuscleViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 09.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class TrialMuscleViewController: TrialBaseViewController {

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        trialType = TrialType.Muscle
        needSoundableClick = true
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBOutlet weak var labelTotal: UILabel!
    @IBOutlet weak var labelTap: UILabel!
    @IBOutlet weak var imageRondo: UIImageView!
    
    var taps: Int = 0
    var trialActive = false
    var trialStarted = false
    var endTimer: NSTimer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    func reset(){
        labelTotal.hidden = true
        labelTotal.alpha = 1
        labelTotal.text = "Taps: 0"
        labelTap.hidden = true
        imageRondo.hidden = true
        taps = 0
        trialActive = false
        trialStarted = false
        endTimer?.invalidate()
        
        trialRunning = false
    }
    
    override func runTrial() {
        if !trialRunning {
            return
        }
        reset()
        super.runTrial()
        trialRunning = true
        
        labelTotal.hidden = false
        labelTap.hidden = false
        imageRondo.hidden = false
        trialActive = true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if !trialActive {
            return
        }
        if !trialStarted {
            trialStarted = true
            endTimer = NSTimer.scheduledTimerWithTimeInterval(5.0, target: self, selector: Selector("finishTrial"), userInfo: nil, repeats: false)
        }
        taps++
        labelTotal.text = "Taps: \(taps)"
        clickSound()
    }
    
    override func finishTrial() {
        if !trialRunning {
            return
        }
        trialActive = false
        labelTotal.alpha = 0.5
        labelTap.hidden = true
        imageRondo.hidden = true
        endTimer.invalidate()
        
        trialResults = [Double(taps) * 2]
        super.finishTrial()
    }
    
    override func stopTrial() {
        super.stopTrial()
        reset()
    }

}
