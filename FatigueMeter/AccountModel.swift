//
//  AccountModel.swift
//  FatigueMeter
//
//  Created by vikseriq on 29.07.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import Accounts
import Social
import UIKit

private let _AccountModelSharedInstance = AccountModel()

class AccountModel: NSObject {
   
    var id: Int = 0
    var login: String = ""
    var token: String = ""
    var oAuth: String = ""
    var lastLogin: NSDate = NSDate(timeIntervalSince1970: 0)
    let settingsKeys = ["accountId", "accountLogin", "accountToken", "accountFbId", "accountLastLogin"]
    private var urlSession: NSURLSession
    
    class var sharedInstance: AccountModel {
        return _AccountModelSharedInstance
    }
    
    override init(){
        let settings = NSUserDefaults.standardUserDefaults()
        var key = 0
        id = settings.integerForKey(settingsKeys[key++]) ?? 0
        login = settings.stringForKey(settingsKeys[key++]) ?? ""
        token = settings.stringForKey(settingsKeys[key++]) ?? ""
        oAuth = settings.stringForKey(settingsKeys[key++]) ?? ""
        lastLogin = NSDate(timeIntervalSince1970: settings.doubleForKey(settingsKeys[key++]))
        
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        configuration.timeoutIntervalForRequest = 30
        configuration.timeoutIntervalForResource = 5 * 3600
        urlSession = NSURLSession(configuration: configuration)
    }
    
    func save(){
        let settings = NSUserDefaults.standardUserDefaults()
        var key = 0
        settings.setInteger(id, forKey: settingsKeys[key++])
        settings.setObject(login, forKey: settingsKeys[key++])
        settings.setObject(token, forKey: settingsKeys[key++])
        settings.setObject(oAuth, forKey: settingsKeys[key++])
        settings.setDouble(lastLogin.timeIntervalSince1970, forKey: settingsKeys[key++])
        settings.synchronize()
    }
    
    class func isActive() -> Bool {
        let model = AccountModel.sharedInstance
        return model.id != 0 && model.token.characters.count > 0
    }
    
    func logoff(){
        id = 0
        token = ""
        oAuth = ""
        lastLogin = NSDate(timeIntervalSince1970: 0)
        save()
    }
    
    func fbLogin(callback: (error: String?) -> ()){
        let fbAppID = NSBundle.mainBundle().infoDictionary!["FacebookAppID"] as? String ?? ""
        let accountStore = ACAccountStore()
        let accountType = accountStore.accountTypeWithAccountTypeIdentifier(ACAccountTypeIdentifierFacebook)
        let options: [NSObject : AnyObject] = [ACFacebookAppIdKey: fbAppID, ACFacebookPermissionsKey: ["email"]]
        
        accountStore.requestAccessToAccountsWithType(accountType, options: options as [NSObject : AnyObject]) { (success, error) -> Void in
            if error != nil {
                callback(error: error.localizedDescription)
            } else {
                var accounts = accountStore.accountsWithAccountType(accountType)
                if accounts.count > 0 {
                    let fb = accounts[0] as! ACAccount
                    let params = ["access_token": fb.credential.oauthToken, "fields": "email"]
                    let url = NSURL(string: "https://graph.facebook.com/me/")
                    
                    let request = SLRequest(forServiceType: SLServiceTypeFacebook, requestMethod: SLRequestMethod.GET, URL: url, parameters: params)
                    request.performRequestWithHandler({ (data, response, error) -> Void in
                        if error != nil {
                            callback(error: "Network error. Are you offline?")
                            return
                        }
                        if response.statusCode != 200 {
                            callback(error: "No access to Facebook")
                            return
                        }
                        
                        do {
                            if let json = try NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves) as?
                                NSDictionary {
                                if let email = json.valueForKey("email") as? String {
                                    let id = json.valueForKey("id") as? String ?? "0"
                                    self.transferFB(email, token: AccountModel.generatePassword(id), callback: callback)
                                } else {
                                    callback(error: "Facebook don't provide your email")
                                }
                            }
                        } catch {
                            callback(error: "Wrong Facebook data")
                            return
                        }
                    })
            
                } else {
                    callback(error: "Please allow FatigueMeter to read your profile data")
                }
            }
        }
    }
    
    func transferFB(login: String, token: String, callback: (error: String?) -> ()){
        self.login = login
        
        // try to login as FB user
        self.auth(login, oAuthToken: token) { (error, values) -> () in
            if error == nil {
                // existing|created & auth, ok
                callback(error: nil)
            } else {
                callback(error: error)
            }
        }
        
        
    }
    
    func register(login: String, password: String, callback: (error: String?, values: NSDictionary) -> ()) {
        netRequest(["task": "register", "login": login, "password": password], callback: { (error, values) -> () in
            if error == nil {
                let id = (values.valueForKey("id") ?? "0").integerValue
                let token = values.valueForKey("token") as? String ?? ""
                
                self.id = id
                self.login = login.lowercaseString
                self.token = token
                self.lastLogin = NSDate()
                self.save()
            }
            callback(error: error, values: values)
        })
    }
    
    func auth(login: String, password: String, callback: (error: String?, values: NSDictionary) -> ()) {
        netRequest(["task": "auth", "login": login, "password": password], callback: { (error, values) -> () in
            if error == nil {
                let id = (values.valueForKey("id") ?? "0")!.integerValue
                let token = values.valueForKey("token") as? String ?? ""
                
                self.id = id
                self.login = login.lowercaseString
                self.token = token
                self.lastLogin = NSDate()
                self.save()
            }
            callback(error: error, values: values)
        })
    }
    
    func auth(login: String, oAuthToken: String, callback: (error: String?, values: NSDictionary) -> ()) {
        netRequest(["task": "oauth", "login": login, "token": oAuthToken], callback: { (error, values) -> () in
            if error == nil {
                let id = (values.valueForKey("id") ?? "0")!.integerValue
                let token = values.valueForKey("token") as? String ?? ""
                
                self.id = id
                self.login = login.lowercaseString
                self.token = token
                self.oAuth = oAuthToken
                self.lastLogin = NSDate()
                self.save()
            }
            callback(error: error, values: values)
        })
    }
    
    func forgot(login: String, callback: (error: String?, values: NSDictionary) -> ()) {
        netRequest(["task": "forgot", "login": login], callback: { (error, values) -> () in
            if error == nil {
                self.login = login.lowercaseString
            }
            callback(error: error, values: values)
        })
    }
    
    func putTrialResult(params: Dictionary<String, AnyObject>, callback: (error: String?, values: NSDictionary) -> ()){
        var paramsExt = params
        paramsExt["task"] = "put"
        paramsExt["uid"] = AccountModel.sharedInstance.id
        paramsExt["token"] = AccountModel.sharedInstance.token
        
        netRequest(params, callback: { (error, values) -> () in
            if error != nil {
                //
            }
            callback(error: error, values: values)
        })
    }
    
    func netRequest(params: [String: AnyObject], callback: (error: String?, values: NSDictionary) -> ()){
        let paramData = try! NSJSONSerialization.dataWithJSONObject(params, options: NSJSONWritingOptions.PrettyPrinted)
        let request = NSMutableURLRequest(URL: NSURL(string: AppDelegate.apiPath)!)
        request.HTTPMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = urlSession.uploadTaskWithRequest(request, fromData: paramData) {
            (data, response, error) -> Void in
            
            var errorMessage: String? = nil
            var values: NSDictionary = NSDictionary()
            
            if error != nil {
                print("NSURLSession failed: \(error!.description)")
                errorMessage = "Network error. Are you offline?"
            } else {
                if data == nil {
                    errorMessage = "Service under maintenance. Try again later"
                } else {
                    if AppDelegate.debug {
                        print("data \(NSString(data: data!, encoding: NSUTF8StringEncoding))")
                    }
                    
                    do {
                        if let jsonResult = try NSJSONSerialization.JSONObjectWithData(data!, options: .MutableLeaves) as? NSDictionary {
                            errorMessage = jsonResult.valueForKey("error") as? String
                            if let errorMsg = errorMessage {
                                if errorMsg.characters.count == 0 {
                                    errorMessage = nil
                                }
                            }
                            values = jsonResult
                        }
                    } catch {
                        
                    }
                }
            }
            if AppDelegate.debug {
                if errorMessage != nil {
                    print("request error: \(errorMessage)")
                }
            }
            
            callback(error: errorMessage, values: values)
        }
        task.resume()
    }
    
    class func generatePassword(token: String) -> String {
        return "FAT_FB_" + token
    }
    
}
