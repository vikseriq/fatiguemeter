//
//  TrialResultModel.swift
//  FatigueMeter
//
//  Created by vikseriq on 17.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit
import CoreData

private let _TrialResultModelSharedInstance = TrialResultModel()

class TrialResultModel {

    class var sharedInstance: TrialResultModel {
        return _TrialResultModelSharedInstance
    }
    
    let kCardiac: Double = 15
    var currentResult: [TrialType: TrialSingleResult]!
    var lastEvent: TrialEvent!
    
    func pushResult(id: TrialType, results: [Double]){
        if currentResult == nil {
            currentResult = Dictionary()
        }
        currentResult![id] = TrialSingleResult(trialType: id, data: results[0], fatigue: 0)
        
        if id == TrialType.Memory {
            pushResult(TrialType.Computation, results: [results[1]])
        }
    }
    
    func commitTrial(){
        if currentResult == nil {
            currentResult = Dictionary()
        }
        let commitDate = NSDate()
        
        // getting profile info
        let profile = ProfileModel()
        
        // fetch previous results
        var prevVals: [TrialType: [Double]] = Dictionary()
        var prevEvents = TrialEvent.fetchDay(commitDate)
        if prevEvents.count == 0 {
            // if it's first test in the day – fetching all previous results
            prevEvents = TrialEvent.fetchLatest()
        }
        for mEvent in prevEvents {
            let res = TrialEvent.unarchiveResults(mEvent.results)
            for r in res {
                if prevVals[r.trialType] == nil {
                    prevVals[r.trialType] = []
                }
                prevVals[r.trialType]!.append(r.data)
            }
        }
        
        // populate result & calculate fatigue
        var fatigue: Double = 0
        var hasSkipped = false
        var res: [TrialSingleResult] = []
        for i in TrialType.Stub.range() {
            let ti = TrialType(rawValue: i)!
            if currentResult[ti] == nil {
                hasSkipped = true
                currentResult[ti] = TrialSingleResult(trialType: ti, data: -1, fatigue: -1)
            }
            var vals: [Double] = []
            if let pv = prevVals[ti] {
                for i in 0..<pv.count {
                    vals.append(prevVals[ti]![i])
                }
            }
            if vals.count == 0 {
                // self-comparation if no previous data stored
                vals.append(currentResult[ti]!.data)
            }
            let fx = computeLevel(ti, current: currentResult[ti]!.data, values: vals)
            currentResult[ti]!.fatigue = fx
            fatigue = fatigue + fx
            res.append(currentResult[ti]!)
        }
        fatigue = fatigue / Double(res.count)
        currentResult = nil
        
        // setup object
        lastEvent = TrialEvent(date: commitDate, p: profile, f: fatigue, res: res)
        
        // save data
        lastEvent.commit()
        
        if !hasSkipped {
            // push to server
            pushToServer(lastEvent)
        } else {
            print("offline due event has skipped trials")
        }
    }
    
    func calculateFatigue(inout events: [TrialEvent], currentIndex: Int, startIndex: Int){
        var values: [TrialType: [Double]] = Dictionary()
        for i in startIndex..<events.count {
            if i == currentIndex {
                continue
            }
            for r in events[i].results {
                if values[r.trialType] == nil {
                    values[r.trialType] = []
                }
                values[r.trialType]!.append(r.data)
            }
        }
        
        var fatigue: Double = 0
        for cr in events[currentIndex].results {
            let ti = cr.trialType
            var vals: [Double] = []
            if let pv = values[ti] {
                for j in 0..<pv.count {
                    vals.append(pv[j])
                }
            }
            let fx = computeLevel(ti, current: cr.data, values: vals)
            cr.fatigue = fx
            fatigue = fatigue + fx
        }
        //fatigue = (fatigue + kCardiac) / Double(events[currentIndex].results.count + 1)
        fatigue = fatigue / Double(events[currentIndex].results.count)
        events[currentIndex].fatigue = fatigue
    }
    
    func pushToServer(event: TrialEvent){
       
        var params: [String: AnyObject] = Dictionary()
        
        params["date"] = event.date.timeIntervalSince1970
        params["age"] = event.age
        params["gender"] = event.gender
        params["weight"] = event.weight
        params["height"] = event.height
        params["fatigue"] = String(format: "%.2f", event.fatigue)
        
        var results: [String: String] = Dictionary()
        for val in event.results {
            results["data\(val.trialType.rawValue)"] = String(format: "%.2f;%.2f", val.data, val.fatigue)
        }
        
        params["results"] = results
        
        //if AccountModel.isActive() {
            AccountModel.sharedInstance.putTrialResult(params, callback: { (error, values) -> () in
                if error != nil {
                    let id = values.valueForKey("id") as? Int ?? 0
                    if id > 0 {
                        event.pushed(id)
                    }
                }
            })
        //}
        
    }
    
    /// -1 used for "No data" flag. All negavite values are invalid
    func computeLevel(id: TrialType, current: Double, values: [Double]) -> Double {
        if values.count == 0 || values.minElement() < 0 {
            return -1
        }
        
        let tc = id.fetchConfig()
        var optimal = current
        //var value = current
        var sumValues: Double = 0
        if tc.bestValue > tc.worstValue {
            optimal = min(max(values.maxElement()!, tc.worstValue), tc.bestValue)
            //value = min(max(current, tc.worstValue), tc.bestValue)
            for v in values {
                sumValues = sumValues + min(max(v, tc.worstValue), tc.bestValue)
            }
        } else {
            optimal = max(min(values.minElement()!, tc.worstValue), tc.bestValue)
            //value = max(min(current, tc.worstValue), tc.bestValue)
            for v in values {
                sumValues = sumValues + max(min(v, tc.worstValue), tc.bestValue)
            }
        }
        
        return abs(1 - (sumValues + current) / (optimal * Double(values.count + 1))) * tc.fatigueK * 100
    }
    
}

enum TrialType: Int {
    case Light = 0, Sound, Colors, Frequency, Muscle, Path, Memory, Computation, Sequence, Stub
    
    func key() -> String {
        let keys = [
            "Light",
            "Sound",
            "Colors",
            "Frequency",
            "Muscle",
            "Path",
            "Memory",
            "Computation",
            "Sequence",
            "-"
        ]
        
        return keys[self.rawValue] ?? "-"
    }
    
    func count() -> Int {
        return 9
    }
    
    func range() -> Range<Int> {
        return 0..<self.count()
    }
    
    func fetchConfig() -> TrialConfig {
        let key         = self.key()
        let plistPath   = NSBundle.mainBundle().pathForResource("Values", ofType: "plist")
        let pDict       = NSDictionary(contentsOfFile: plistPath!)
        let pTrials     = pDict!.valueForKey("Trials") as! Dictionary<String, AnyObject>
        let pTrial      = pTrials[key] as! Dictionary<String, AnyObject>
        
        var tc          = TrialConfig(key: key, title: "", fullTitle: "", previewText: "", detailText: "", iconName: "icon_" + key.lowercaseString, images: [""], previewSpeed: 0, bestValue: 0, worstValue: 0, fatigueK: 0)
        tc.title        = pTrial["Title"] as? String ?? ""
        tc.fullTitle    = pTrial["FullTitle"] as? String ?? ""
        tc.detailText   = pTrial["DetailText"] as? String ?? ""
        tc.previewText  = pTrial["PreviewText"] as? String ?? ""
        tc.images       = pTrial["Images"] as? Array<String> ?? []
        tc.previewSpeed = pTrial["PreviewSpeed"] as? Int ?? 1000
        tc.bestValue    = pTrial["ValueBest"] as? Double ?? 0
        tc.worstValue   = pTrial["ValueWorst"] as? Double ?? 0
        tc.fatigueK     = pTrial["FatigueK"] as? Double ?? 0
        
        return tc
    }
    
}

struct TrialConfig {
    var key: String
    var title: String
    var fullTitle: String
    var previewText: String
    var detailText: String
    var iconName: String
    var images: [String]
    var previewSpeed: Int
    var bestValue: Double
    var worstValue: Double
    var fatigueK: Double
}

class TrialEvent {
    
    var results: [TrialSingleResult] = []
    var date: NSDate = NSDate()
    var synched: Bool = false
    var age: Int = 0
    var gender: Int = 0
    var weight: Double = 0
    var height: Double = 0
    var fatigue: Double = 0
    var _managed: TrialEventManaged?
    
    init(date: NSDate, p: ProfileModel, f: Double, res: [TrialSingleResult]){
        self.date = date
        age = p.age
        gender = p.gender
        weight = p.weight
        height = p.height
        fatigue = f
        results = res
        synched = false
        _managed = nil
    }
    
    init(managed: TrialEventManaged){
        _managed = managed
        date = managed.date
        synched = managed.synched.boolValue
        age = managed.age.integerValue
        gender = managed.gender.integerValue
        weight = managed.weight.doubleValue
        height = managed.height.doubleValue
        fatigue = managed.fatigue.doubleValue
        results = NSKeyedUnarchiver.unarchiveObjectWithData(managed.results) as! [TrialSingleResult]
    }
    
    func setToManaged(event: TrialEventManaged){
        event.date = date
        event.synched = synched
        event.age = age
        event.gender = gender
        event.height = height
        event.weight = weight
        event.fatigue = fatigue
        event.results = NSKeyedArchiver.archivedDataWithRootObject(results)
    }
    
    func writeChanges(){
        if let managed = _managed {
            setToManaged(managed)
            TrialEvent.save()
        }
    }
    
    func commit(){
        let context = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        
        if _managed == nil {
            let entity = NSEntityDescription.entityForName("Event", inManagedObjectContext: context)
            _managed = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: context) as? TrialEventManaged
        }
        
        setToManaged(_managed!)
        
        do {
            try context.save()
        } catch {
            NSLog("Commit failed \(error)")
        }
    }
    
    func pushed(serverId: Int){
        synched = serverId > 0
        if let m = _managed {
            setToManaged(m)
        }
    }
    
    class func fetchTotalEvents() -> Int {
        let context = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        let fetch = NSFetchRequest(entityName: "Event")
        let e = NSErrorPointer()
        let count = context.countForFetchRequest(fetch, error: e)
        
        return count
    }
    
    class func fetchLatest() -> [TrialEventManaged] {
        return fetchLatest(count: 30)
    }
    
    class func fetchLatest(count count: Int) -> [TrialEventManaged] {
        return TrialEvent.fetchDateOrdered(count, minDate: nil, maxDate: NSDate())
    }
    
    class func fetchMostRecent() -> TrialEventManaged? {
        var last = TrialEvent.fetchDateOrdered(1, minDate: nil, maxDate: NSDate())
        if last.count == 1 {
            return last[0]
        }
        return nil
    }
    
    class func fetchLastActualDay() -> [TrialEventManaged] {
        if let recent = self.fetchMostRecent() {
            return self.fetchDay(recent.date)
        } else {
            return []
        }
    }
    
    class func fetchDay(day: NSDate) -> [TrialEventManaged] {
        let comps = FMUtility.calendar.components(FMUtility.calendarParts, fromDate: day)
        comps.hour = 0
        comps.minute = 0
        comps.second = 0
        let dayStart = FMUtility.calendar.dateFromComponents(comps)!
        comps.day++
        let dayEnd = FMUtility.calendar.dateFromComponents(comps)!
        return TrialEvent.fetchDateOrdered(50, minDate: dayStart, maxDate: dayEnd)
    }
    
    class func fetchDayPart(dayTill: NSDate) -> [TrialEventManaged] {
        let comps = FMUtility.calendar.components(FMUtility.calendarParts, fromDate: dayTill)
        comps.hour = 0
        comps.minute = 0
        comps.second = 0
        let dayStart = FMUtility.calendar.dateFromComponents(comps)!
        return TrialEvent.fetchDateOrdered(50, minDate: dayStart, maxDate: dayTill)
    }
    
    class func fetchMonth(month: NSDate) -> [TrialEventManaged] {
        let comps = FMUtility.calendar.components(FMUtility.calendarParts, fromDate: month)
        comps.day = 1
        comps.hour = 0
        comps.minute = 0
        comps.second = 0
        let monthStart = FMUtility.calendar.dateFromComponents(comps)!
        comps.month++
        let monthEnd = FMUtility.calendar.dateFromComponents(comps)!
        return TrialEvent.fetchDateOrdered(30 * 20, minDate: monthStart, maxDate: monthEnd)
    }
    
    class func fetchDateOrdered(numberOfRecords: Int, minDate: NSDate?, maxDate: NSDate?, sortAscending: Bool = false) -> [TrialEventManaged] {
        let context = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        let fetch = NSFetchRequest(entityName: "Event")
        fetch.sortDescriptors = [NSSortDescriptor(key: "date", ascending: sortAscending)]
        fetch.fetchLimit = numberOfRecords
        var predicates = [NSPredicate]()
        if let dateTo = maxDate {
            predicates.append(NSPredicate(format: "date <= %@", dateTo))
        }
        if let dateFrom = minDate {
            predicates.append(NSPredicate(format: "date >= %@", dateFrom))
        }
        if predicates.count > 0 {
            fetch.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
        }
        
        do {
            let results = try context.executeFetchRequest(fetch) as? [TrialEventManaged]
            return results ?? []
        } catch {
            print("Fetch failed \(error)")
            return []
        }
    }
    
    class func fetchEdgeEvent(fetchMin: Bool) -> TrialEventManaged? {
        var res = fetchDateOrdered(1, minDate: nil, maxDate: nil, sortAscending: fetchMin)
        return res.count > 0 ? res[0] : nil
    }
    
    class func clearDatabase(){
        let context = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        let fetch = NSFetchRequest(entityName: "Event")
        fetch.includesPropertyValues = false
        
        do {
            if let result = try context.executeFetchRequest(fetch) as? [NSManagedObject] {
                for object in result {
                    context.deleteObject(object)
                }
                self.save()
            }
        } catch {
            NSLog("Can't clear DB: \(error)")
        }
    }
    
    class func remove(val: NSManagedObject){
        let context = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        context.deleteObject(val)
        self.save()
    }
    
    class func save(){
        let context = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        do {
            try context.save()
        } catch {
            NSLog("Saving error: \(error)")
        }
    }
    
    class func unarchiveResults(managedResults: NSData) -> [TrialSingleResult] {
        return  NSKeyedUnarchiver.unarchiveObjectWithData(managedResults) as! [TrialSingleResult]
    }
    
}

class TrialSingleResult: NSObject, NSCoding {
    
    var trialType: TrialType
    var fatigue: Double!
    var data: Double
    
    init(trialType: TrialType, data: Double, fatigue: Double){
        self.trialType = trialType
        self.data = data
        self.fatigue = fatigue
    }
    
    required init(coder aDecoder: NSCoder) {
        trialType = TrialType(rawValue: aDecoder.decodeIntegerForKey("type"))!
        data = aDecoder.decodeDoubleForKey("data")
        fatigue = aDecoder.decodeDoubleForKey("fat")
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeDouble(fatigue, forKey: "fat")
        aCoder.encodeDouble(data, forKey: "data")
        aCoder.encodeInteger(trialType.rawValue, forKey: "type")
    }
    
    func asString() -> String {
        var units = ""
        var data = self.data
        switch trialType {
        case .Muscle:
            data = data / 2.0
            units = "taps"
        case .Path:
            units = "%"
        case .Memory:
            units = "numb"
        case .Computation, .Sequence:
            units = "s"
        default:
            units = "ms"
        }
        return String(format: "%.2f %@", data, units)
    }
    
}


