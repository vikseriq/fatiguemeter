//
//  MoreRunViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 28.09.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class MoreRunViewController: UIViewController {
    
    override func viewDidAppear(animated: Bool) {
        NaviViewController.setTitle(self.navigationController!, title: "Pass two tests")
    }
    
    @IBAction func fireRun(sender: AnyObject) {
        Flurry.logEvent("showTrial", withParameters: ["source": "moreRun"])
        MenuViewController.unwrap(self)?.performStateChange(.TrialGo)
    }
    
}
