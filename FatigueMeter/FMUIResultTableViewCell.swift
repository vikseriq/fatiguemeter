//
//  FMUIResultTableViewCell.swift
//  FatigueMeter
//
//  Created by vikseriq on 03.03.16.
//  Copyright © 2016 vikseriq. All rights reserved.
//

import UIKit

class FMUIResultTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewBase.layer.cornerRadius = 4
        selectedBackgroundView = UIView(frame: viewBase.frame)
        selectedBackgroundView?.backgroundColor = viewBase.tintColor
        selectedBackgroundView?.layer.cornerRadius = 4
    }
    
    @IBOutlet weak var viewBase: UIView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageIcon: UIImageView!
    @IBOutlet weak var labelSubtitle: UILabel!
    @IBOutlet weak var viewPill: FMUIRoundPill!
    
    func setupView(result: TrialSingleResult?){
        guard let r = result else {
            return
        }
        let resultFormat = RecommendModel.getByFatigue(r.fatigue)
        let resultType = r.trialType.fetchConfig()
        
        labelTitle.text = resultType.fullTitle
        imageIcon.image = UIImage(named: resultType.iconName)
        labelSubtitle.text = resultFormat.title + ", " + r.asString()
        viewPill.backgroundColor = resultFormat.color
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        selectedBackgroundView?.frame = viewBase.frame
    }
    
}
