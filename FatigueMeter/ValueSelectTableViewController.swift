//
//  ValueSelectTableViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 05.05.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class ValueSelectTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView(frame: CGRectZero)
        tableView.tableHeaderView = UIView(frame: CGRectMake(0, 20, tableView.bounds.width, 20))
        tableView.tintColor = UIColor.whiteColor()
    }

    var tableTitle: String? {
        didSet {
            self.title = tableTitle
        }
    }
    var values: [String]!
    var selectedIndexes: [Int]!
    var multiselect: Bool = false
    var parentController: SettingsTableViewController!
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return values.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        tableView.allowsMultipleSelection = multiselect
        
        let cell = tableView.dequeueReusableCellWithIdentifier("option", forIndexPath: indexPath)

        cell.textLabel?.text = values[indexPath.row]
        let checked = selectedIndexes.filter { (el) -> Bool in
            return el == indexPath.row
        }.count == 1
        cell.accessoryType = checked ? UITableViewCellAccessoryType.Checkmark :  UITableViewCellAccessoryType.None
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        
        let checked = selectedIndexes.indexOf(indexPath.row)
        if multiselect {
            if let index = checked {
                selectedIndexes.removeAtIndex(index)
            } else {
                selectedIndexes.append(indexPath.row)
            }
        } else {
            selectedIndexes.removeAll(keepCapacity: true)
            selectedIndexes.append(indexPath.row)
        }
        tableView.reloadData()
    }

    override func viewWillDisappear(animated: Bool) {
        if parentController != nil {
            parentController.selectedIndexes = selectedIndexes
        }
    }

}
