//
//  AccountRegisterViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 01.06.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class AccountRegisterViewController: UIViewController {

    
    @IBOutlet weak var loginText: FMUIRowView!
    @IBOutlet weak var passwordText: FMUIRowView!
    @IBOutlet weak var repeatPasswordText: FMUIRowView!
    @IBOutlet weak var registerButton: FMUIRoundButton!

    var processIndicator: FMUIProcessIndicator!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loginText.addTarget(passwordText, action: Selector("becomeFirstResponder"), forControlEvents: UIControlEvents.EditingDidEndOnExit)
        passwordText.addTarget(repeatPasswordText, action: Selector("becomeFirstResponder"), forControlEvents: UIControlEvents.EditingDidEndOnExit)
        repeatPasswordText.addTarget(self, action: Selector("becomeFirstResponder"), forControlEvents: UIControlEvents.EditingDidEndOnExit)
    }
    
    override func viewDidAppear(animated: Bool) {
        processIndicator = FMUIProcessIndicator(view: self.view)
    }
    
    @IBAction func registerFired(sender: AnyObject) {
        becomeFirstResponder()
        let login = loginText.getValue()
        if login.characters.count < 3 {
            loginText.shakeBox()
            return
        }
        
        let password = passwordText.getValue()
        if password.characters.count < 6 {
            passwordText.shakeBox()
            return
        }
        
        let repeatPassword = repeatPasswordText.getValue()
        if password != repeatPassword {
            passwordText.shakeBox()
            repeatPasswordText.shakeBox()
            return
        }
        
        view.endEditing(true)
        processIndicator.show()
        
        AccountModel.sharedInstance.register(login, password: password) { (error, values) -> () in
            NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                self.processIndicator?.dismiss()
                if let e = error {
                    self.showAlert(e, onButton: { () -> () in })
                } else {
                    self.showAlert("You are successful registered.\nWelcome to FatigueMeter club!", onButton: { () -> () in
                        MenuViewController.unwrap(self)?.popToRootController()
                    })
                }
            })
        }
    }
    
    func showAlert(message: String, onButton: () -> ()){
        let title = "Registration"
        let button = "Ok"
        
        if #available(iOS 8.0, *) {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
            alertController.addAction(UIAlertAction(title: button, style: UIAlertActionStyle.Default, handler: { action in
                onButton()
            }))
            self.presentViewController(alertController, animated: true, completion: nil)
        } else {
            let alertView = UIAlertView(title: title, message: message, delegate: nil, cancelButtonTitle: button)
            alertView.alertViewStyle = .Default
            alertView.show()
            onButton()
        }
    }
    
}
