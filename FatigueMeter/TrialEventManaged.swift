//
//  TrialEventManaged.swift
//  FatigueMeter
//
//  Created by vikseriq on 27.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import Foundation
import CoreData

@objc(TrialEventManaged)
class TrialEventManaged: NSManagedObject {

    @NSManaged var age: NSNumber
    @NSManaged var date: NSDate
    @NSManaged var fatigue: NSNumber
    @NSManaged var gender: NSNumber
    @NSManaged var height: NSNumber
    @NSManaged var results: NSData
    @NSManaged var synched: NSNumber
    @NSManaged var weight: NSNumber

}
