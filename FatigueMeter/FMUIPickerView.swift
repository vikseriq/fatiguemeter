//
//  FMUIPickerView.swift
//  FatigueMeter
//
//  Created by vikseriq on 05.05.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class FMUIPickerView: UIPickerView {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
    }
    
    init(){
        super.init(frame: CGRectZero)
        
        setupView()
    }
    
    var toolbar: UIToolbar!
    var stateDelegate: FMUIPickerStateDelegate!
    var tap: UITapGestureRecognizer!

    func setupView(){
        toolbar = UIToolbar()
        toolbar.barStyle = UIBarStyle.Default
        toolbar.translucent = true
        toolbar.tintColor = UIColor(rgba: "#265F90")
        toolbar.sizeToFit()
        
        let pickerDone = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Done, target: self, action: Selector("pickerDone"))
        let pickerCancel = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("pickerDismiss"))
        let pickerSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        
        toolbar.setItems([pickerCancel, pickerSpace, pickerDone], animated: false)
    }
    
    func assignToTextField(textField: UITextField){
        textField.inputAccessoryView = toolbar
        textField.inputView = self
    }
    
    func dismissOnTap(view: UIView){
        tap = UITapGestureRecognizer(target: self, action: Selector("pickerDismiss"))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        view.addGestureRecognizer(tap)
    }
    
    func pickerDone(){
        if tap != nil {
            tap.view?.removeGestureRecognizer(tap)
        }
        if let x = stateDelegate {
            x.pickerView(self, dismissWithValue: true)
        }
    }
    
    func pickerDismiss(){
        if tap != nil {
            tap.view?.removeGestureRecognizer(tap)
        }
        if let x = stateDelegate {
            x.pickerView(self, dismissWithValue: false)
        }
    }
    
}

protocol FMUIPickerStateDelegate: NSObjectProtocol {
    func pickerView(picker: FMUIPickerView, dismissWithValue: Bool)
    
}
