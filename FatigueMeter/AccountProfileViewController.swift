//
//  AccountProfileViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 25.08.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class AccountProfileViewController: UIViewController {

    @IBOutlet weak var loginText: FMUIRowView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loginText.value = AccountModel.sharedInstance.login
        loginText.textValue.enabled = false
    }
    
    @IBAction func logoffFired(sender: AnyObject) {
        AccountModel.sharedInstance.logoff()
        MenuViewController.unwrap(self)?.popController()
    }

    @IBAction func privacyFired(sender: AnyObject) {
        if let url = NSURL(string: "http://ormanbay.com/privacy/"){
            UIApplication.sharedApplication().openURL(url)
        }
    }
    
}
