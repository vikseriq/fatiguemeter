//
//  NaviViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 25.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit
import Foundation

class NaviViewController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if self.revealViewController() != nil {
            //self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            
            self.revealViewController().toggleAnimationType = SWRevealToggleAnimationType.EaseOut
            self.revealViewController().toggleAnimationDuration = 0.25
            self.revealViewController().frontViewShadowOpacity = 0.4
            self.revealViewController().draggableBorderWidth = 150
            self.revealViewController().bounceBackOnOverdraw = false
            self.revealViewController().rearViewRevealWidth = 260
            self.revealViewController().rearViewRevealOverdraw = 0
            self.revealViewController().rearViewRevealDisplacement = 0
        }
        
        AppDelegate.naviViewController = self
        
        imageMenu = UIImage(named: "menu")
        
        menuButton = UIButton()
        menuButton.imageView?.contentMode = UIViewContentMode.ScaleAspectFit
        menuButton.addTarget(self, action: Selector("toggleMenu"), forControlEvents: UIControlEvents.TouchUpInside)
        menuButton.frame = CGRectMake(0, 0, 25, 30)
        menuButton.setImage(imageMenu, forState: .Normal)
        menuButton.setImage(imageMenu.tintWithColor(FMUtility.colorActive, blendMode: .Multiply), forState: .Highlighted)
        
        disclosureButton = UIButton(type: .DetailDisclosure)
        disclosureButton.addTarget(self, action: Selector("fireDisclosure"), forControlEvents: UIControlEvents.TouchUpInside)
        disclosureButton.frame = CGRectMake(0, 0, 25, 30)
    }
    
    var menuButton: UIButton!
    var disclosureButton: UIButton!
    var disclosureTarget: (() -> Void)!
    var imageMenu: UIImage!
    
    override func viewWillAppear(animated: Bool) {
        if AppDelegate.widgetAction == AppDelegate.kWidgetActionRun {
            runTrial()
        } else {
            if FMStateful.now() == .Load2Home || FMStateful.now() == .Home {
                setHome()
            }
        }
    }
    
    func runTrial(){
        AppDelegate.widgetAction = AppDelegate.kWidgetActionNone
        if let menu = self.revealViewController().rearViewController as? MenuViewController {
            menu.performStateChange(.TrialGo)
        }
    }
    
    func setHome(){
        if let menu = self.revealViewController().rearViewController as? MenuViewController {
            FMStateful.setState(.Loading)
            menu.performStateChange(.Load2Home)
        }
    }
    
    func goBack(){
        popViewControllerAnimated(true)
    }
    
    var menuToggled: Bool {
        get {
            if let rv = revealViewController() {
                return rv.frontViewPosition != FrontViewPosition.Left
            }
            return false
        }
    }
    
    func toggleMenu(){
        revealViewController()?.revealToggleAnimated(true)
    }
    
    func fireDisclosure(){
        disclosureTarget()
    }
    
    class func showMenu(sender: UIViewController){
        if sender.revealViewController() == nil {
            return
        }
        sender.view.addGestureRecognizer(sender.revealViewController().panGestureRecognizer())
        if let nc = sender.navigationController as? NaviViewController {
            sender.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: nc.menuButton)
        }
    }
    
    class func showDisclosure(sender: UIViewController, callback: () -> Void){
        if let nc = sender.navigationController as? NaviViewController {
            sender.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: nc.disclosureButton)
            nc.disclosureTarget = callback
        }
    }
    
    class func setTitle(sender: UINavigationController, title: String){
        if let nb = sender.navigationBar as? FMUINavigationBar {
            nb.topItem?.title = title
        }
    }
    
}
