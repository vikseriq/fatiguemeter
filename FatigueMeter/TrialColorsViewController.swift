//
//  TrialColorsViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 09.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import Foundation
import UIKit

class TrialColorsViewController: TrialBaseViewController {

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        trialType = TrialType.Colors
        needSoundableClick = true
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBOutlet weak var viewBlink: UIView!
    @IBOutlet weak var labelTap: UILabel!
    @IBOutlet weak var labelWait: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    let colorGreen = UIColor(rgba: "#72A639")
    let colorWhite = UIColor.whiteColor()
    let colorRed = UIColor(rgba: "#CD5122")
    let kWhite = 0
    let kGreen = 1
    let kRed = 2
    var colorMap: [Int]!
    var currentRun: Int = 0
    var trialActive: Bool = false
    var timeStart: CFTimeInterval = 0
    var runTimes: [Double] = []
    var extraTime: Double = 0
    
    var delayTimer: NSTimer!
    var dismissTimer: NSTimer!
    
    func reset(){
        viewBlink.hidden = true
        labelTap.hidden = true
        labelWait.hidden = true
        
        colorMap = []
        for i in 0..<9 {
            colorMap.append(i % 3)
        }
        repeat {
            colorMap.sortInPlace {
                (_,_) in arc4random() < arc4random()
            }
        } while colorMap[colorMap.count - 1] == kRed
        currentRun = -1
        trialActive = false
        if delayTimer != nil {
            delayTimer.invalidate()
        }
        if dismissTimer != nil {
            dismissTimer.invalidate()
        }
        delayTimer = nil
        dismissTimer = nil
        runTimes = []
        extraTime = 0
        
        trialRunning = false
    }
    
    override func runTrial() {
        if !trialRunning {
            return
        }
        super.runTrial()
        reset()
        trialRunning = true
        scheduleNext(0.0)
    }
    
    func scheduleNext(extraDelay: Double){
        let delay = FMUtility.random(0.8, to: 1.6) + extraDelay
        
        labelTap.hidden = true
        viewBlink.hidden = true
        delayTimer?.invalidate()
        
        if runTimes.count >= 6 {
            finishTrial()
            return
        }
        
        labelWait.hidden = false
        
        delayTimer = NSTimer.scheduledTimerWithTimeInterval(delay, target: self, selector: Selector("nextColor"), userInfo: nil, repeats: false)
    }
    
    func nextColor(){
        if !trialRunning {
            return
        }
        trialActive = true
        currentRun++
        let ci = colorMap[currentRun]
        switch ci {
        case kWhite:
            labelTap.textColor = UIColor.blackColor()
            labelTap.hidden = false
            viewBlink.backgroundColor = colorWhite
        case kGreen:
            labelTap.textColor = UIColor.whiteColor()
            labelTap.hidden = false
            viewBlink.backgroundColor = colorGreen
        default:
            labelTap.hidden = true
            viewBlink.backgroundColor = colorRed
            break
        }
        labelWait.hidden = true
        viewBlink.hidden = false
        dismissTimer = NSTimer.scheduledTimerWithTimeInterval(1.6, target: self, selector: Selector("dismissColor"), userInfo: nil, repeats: false)
        
        timeStart = CFAbsoluteTimeGetCurrent()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if trialActive {
            trialActive = false
            if colorMap[currentRun] == kRed {
                extraTime += 250
            } else {
                hideColor(false)
            }
        } else if trialRunning {
            scheduleNext(curbTouches())
        }
        clickSound()
    }
    
    func hideColor(byTimeout: Bool){
        let dt = Double(CFAbsoluteTimeGetCurrent() - timeStart)
        if colorMap[currentRun] == kWhite || colorMap[currentRun] == kGreen {
            runTimes.append(dt)
        }
        trialActive = false
        dismissTimer.invalidate()
        scheduleNext(0.0)
    }
    
    func dismissColor(){
        if !trialRunning {
            return
        }
        hideColor(true)
    }
    
    override func finishTrial() {
        trialResults = [runTimes.average() * 1000 + extraTime]
        super.finishTrial()
    }
    
    override func stopTrial() {
        super.stopTrial()
        reset()
    }

}
