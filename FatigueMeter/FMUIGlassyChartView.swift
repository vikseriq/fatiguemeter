//
//  FMUIGlassyChartView.swift
//  FatigueMeter
//
//  Created by vikseriq on 07.03.16.
//  Copyright © 2016 vikseriq. All rights reserved.
//

import UIKit

class FMUIGlassyChartView: UIView {

    var headlineTitle: String = "Your result" {
        didSet {
            setupView()
        }
    }
    
    var headlineDetail: String = "Date" {
        didSet {
            setupView()
        }
    }
    
    let colorPrimary = UIColor(rgba: "#ffffffaa")
    
    var viewChart: UIView!
    var boxHeadline: UIControl!
    var labelHeadlineTitle: UILabel!
    var labelHeadlineDetail: UILabel!
    
    var timeCrop: Double = 0
    var pointData: [(x: Double, y: Double)]!
    
    func initView(){
        for subview in self.subviews {
            subview.removeFromSuperview()
        }
        
        let headlineHeight: CGFloat = 30
        let gapX: CGFloat = 15
        
        boxHeadline = UIControl(frame: CGRectMake(0, 0, frame.width, headlineHeight))
        let halfBox = boxHeadline.frame.width * 0.5
        
        labelHeadlineTitle = UILabel(frame: CGRectMake(gapX, 0, halfBox - gapX, headlineHeight))
        labelHeadlineTitle.font = FMUtility.systemFont(14)
        labelHeadlineTitle.textColor = FMUtility.colorWhite
        labelHeadlineTitle.textAlignment = .Left
        labelHeadlineDetail = UILabel(frame: CGRectMake(halfBox + gapX * 2, 0, halfBox - gapX * 3, headlineHeight))
        labelHeadlineDetail.font = FMUtility.systemFont(12)
        labelHeadlineDetail.textColor = colorPrimary
        labelHeadlineDetail.textAlignment = .Right
        
        addSubview(boxHeadline)
        boxHeadline.addSubview(labelHeadlineTitle)
        boxHeadline.addSubview(labelHeadlineDetail)
        
        let chartTop = headlineHeight
        viewChart = FMUIGlassyPointsView(frame: CGRectMake(0, chartTop, frame.width, frame.height - chartTop), parent: self)
        
        addSubview(viewChart)
    }
    
    func setupView(data: [NSTimeInterval: Double], toDate: NSDate?){
        if data.count == 0 {
            return
        }
        let keys = data.keys.sort()
        timeCrop = toDate?.timeIntervalSince1970 ?? 0
        pointData = []
        
        for k in keys {
            pointData.append(x: k, y: data[k]!)
        }
        setNeedsDisplay()
        
        setupView()
    }
    
    func setupView(data: [NSTimeInterval: Double]){
        setupView(data, toDate: nil)
    }
    
    func setupView(){
        if viewChart == nil {
            initView()
        }
        
        labelHeadlineDetail.text = headlineDetail
        labelHeadlineTitle.text = headlineTitle
    }
    
    func onHeadlineTouchAction(target: AnyObject?, action: Selector){
        boxHeadline.addTarget(target, action: action, forControlEvents: .TouchUpInside)
    }
    
    func onChartTouchAction(target: AnyObject, action: Selector){
        let recognizer = UITapGestureRecognizer(target: target, action: action)
        viewChart.addGestureRecognizer(recognizer)
    }
}

class FMUIGlassyPointsView: UIView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init(frame: CGRect, parent: FMUIGlassyChartView){
        super.init(frame: frame)
        self.parent = parent
        self.opaque = false
        
        topY = 10
        frameHeight = frame.height - topY
        legendTopY = frameHeight - legendHeight + radiusPoint
        pointsRect = CGRectMake(topY, 0, parent.frame.width, legendTopY)
        
        let paragraph: NSMutableParagraphStyle = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        paragraph.alignment = NSTextAlignment.Center
        textAttrTime = [
            NSFontAttributeName: UIFont.boldSystemFontOfSize(14),
            NSForegroundColorAttributeName: colorPrimary,
            NSParagraphStyleAttributeName: paragraph
        ]
        textAttrDateTime = [
            NSFontAttributeName: UIFont.boldSystemFontOfSize(10),
            NSForegroundColorAttributeName: colorPrimary,
            NSParagraphStyleAttributeName: paragraph
        ]
    }
    
    class func curveControlPoint(left: CGPoint, _ right: CGPoint) -> CGPoint {
        return CGPointMake(
            (left.x + right.x) * 0.5,
            (left.y + right.y) * 0.5 * (1 + 0.2 * (left.y > right.y ? 1 : -1))
        )
    }
    
    let legendHeight: CGFloat = 30
    let radiusPoint: CGFloat = 5
    let radiusBigPointInner: CGFloat = 7
    let radiusBigPointGap: CGFloat = 2
    let lineWidth: CGFloat = 2
    let colorPrimary = UIColor(rgba: "#A1BFBF")
    let colorGrid = UIColor(rgba: "#FFFFFF18")
    let colorPoint = UIColor.whiteColor()
    let colorPointOuter = UIColor(rgba: "#94A2A9")
    let fatigueTextFont = FMUtility.systemFont(12)
    let fatigueTextParagraph: NSMutableParagraphStyle = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
    let fatigueTextHeight: CGFloat = 15
    
    var textAttrDateTime: [String: AnyObject]!
    var textAttrTime: [String: AnyObject]!
    var topY: CGFloat = 0
    var frameHeight: CGFloat = 0
    var legendTopY: CGFloat = 0
    var parent: FMUIGlassyChartView!
    var pointsRect: CGRect!
    
    override func drawRect(rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
    
        if parent.pointData != nil && parent.pointData.count > 0 {
            drawPoints(context!, rect: pointsRect, values: parent.pointData, actualFrom: parent.timeCrop)
        }
    }
    
    func drawPoints(context: CGContext, rect: CGRect, values: [(x: Double, y: Double)], actualFrom: Double){
        let gap = radiusPoint

        var maxV = values[0].y
        var minV = values[0].y
        for (_, y) in values {
            maxV = max(maxV, y)
            minV = min(minV, y)
        }
        
        // draw scale
        let fatigueHigh = RecommendModel.getByFatigue(minV)
        let fatigueLow = RecommendModel.getByFatigue(maxV)
        
        let halfWidth = frame.width * 0.5
        let fatigueHighRect = CGRectMake(halfWidth + gap, 0, halfWidth - 2 * gap, fatigueTextHeight)
        let fatigueLowRect = CGRectMake(halfWidth + gap, legendTopY - fatigueTextHeight, halfWidth - 2 * gap, fatigueTextHeight)
        
        fatigueTextParagraph.alignment = NSTextAlignment.Right
        NSString(string: fatigueHigh.title).drawInRect(fatigueHighRect, withAttributes: [
            NSFontAttributeName: fatigueTextFont,
            NSForegroundColorAttributeName: fatigueHigh.color,
            NSParagraphStyleAttributeName: fatigueTextParagraph
        ])
        if fatigueLow.range != fatigueHigh.range {
            NSString(string: fatigueLow.title).drawInRect(fatigueLowRect, withAttributes: [
                NSFontAttributeName: fatigueTextFont,
                NSForegroundColorAttributeName: fatigueLow.color,
                NSParagraphStyleAttributeName: fatigueTextParagraph
            ])
        }
        
        var deltaV = CGFloat(maxV - minV)
        if deltaV == 0 {
            deltaV = 1
        }
        
        let stepX: CGFloat = values.count > 1 ? rect.width / CGFloat(values.count) : rect.width
        let stepY: CGFloat = (rect.height - 4 * gap) / deltaV
        var startY = topY + rect.minY + 2.5 * gap
        
        var drawPoint: CGPoint = CGPoint(x: rect.minX + gap - stepX * 0.5, y: 0)
        var prevPoint: CGPoint = CGPoint(x: -stepX, y: 0)
        var middlePoint: CGPoint! = nil
        var prevCoordX: Double = 0
        if values.count == 1 {
            // one value - display in center
            drawPoint.x = rect.width / 2 - stepX
            startY = rect.minY + rect.height / 2
        }
        let drawPointStart = drawPoint
        
        // draw line
        CGContextSetLineWidth(context, lineWidth)
        CGContextSetLineCap(context, .Butt)
        colorPrimary.setStroke()
        for coord in values {
            let val = CGFloat(coord.y)
            drawPoint.x += stepX
            drawPoint.y = startY + stepY * CGFloat(val - CGFloat(minV))
            if prevCoordX < actualFrom {
                CGContextSetLineDash(context, 0, [4.0, 2.0], 2)
            } else {
                CGContextSetLineDash(context, 0, [1.0], 0)
            }
            if prevPoint.x < 0 {
                prevPoint.y = drawPoint.y
                middlePoint = FMUIGlassyPointsView.curveControlPoint(prevPoint, drawPoint)
                CGContextMoveToPoint(context, prevPoint.x, prevPoint.y)
                CGContextAddQuadCurveToPoint(context, middlePoint.x, middlePoint.y, drawPoint.x, drawPoint.y)
                CGContextStrokePath(context)
            } else {
                middlePoint = FMUIGlassyPointsView.curveControlPoint(prevPoint, drawPoint)
                CGContextMoveToPoint(context, prevPoint.x, prevPoint.y)
                CGContextAddQuadCurveToPoint(context, middlePoint.x, middlePoint.y, drawPoint.x, drawPoint.y)
                CGContextStrokePath(context)
            }
            
            prevPoint = drawPoint
            prevCoordX = coord.x
        }
        // and line goes to future
        CGContextSetLineDash(context, 0, [1.0], 0)
        drawPoint = CGPointMake(self.frame.width * 1.25, drawPoint.y)
        middlePoint = FMUIGlassyPointsView.curveControlPoint(prevPoint, drawPoint)
        CGContextMoveToPoint(context, prevPoint.x, prevPoint.y)
        CGContextAddQuadCurveToPoint(context, middlePoint.x, middlePoint.y, drawPoint.x, drawPoint.y)
        CGContextStrokePath(context)
        
        // draw points and legend
        var i = 0
        CGContextSetLineWidth(context, 1)
        colorPoint.setFill()
        colorGrid.setStroke()
        drawPoint = drawPointStart
        var textRect = CGRectMake(drawPoint.x - stepX * 0.5, legendTopY + gap, stepX, legendHeight)
        
        for coord in values {
            let val = CGFloat(coord.y)
            drawPoint.x += stepX
            drawPoint.y = startY + stepY * CGFloat(val - CGFloat(minV))
            textRect.offsetInPlace(dx: stepX, dy: 0)
            
            // grid line
            CGContextMoveToPoint(context, drawPoint.x, drawPoint.y)
            CGContextAddLineToPoint(context, drawPoint.x, legendTopY)
            CGContextStrokePath(context)
            
            // legend
            let date = NSDate(timeIntervalSince1970: coord.x)
            if coord.x >= actualFrom {
                NSString(string: date.asString(.NoStyle, timeStyle: .ShortStyle)).drawInRect(textRect, withAttributes: textAttrTime)
            } else {
                NSString(string: date.asString(.ShortStyle, timeStyle: .NoStyle) + "\n" + date.asString(.NoStyle, timeStyle: .ShortStyle)).drawInRect(textRect, withAttributes: textAttrDateTime)
            }
            
            // point circle
            if i < values.count - 1 {
                colorPoint.setFill()
                CGContextAddArc(context, drawPoint.x, drawPoint.y, radiusPoint, 0, CGFloat(M_PI * 2), 1)
                CGContextFillPath(context)
            } else {
                colorPointOuter.setFill()
                CGContextAddArc(context, drawPoint.x, drawPoint.y, radiusBigPointInner + radiusBigPointGap, 0, CGFloat(M_PI * 2), 1)
                CGContextFillPath(context)
                
                colorPoint.setFill()
                CGContextAddArc(context, drawPoint.x, drawPoint.y, radiusBigPointInner, 0, CGFloat(M_PI * 2), 1)
                CGContextFillPath(context)
            }
            i++
        }
        
    }
}
