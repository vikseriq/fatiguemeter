//
//  CalendarViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 18.05.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class CalendarViewController: UIViewController, FMUICalendarViewDelegate, FMUICalendarViewDataSource {

    @IBOutlet weak var calendarView: FMUICalendarView!
    
    static var date: NSDate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        calendarView.delegate = self
        calendarView.dataSource = self
    }
    
    override func viewWillAppear(animated: Bool) {
        FMStateful.setState(.Calendar)
        
        if CalendarViewController.date == nil {
            CalendarViewController.date = NSDate()
        }
        if let date = FMStateful.getData() as? NSDate {
            CalendarViewController.date = date
        }
        calendarView.showMonth(CalendarViewController.date, offsetMonth: 0)
    }

    func calendarView(calendarView: FMUICalendarView, didSelectDate date: NSDate) {
        FMStateful.setData(date)
        CalendarViewController.date = date
        MenuViewController.unwrap(self)?.performStateChange(.ProgressDay)
    }
    
    func calendarView(calendarView: FMUICalendarView, activeDaysForMonth monthDate: NSDate) -> [Int] {
        let results = TrialEvent.fetchMonth(monthDate)
        var comps: NSDateComponents!
        var days: [Int] = []
        for result in results {
            comps = FMUtility.calendar.components(FMUtility.calendarParts, fromDate: result.valueForKey("date") as! NSDate)
            if days.indexOf(comps.day) == nil {
                days.append(comps.day)
            }
        }
        return days
    }
    
    func calendarView(calendarView: FMUICalendarView, dateRange defaultDate: NSDate) -> (from: NSDate, till: NSDate){
        let dateMin: NSDate = TrialEvent.fetchEdgeEvent(true)?.valueForKey("date") as? NSDate ?? defaultDate
        let dateMax: NSDate = TrialEvent.fetchEdgeEvent(false)?.valueForKey("date") as? NSDate ?? defaultDate
        
        return (dateMin, dateMax)
    }
    
}
