//
//  HelpViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 24.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class HelpViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        data = []
        for i in TrialType.Stub.range() {
            data.append(TrialType(rawValue: i)!.fetchConfig())
        }
        
        tableView.tableFooterView = UIView(frame: CGRectZero)
        tableView.tintColor = UIColor(rgba: "#6BBD82")
        tableView.rowHeight = 44
    }
    
    var data: [TrialConfig]!
    var imageViewFrame: CGRect!
    let imageWidth: CGFloat = 30
    
    override func viewDidAppear(animated: Bool) {
        FMStateful.setState(.Help)
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count + 1
    }
    
    override func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: CGRectMake(0, 0, self.view.frame.width, 10))
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cellHelp")!
        if indexPath.row > 0 {
            cell.textLabel?.text = data[indexPath.row - 1].title
            cell.imageView?.image = UIImage(named: data[indexPath.row - 1].iconName)?.resize(imageWidth, height: imageWidth)
            cell.backgroundColor = UIColor.clearColor()
        } else {
            cell.textLabel?.text = "Watch tour"
            cell.imageView?.image = UIImage(named: "play_tour")?.resize(imageWidth, height: imageWidth)
            cell.backgroundColor = UIColor(rgba: "#407c93")
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if indexPath.row > 0 {
            FMStateful.setData(indexPath.row - 1)
            MenuViewController.unwrap(self)?.performStateChange(.HelpTrial)
        } else {
            Flurry.logEvent("showIntro")
            MenuViewController.unwrap(self)?.performStateChange(.Tour)
        }
    }
    
}
