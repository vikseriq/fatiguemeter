//
//  FMHealthManager.swift
//  FatigueMeter
//
//  Created by vikseriq on 28.08.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import Foundation
import HealthKit

@available(iOS 8.0, *)
private let _FMHealthManager = FMHealthManager()

@available(iOS 8.0, *)
class FMHealthManager: NSObject {
   
    class var sharedInstance: FMHealthManager? {
        get {
            if AppDelegate.iOS7 || !HKHealthStore.isHealthDataAvailable() {
                if AppDelegate.debug {
                    print("No HK on Device")
                }
                return nil
            } else {
                return _FMHealthManager
            }
        }
    }
    
    let healthStore: HKHealthStore = HKHealthStore()
    let weightQuantity = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)
    let heightQuantity = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeight)
    
    func authorize(completion: (error: String) -> Void){
        var writingTypes = Set<HKSampleType>()
        writingTypes.insert(weightQuantity!)
        writingTypes.insert(heightQuantity!)
        
        var readingTypes = Set<HKObjectType>()
        readingTypes.insert(HKObjectType.characteristicTypeForIdentifier(HKCharacteristicTypeIdentifierDateOfBirth)!)
        readingTypes.insert(HKObjectType.characteristicTypeForIdentifier(HKCharacteristicTypeIdentifierBiologicalSex)!)
        readingTypes.insert(weightQuantity!)
        readingTypes.insert(heightQuantity!)
        
        healthStore.requestAuthorizationToShareTypes(writingTypes, readTypes: readingTypes) { (success, error) -> Void in
            completion(error: error?.description ?? "")
        }
    }
    
    func readProfileData() -> (age: Int, sex: Int){
        var age: Int = 0
        var sex: Int = 0
        
        do {
            let birthday = try healthStore.dateOfBirth()
            let diff = NSCalendar.currentCalendar().components(.Year, fromDate: birthday, toDate: NSDate(), options: [])
            age = diff.year
        } catch {
            age = 0
        }
        
        do {
            let bioSex = try healthStore.biologicalSex()
            switch bioSex.biologicalSex {
            case .Female:
                sex = 2
            case .Male:
                sex = 1
            default:
                sex = 0
            }
        } catch {
            sex = 0
        }
        
        return (age, sex)
    }
    
    func readWeight(callback: (Double?) -> Void){
        fetchLatestSample(weightQuantity!, completion: { (error, sample) -> Void in
            if error != nil {
                print("HK: Can't read weight: \(error?.localizedDescription)")
                callback(nil)
                return
            }
            if let weight = sample as? HKQuantitySample {
                let kg = weight.quantity.doubleValueForUnit(HKUnit.gramUnitWithMetricPrefix(HKMetricPrefix.Kilo))
                callback(kg)
            } else {
                print("HK: No weight data")
                callback(nil)
            }
        })
    }
    
    func readHeight(callback: (Double?) -> Void){
        fetchLatestSample(heightQuantity!, completion: { (error, sample) -> Void in
            if error != nil {
                print("HK: Can't read height: \(error?.localizedDescription)")
                callback(nil)
                return
            }
            if let height = sample as? HKQuantitySample {
                let m = height.quantity.doubleValueForUnit(HKUnit.meterUnit())
                callback(m)
            } else {
                print("HK: No height data")
                callback(nil)
            }
        })
    }
    
    func writeWeight(value: Double){
        let date = NSDate()
        let quantity = HKQuantity(unit: HKUnit.poundUnit(), doubleValue: value)
        let sample = HKQuantitySample(type: weightQuantity!, quantity: quantity, startDate: date, endDate: date)
        healthStore.saveObject(sample) { (success, error) -> Void in
            if error != nil {
                print("HK: Can't write weight: \(error!.localizedDescription)")
            }
        }
    }
    
    func writeHeight(value: Double){
        let date = NSDate()
        let quantity = HKQuantity(unit: HKUnit.footUnit(), doubleValue: value)
        let sample = HKQuantitySample(type: heightQuantity!, quantity: quantity, startDate: date, endDate: date)
        healthStore.saveObject(sample) { (success, error) -> Void in
            if error != nil {
                print("HK: Can't write height: \(error!.localizedDescription)")
            }
        }
    }
    
    func fetchLatestSample(sampleType: HKSampleType, completion: (NSError?, HKSample?) -> Void){
        let predicate = HKQuery.predicateForSamplesWithStartDate(NSDate.distantPast(), endDate: NSDate(), options: .None)
        let sort = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)
        
        let query = HKSampleQuery(sampleType: sampleType, predicate: predicate, limit: 1, sortDescriptors: [sort]) { (query, result, error) -> Void in
            if error != nil {
                completion(error, nil)
                return
            }
            
            let sample = result!.first as? HKQuantitySample
            completion(nil, sample)
        }
        
        healthStore.executeQuery(query)
    }
    
}
