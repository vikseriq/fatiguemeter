//
//  FMUIGradientTableView.swift
//  FatigueMeter
//
//  Created by vikseriq on 29.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class FMUIGradientTableView: UITableView {
    
    var startColor: UIColor = FMUtility.gradientColorDark {
        didSet {
            setupView()
        }
    }
    
    var endColor: UIColor = FMUtility.gradientColorLight {
        didSet {
            setupView()
        }
    }
    
    private func setupView(){
        let colors: Array = [startColor.CGColor, endColor.CGColor]
        
        gradientLayer.colors = colors
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 0.1)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 1)
        
        self.setNeedsDisplay()
    }
    
    var gradientLayer: CAGradientLayer {
        return layer as! CAGradientLayer
    }
    
    override class func layerClass()->AnyClass{
        return CAGradientLayer.self
    }
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }

}
