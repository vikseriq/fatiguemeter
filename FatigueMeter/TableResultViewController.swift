//
//  TableResultViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 16.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class TableResultViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Test result"
        tableReport.registerNib(UINib(nibName: "FMUIResultTableViewCell", bundle: nil), forCellReuseIdentifier: reusableResultCell)
        imageRecommend = UIImage(named: "menu_recommendations")?.resize(40, height: 40)
    }

    @IBOutlet weak var tableReport: UITableView!
    @IBOutlet weak var labelHead: UILabel!
    @IBOutlet weak var labelSubhead: UILabel!
    
    var event: TrialEvent!
    var lastViewedEvent: TrialEvent! = nil
    var results: [TrialEventManaged] = []
    let sectionCellHeight: [CGFloat] = [130, 60, 80]
    let reusableResultCell = "cellResult"
    var closeBtn: UIButton!
    var imageRecommend: UIImage!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        if FMStateful.now() == .ProgressLast {
            // display like modal
            let imageBack = UIImage(named: "close")
            closeBtn = UIButton()
            closeBtn.setImage(imageBack, forState: .Normal)
            closeBtn.imageView?.contentMode = UIViewContentMode.ScaleAspectFit
            closeBtn.addTarget(self, action: Selector("goHome"), forControlEvents: UIControlEvents.TouchUpInside)
            closeBtn.frame = CGRectMake(0, 0, 25, 30)
            
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: closeBtn)
            
            results = TrialEvent.fetchDay(NSDate())
            if results.count > 0 {
                event = TrialEvent(managed: results[0])
            }
        } else {
            let eventShared = FMStateful.getData() as? TrialEventManaged
            
            if eventShared != nil {
                // comes from DayResult
                event = TrialEvent(managed: eventShared!)
                results = TrialEvent.fetchDayPart(event.date)
            } else if lastViewedEvent != nil {
                // comes from popController Help/Recommend
                event = lastViewedEvent
                results = TrialEvent.fetchDayPart(event.date)
            } else {
                results = []
            }
        }
        
        if event == nil {
            return
        }
        
        lastViewedEvent = event
        FMStateful.setState(FMStates.ProgressDetail)
        // setup date for navigating back to DayResult if acceptable
        FMStateful.setData(lastViewedEvent.date)
        
        let resultFormat = RecommendModel.getByFatigue(event.fatigue)
        labelHead.text = resultFormat.title
        labelHead.textColor = resultFormat.color
        labelSubhead.text = resultFormat.text
        tableReport.reloadData()
    }
    
    func goHome(){
        FMStateful.setData(HomeViewController.flagTestFinished)
        MenuViewController.unwrap(self)?.performStateChange(.Home)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell
        switch indexPath.section {
        case 0:
            cell = tableReport.dequeueReusableCellWithIdentifier("tableChart")!
            cell.opaque = false
            cell.backgroundColor = UIColor.clearColor()
            (cell as! TableChartCell).setup(results)
            if results.count <= 1 {
                // allow selection when no results – it will open Recommendation with HowTo text
                cell.selectionStyle = UITableViewCellSelectionStyle.Blue
                cell.userInteractionEnabled = true
            }
        case 1:
            cell = tableReport.dequeueReusableCellWithIdentifier("tableRecommend")!
            cell.imageView?.image = imageRecommend
        default:
            cell = tableReport.dequeueReusableCellWithIdentifier(reusableResultCell)!
            cell.backgroundColor = UIColor.clearColor()
            var resultValue = event.results.first
            for res in event.results {
                if res.trialType.rawValue == indexPath.row {
                    resultValue = res
                    break
                }
            }
            (cell as! FMUIResultTableViewCell).setupView(resultValue)
        }
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return indexPath.section == 2 ? UITableViewAutomaticDimension : sectionCellHeight[indexPath.section]
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return sectionCellHeight[indexPath.section]
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 2 ? (event == nil ? 0 : event.results.count) : 1
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView(frame: (section == 0 ? CGRectZero : CGRectMake(0, 0, 100, 100)))
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if indexPath.section == 1 || (indexPath.section == 0 && results.count <= 1) {
            // recommendation
            Flurry.logEvent("showRecommend", withParameters: ["source": "reportTop"])
            FMStateful.setData(event)
            MenuViewController.unwrap(self)?.performStateChange(.RecommendSet)
        } else if indexPath.section == 2 {
            // trial description
            FMStateful.setData(event.results[indexPath.row].trialType.rawValue)
            MenuViewController.unwrap(self)?.performStateChange(.HelpTrial)
        }
    }
    
}

class TableChartCell: UITableViewCell {
    
    @IBOutlet weak var chart: FMUIGlassyChartView!
    
    func setup(data: [TrialEventManaged]){
        chart.headlineDetail = data.first!.date.asString(.ShortStyle, timeStyle: .NoStyle)
        chart.setupView(FMUtility.eventsToPoints(data))
    }
    
}
