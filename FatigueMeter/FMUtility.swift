//
//  FMUtility.swift
//  FatigueMeter
//
//  Created by vikseriq on 07.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import Foundation
import UIKit

class FMUtility {
    
    class func random(range: Range<Int>) -> Int {
        return range.startIndex + Int(arc4random_uniform(UInt32(range.endIndex - range.startIndex)))
    }
    
    class func random(from: Double, to: Double) -> Double {
        return  from + Double(arc4random()) / Double(UInt32.max) * (to - from)
    }
    
    static let colorActive = UIColor(rgba: "#00B56E")
    static let colorWhite = UIColor(rgba: "#FFFFFF")
    static let gradientColorLight = UIColor(rgba: "#44b984")
    static let gradientColorDark = UIColor(rgba: "#296887")
    static let calendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
    static let calendarParts: NSCalendarUnit = [.Year, .Month, .Day, .Hour, .Minute, .Second]
    static var formatter = NSDateFormatter()
    static var locale = NSLocale(localeIdentifier: "en-US")

    class func systemFont(size: CGFloat) -> UIFont {
        return UIFont.systemFontOfSize(size)
    }
    
    // Populate points array based on TrialEvents result for displaying in chart
    class func eventsToPoints(results: [TrialEventManaged]?) -> [NSTimeInterval: Double] {
        if results == nil || results!.count == 0 {
            return [:]
        }
        var res = [NSTimeInterval: Double](minimumCapacity: results!.count)
        for i in results! {
            if i.fatigue.doubleValue <= 0 {
                continue
            }
            res[i.date.timeIntervalSince1970] = i.fatigue.doubleValue
        }
        return res
    }
    
    static func shake(view: UIView, times: Int, ltr: Bool){
        UIView.animateWithDuration(0.03, animations: { () -> Void in
            view.transform = CGAffineTransformMakeTranslation(5 * (ltr ? 1: -1), 0)
        }) { (Bool) -> Void in
            view.transform = CGAffineTransformIdentity
            if times > 0 {
                FMUtility.shake(view, times: times-1, ltr: !ltr)
            }
        }
    }
    
}

extension Array {
    func average() -> Double {
        var sum: Double = 0
        if self.count == 0 {
            return sum
        }
        for i in 0..<self.count {
            sum += self[i] as! Double
        }
        return sum / Double(self.count)
    }
    
    func sum() -> Double {
        var sum: Double = 0
        for i in 0..<self.count {
            sum += self[i] as! Double
        }
        return sum
    }
}

extension String {
    subscript (i: Int) -> Character {
        return self[self.startIndex.advancedBy(i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    subscript (r: Range<Int>) -> String {
        return substringWithRange(Range(start: startIndex.advancedBy(r.startIndex), end: startIndex.advancedBy(r.endIndex)))
    }
    
    func replace(target: String, withString: String) -> String {
        return self.stringByReplacingOccurrencesOfString(target, withString: withString, options: NSStringCompareOptions.LiteralSearch, range: nil)
    }
    
    func regexMatches(regex: String!) -> [String] {
        let regex = try! NSRegularExpression(pattern: regex, options: [])
        let _text = self as NSString
        let results = regex.matchesInString(self, options: [], range: NSMakeRange(0, _text.length))
        var matches = [String]()
        for checkResult in results {
            for i in 0..<checkResult.numberOfRanges {
                matches.append(_text.substringWithRange(checkResult.rangeAtIndex(i)))
            }
        }
        return matches
    }
}

extension NSDate {
    func asString(format format: String) -> String {
        FMUtility.formatter.dateFormat = NSDateFormatter.dateFormatFromTemplate(format, options: 0, locale: FMUtility.locale)
        FMUtility.formatter.locale = FMUtility.locale
        return FMUtility.formatter.stringFromDate(self)
    }
    
    func asString(dateStyle: NSDateFormatterStyle, timeStyle: NSDateFormatterStyle) -> String {
        FMUtility.formatter.timeStyle = timeStyle
        FMUtility.formatter.dateStyle = dateStyle
        FMUtility.formatter.locale = FMUtility.locale
        return FMUtility.formatter.stringFromDate(self)
    }
    
    func midnight() -> NSDate {
        return midnight(0)
    }
    
    func midnight(offsetDays: Int) -> NSDate {
        let comps = FMUtility.calendar.components(FMUtility.calendarParts, fromDate: self)
        comps.hour = 0
        comps.minute = 0
        comps.second = 0
        comps.day = comps.day + offsetDays
        return FMUtility.calendar.dateFromComponents(comps)!
    }
}

extension UIColor {
    convenience init(rgba: String) {
        var red:   CGFloat = 0.0
        var green: CGFloat = 0.0
        var blue:  CGFloat = 0.0
        var alpha: CGFloat = 1.0
        
        if rgba.hasPrefix("#") {
            let index   = rgba.startIndex.advancedBy(1)
            let hex     = rgba.substringFromIndex(index)
            let scanner = NSScanner(string: hex)
            var hexValue: CUnsignedLongLong = 0
            if scanner.scanHexLongLong(&hexValue) {
                switch (hex.characters.count) {
                case 3:
                    red   = CGFloat((hexValue & 0xF00) >> 8)       / 15.0
                    green = CGFloat((hexValue & 0x0F0) >> 4)       / 15.0
                    blue  = CGFloat(hexValue & 0x00F)              / 15.0
                case 4:
                    red   = CGFloat((hexValue & 0xF000) >> 12)     / 15.0
                    green = CGFloat((hexValue & 0x0F00) >> 8)      / 15.0
                    blue  = CGFloat((hexValue & 0x00F0) >> 4)      / 15.0
                    alpha = CGFloat(hexValue & 0x000F)             / 15.0
                case 6:
                    red   = CGFloat((hexValue & 0xFF0000) >> 16)   / 255.0
                    green = CGFloat((hexValue & 0x00FF00) >> 8)    / 255.0
                    blue  = CGFloat(hexValue & 0x0000FF)           / 255.0
                case 8:
                    red   = CGFloat((hexValue & 0xFF000000) >> 24) / 255.0
                    green = CGFloat((hexValue & 0x00FF0000) >> 16) / 255.0
                    blue  = CGFloat((hexValue & 0x0000FF00) >> 8)  / 255.0
                    alpha = CGFloat(hexValue & 0x000000FF)         / 255.0
                default:
                    print("Invalid RGB string")
                }
            } else {
                print("Scan hex error")
            }
        } else {
            print("Invalid RGB string, missing '#' as prefix")
        }
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
}

extension UIImage {
    func tintWithColor(color:UIColor, blendMode: CGBlendMode)->UIImage {
        
        UIGraphicsBeginImageContext(self.size)
        let context = UIGraphicsGetCurrentContext()
        
        // flip the image
        CGContextScaleCTM(context, 1.0, -1.0)
        CGContextTranslateCTM(context, 0.0, -self.size.height)
        
        // multiply blend mode
        CGContextSetBlendMode(context, blendMode)
        
        let rect = CGRectMake(0, 0, self.size.width, self.size.height)
        CGContextClipToMask(context, rect, self.CGImage)
        color.setFill()
        CGContextFillRect(context, rect)
        
        // create uiimage
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
        
    }
    
    func resize(width: CGFloat, height: CGFloat) -> UIImage {
        let rect = CGRectMake(0.0, 0.0, width, height)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, UIScreen.mainScreen().scale)
        self.drawInRect(rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
}