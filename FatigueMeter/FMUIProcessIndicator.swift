//
//  FMUIProcessIndicator.swift
//  FatigueMeter
//
//  Created by vikseriq on 24.08.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class FMUIProcessIndicator: UIView {

    var activityIndicator: UIActivityIndicatorView!
    let size: CGFloat = 80

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init(view: UIView){
        super.init(frame: CGRectMake(0, 0, view.bounds.width, view.bounds.height))
        self.hidden = true
        self.backgroundColor = UIColor(rgba: "#FFFFFF44")
        self.userInteractionEnabled = true
        view.addSubview(self)
        setupView()
    }
    
    func setupView(){
        let backgroundView = UIView(frame: CGRectMake((self.bounds.width - size) / 2, (self.bounds.height - size) / 2, size, size))
        backgroundView.backgroundColor = UIColor(rgba: "#44444455")
        backgroundView.clipsToBounds = true
        backgroundView.layer.cornerRadius = 10
        backgroundView.userInteractionEnabled = true
        
        let indicatorOffset: CGFloat = size / 2 - size / 4
        activityIndicator = UIActivityIndicatorView(frame: CGRectMake(indicatorOffset, indicatorOffset, size / 2, size / 2))
        activityIndicator.activityIndicatorViewStyle = .WhiteLarge
        
        backgroundView.addSubview(activityIndicator)
        self.addSubview(backgroundView)
    }

    func show(){
        self.hidden = false
        activityIndicator.startAnimating()
    }
    
    func dismiss(){
        activityIndicator.stopAnimating()
        self.hidden = true
    }
    
}
