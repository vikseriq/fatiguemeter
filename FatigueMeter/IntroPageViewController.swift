//
//  IntroPageViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 06.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class IntroPageViewController: UIViewController {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    var screenId: Int = 0
    var placed: Bool = false
    var header: String = "Title"
    var image: String? = nil
    var desc: String = "Subtitle"
    var gradientA: UIColor = UIColor.whiteColor()
    var gradientB: UIColor = UIColor.blackColor()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        pageTitle.text = header
        pageDesc.text = desc
        if image != nil {
            pageImage.image = UIImage(named: image!)
        }
        (self.view as! FMUIGradientView).startColor = gradientA
        (self.view as! FMUIGradientView).endColor = gradientB
    }
    
    @IBOutlet weak var pageTitle: UILabel!
    @IBOutlet weak var pageDesc: UILabel!
    @IBOutlet weak var pageImage: UIImageView!
    
}
