//
//  HelpTrialViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 07.05.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class HelpTrialViewController: UIViewController {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var textContent: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let ti = FMStateful.getData() as? Int {
            if let tc = TrialType(rawValue: ti)?.fetchConfig() {
                self.title = tc.title
                labelTitle.text = tc.fullTitle
                textContent.text = tc.detailText
                
                let _image = UIImage(named: tc.iconName)
                if let image = _image {
                    imageView.image = image
                    imageView.hidden = false
                    imageView.sizeToFit()
                } else {
                    imageView.hidden = true
                    imageHeightConstraint.constant = 0
                }
                
                Flurry.logEvent("showHelpItem", withParameters: ["code": tc.key])
            } else {
                print("No description for selected trial")
            }
        }
    }

}
