//
//  FMUICalendarCellView.swift
//  FatigueMeter
//
//  Created by vikseriq on 22.05.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class FMUICalendarCellView: UIControl {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    static let colorPrimary = UIColor(rgba: "#2d7070")
    static let colorActive = UIColor(rgba: "#ffffff")
    static let colorMuted = UIColor(rgba: "#ffffffCC")
    static let colorSecondary = UIColor(rgba: "#ffffff33")
    static let colorBorder = UIColor(rgba: "#ffffff1A")
    static let font = FMUtility.systemFont(16)
    let two: CGFloat = 2
    var labelDay: UILabel!
    
    var text: String = "" {
        didSet {
            labelDay.text = text
        }
    }
    var isActive: Bool = false {
        didSet {
            updateView()
        }
    }
    var isCurrent: Bool = false {
        didSet {
            updateView()
        }
    }
    override var highlighted: Bool {
        get {
            return super.highlighted
        }
        set {
            super.highlighted = newValue
            updateView()
        }
    }
    
    func setupView(){
        backgroundColor = UIColor.clearColor()
        let w = self.frame.width * 0.85
        labelDay = UILabel(frame: CGRectMake((self.frame.width - w) / two, (self.frame.height - w) / two, w, w))
        labelDay.font = FMUICalendarCellView.font
        labelDay.textAlignment = .Center
        labelDay.layer.cornerRadius = labelDay.frame.width / two
        labelDay.layer.masksToBounds = true
        self.addSubview(labelDay)
        
        setNeedsDisplay()
    }
    
    func updateView(){
        let colored = isCurrent || highlighted
        labelDay.textColor = colored ? FMUICalendarCellView.colorPrimary :
            isActive ? FMUICalendarCellView.colorActive : FMUICalendarCellView.colorMuted
        labelDay.backgroundColor = colored ? FMUICalendarCellView.colorActive :
            isActive ? FMUICalendarCellView.colorSecondary : UIColor.clearColor()
        setNeedsDisplay()
    }
    
    override func drawRect(rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        
        CGContextSetStrokeColorWithColor(context, FMUICalendarCellView.colorBorder.CGColor)
        CGContextSetLineWidth(context, two)
        
        CGContextMoveToPoint(context, self.frame.width, 0)
        CGContextAddLineToPoint(context, 0, 0)
        CGContextStrokePath(context)
    }
    
}
