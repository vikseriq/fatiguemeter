//
//  TrialSequenceNumpadView.swift
//  FatigueMeter
//
//  Created by vikseriq on 15.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

@IBDesignable class TrialSequenceNumpadView: UIView {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    @IBInspectable var flush: Bool = false {
        didSet {
            setupView()
        }
    }
    
    let colorNormal = UIColor(rgba: "#FAFAFB")
    let colorActive = UIColor(rgba: "#6BBD82")
    let colorBorder = UIColor(rgba: "#BCC0C5")
    let colorText = UIColor(rgba: "#000000")
    
    var delegate: FMNumpadDelegate!
    var inited = false
    var sequence: [String] = []
    
    func cleanUp(){
        for view in subviews {
            view.removeFromSuperview()
        }
        setNeedsDisplay()
    }
    
    func setupView(){
        
        let rect: CGRect = bounds
        cleanUp()
        
        if delegate != nil {
            sequence = delegate.sequencePad
        } else {
            sequence = []
            for i in 1...20 {
                sequence.append(String(i))
            }
            sequence.append("A")
            sequence.append("B")
            sequence.append("C")
            sequence.append("D")
            sequence.append("E")
        }
        sequence.sortInPlace {
            (_,_) in arc4random() < arc4random()
        }
        
        
        let tileW: CGFloat = rect.width / 5
        let tileH: CGFloat = rect.height / 5
        let border: CGFloat = 1
        var buttonFrame = CGRectMake(0, 0, tileW, tileH)
        let font = UIFont(name: "HelveticaNeue", size: 25)
        var btn: UIButton!
        
        for i in 0..<sequence.count {
            buttonFrame.origin.x = CGFloat(i % 5) * (tileW + border)
            buttonFrame.origin.y = floor(CGFloat(i) / 5) * (tileH + border)
            btn = UIButton(frame: buttonFrame)
            btn.titleLabel?.font = font
            btn.setTitle(sequence[i], forState: UIControlState.Normal)
            btn.setTitleColor(colorText, forState: UIControlState.Normal)
            btn.titleLabel?.textAlignment = NSTextAlignment.Center
            btn.backgroundColor = colorNormal
            btn.addTarget(self, action: Selector("onPress:"), forControlEvents: UIControlEvents.TouchUpInside)
            self.addSubview(btn)
        }
        
        self.backgroundColor = colorBorder
        
        inited = true
        setNeedsDisplay()
    }
    
    
    func onPress(button: UIButton){
        if delegate == nil || button.backgroundColor == colorActive {
            return
        }
        var active = false
        var label = ""
        if let lbl = button.titleLabel {
            label = lbl.text!
        }
        
        if delegate.buttonPressed(label) {
            active = true
        } else {
            active = false
        }
        button.backgroundColor = active ? colorActive : colorNormal
        button.setTitleColor(active ? colorNormal : colorText, forState: UIControlState.Normal)
    }
    
}

protocol FMNumpadDelegate {
    
    var sequencePad: [String] { get }
    func buttonPressed(value: String) -> Bool

}