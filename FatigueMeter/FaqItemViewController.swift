//
//  FaqItemViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 25.08.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class FaqItemViewController: UIViewController {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var textContent: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let faq = FMStateful.getData() as? FaqModel {
            labelTitle.text = faq.title
            textContent.text = faq.text
            
            Flurry.logEvent("showFAQItem", withParameters: ["id": faq.id])
        }
        
    }
    
}
