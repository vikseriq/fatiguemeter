//
//  FMUIRowView.swift
//  FatigueMeter
//
//  Created by vikseriq on 25.05.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

@IBDesignable class FMUIRowView: UIControl {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
    }
    
    @IBInspectable var placeholder: String = "Value" {
        didSet {
            setupView()
        }
    }
    
    @IBInspectable var keyTitle: String = "Key" {
        didSet {
            setupView()
        }
    }
    
    @IBInspectable var value: String = "" {
        didSet {
            setupView()
        }
    }
    
    @IBInspectable var passwordMode: Bool = false {
        didSet {
            setupView()
        }
    }
    
    @IBInspectable var borderBottom: Bool = false {
        didSet {
            setupView()
        }
    }

    let gap: CGFloat = 2
    let paddingLeft: CGFloat = 15
    let keyColumntWidth: CGFloat = 100
    let fontKey = UIFont(name: "HelveticaNeue", size: 17)
    let fontValue = UIFont(name: "HelveticaNeue", size: 15)
    let colorGray = UIColor.lightGrayColor()
    
    var labelKey: UILabel!
    var textValue: UITextField!
    
    func initView(){
        labelKey = UILabel(frame: CGRectMake(paddingLeft, gap, keyColumntWidth - paddingLeft, bounds.height - 2 * gap))
        labelKey.font = fontKey
        labelKey.text = "Key"
        addSubview(labelKey)
        
        textValue = UITextField(frame: CGRectMake(keyColumntWidth, gap, bounds.width - keyColumntWidth, bounds.height - 2 * gap))
        textValue.borderStyle = UITextBorderStyle.None
        textValue.font = fontValue
        textValue.placeholder = ""
        textValue.returnKeyType = UIReturnKeyType.Next
        textValue.clearButtonMode = UITextFieldViewMode.WhileEditing
        textValue.keyboardType = UIKeyboardType.Default
        textValue.addTarget(self, action: "inputDone", forControlEvents: UIControlEvents.EditingDidEndOnExit)
        addSubview(textValue)
        
        setupView()
    }
    
    func setupView(){
        if labelKey == nil {
            initView()
        }
        
        labelKey.text = keyTitle
        textValue.placeholder = placeholder
        textValue.keyboardType = placeholder.rangeOfString("@") == nil ? .Default : .EmailAddress
        textValue.text = value
        textValue.secureTextEntry = passwordMode
        
        setNeedsDisplay()
    }
    
    override func becomeFirstResponder() -> Bool {
        return textValue.becomeFirstResponder()
    }
    
    func getValue() -> String {
        return textValue.text!
    }
    
    func inputDone(){
        sendActionsForControlEvents(UIControlEvents.EditingDidEndOnExit)
    }
    
    func shakeBox(){
        FMUtility.shake(textValue, times: 4, ltr: true)
    }
    
    override func drawRect(rect: CGRect) {
        if !borderBottom {
            return
        }
        
        let context = UIGraphicsGetCurrentContext()
        colorGray.setStroke()
        CGContextSetLineWidth(   context, 1)
        CGContextMoveToPoint(    context, frame.minX + 15, frame.maxY - 1)
        CGContextAddLineToPoint( context, frame.maxX, frame.maxY - 1)
        CGContextStrokePath(     context)
    }
    
}
