//
//  FMUIChevronButton.swift
//  FatigueMeter
//
//  Created by vikseriq on 23.05.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class FMUIChevronButton: UIControl {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    var textTitle: String! {
        didSet {
            labelText.text = textTitle
        }
    }
    
    var textDetail: String! {
        didSet {
            labelDetail.text = textDetail
        }
    }
    
    var chevron: Bool = false {
        didSet {
            imageChevron.hidden = !chevron
            if (self.enabled != chevron){
                labelDetail.frame.offsetInPlace(dx: frame.width * 0.05 * (chevron ? -1 : 1), dy: 0)
            }
            self.enabled = chevron
        }
    }
    
    override var highlighted: Bool {
        get {
            return super.highlighted
        }
        set {
            super.highlighted = newValue
            labelText?.highlighted = newValue
            imageChevron?.highlighted = newValue
        }
    }
    
    var labelText: UILabel!
    var labelDetail: UILabel!
    var imageChevron: UIImageView!
    
    func setupView(){
        let font = UIFont(name: "HelveticaNeue", size: 16)
        let grayColor = UIColor(rgba: "#c3c6ca")
        
        labelText = UILabel(frame: CGRectMake(0, 0, frame.width * 0.5, frame.height))
        labelText.textColor = UIColor.blackColor()
        labelText.highlightedTextColor = FMUtility.colorActive
        labelText.font = font
        addSubview(labelText)
        
        labelDetail = UILabel(frame: CGRectMake(frame.width * 0.6, 0, frame.width * 0.35, frame.height))
        labelDetail.textColor = grayColor
        labelDetail.font = font
        labelDetail.textAlignment = .Right
        addSubview(labelDetail)
        
        let image = UIImage(named: "accessory_white")!
        imageChevron = UIImageView(image: image.tintWithColor(grayColor, blendMode: .Multiply))
        imageChevron.highlightedImage = image.tintWithColor(FMUtility.colorActive, blendMode: .Multiply)
        imageChevron.contentMode = UIViewContentMode.ScaleAspectFit
        imageChevron.frame = CGRectMake(frame.width - 10, (frame.height - 15) / 2.0, 10, 15)
        addSubview(imageChevron)
        
        chevron = false
    }
    
    
}
