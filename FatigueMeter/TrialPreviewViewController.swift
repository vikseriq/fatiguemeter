//
//  TrialPreviewViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 09.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class TrialPreviewViewController: UIViewController {
    
    var trialConfig: TrialConfig!
    var callerVC: FMPreviewDelegate!
    var currentImage = 0
    var imageTimer: NSTimer!
    var images: [UIImage] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.hidden = true
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        trialImage.tintColor = UIColor.whiteColor()
        
        if trialConfig.images.count == 1 {
            trialImage.image = UIImage(named: trialConfig.images[0])!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        } else if trialConfig.images.count > 1 {
            currentImage = 0
            images = []
            for i in 0..<trialConfig.images.count {
                images.append(UIImage(named: trialConfig.images[i])!)
            }
            trialImage.image = images[0]
        }
        
        trialImage.frame = CGRectMake(trialImage.bounds.origin.x, trialImage.bounds.origin.y, self.view.bounds.width, self.view.bounds.height * CGFloat(0.3))
        trialImage.setNeedsLayout()
        trialImage.alpha = 0
        
        trialDesc.text = trialConfig.previewText
        trialDesc.alpha = 0
        trialButton.alpha = 0
        
        view.hidden = false
    }
    
    override func viewDidAppear(animated: Bool) {
        // fancy animations
        UIView.animateWithDuration(0.5, animations: {
            self.trialDesc.alpha = 1
            }) { (Bool) -> Void in
                UIView.animateWithDuration(0.5, animations: {
                    self.trialImage.alpha = 1
                    }, completion: { (Bool) -> Void in
                        UIView.animateWithDuration(0.3, animations: {
                            self.trialButton.alpha = 1
                        })
                })
        }
        if trialConfig.images.count > 1 {
            changeImage()
        }
    }
    
    func changeImage(){
        let interval = Double(trialConfig.previewSpeed) / 1000.0
        trialImage.image = images[currentImage].imageWithRenderingMode(UIImageRenderingMode.Automatic)
        currentImage++
        if currentImage >= images.count {
            currentImage = 0
        }
        imageTimer = NSTimer.scheduledTimerWithTimeInterval(interval, target: self, selector: Selector("changeImage"), userInfo: nil, repeats: false)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        imageTimer?.invalidate()
    }
    
    @IBOutlet weak var trialButton: FMUIRoundButton!
    @IBOutlet weak var trialDesc: UILabel!
    @IBOutlet weak var trialImage: UIImageView!
    @IBAction func startButton(sender: AnyObject) {
        callerVC?.previewClosing()
    }
    
}

protocol FMPreviewDelegate {
    
    func previewClosing()
    
}
