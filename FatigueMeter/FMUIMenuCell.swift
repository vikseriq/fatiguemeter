//
//  FMUIMenuCell.swift
//  FatigueMeter
//
//  Created by vikseriq on 24.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

@IBDesignable class FMUIMenuCell: UITableViewCell {

    @IBInspectable var badgeCount: Int = 0
    @IBInspectable var borderTop: Bool = false
    
    var imageAccessory: UIImageView!
    var badgeAccessory: UIView!
    var badgeLabel: UILabel!
    
    func setAccessory(){
        if imageAccessory == nil {
            imageAccessory = UIImageView(image: UIImage(named: "accessory_white"))
            imageAccessory.contentMode = UIViewContentMode.ScaleAspectFit
            imageAccessory.frame = CGRectMake(0, 0, 10, 15)
        }
        if badgeAccessory == nil {
            let badgeHeight: CGFloat = 22
            badgeAccessory = UIView(frame: CGRectMake(0, 0, 22, badgeHeight))
            badgeAccessory.layer.cornerRadius = badgeHeight / 2
            badgeAccessory.layer.backgroundColor = UIColor(rgba: "#FFBD00").CGColor
            badgeLabel = UILabel(frame: badgeAccessory.frame)
            badgeLabel.text = String(badgeCount)
            badgeLabel.font = UIFont(name: "System-Thin", size: 30)
            badgeLabel.textColor = UIColor.blackColor()
            badgeLabel.textAlignment = NSTextAlignment.Center
            badgeAccessory.addSubview(badgeLabel)
        }
        accessoryView = badgeCount == 0 ? imageAccessory : badgeAccessory
        
        if borderTop {
            borderTop = false
            let layerBorder = CALayer()
            layerBorder.frame = CGRectMake(0, 0, 300, 1)
            layerBorder.backgroundColor = UIColor.whiteColor().CGColor
            self.layer.addSublayer(layerBorder)
        }
        
        setNeedsDisplay()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setAccessory()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setAccessory()
    }

}
