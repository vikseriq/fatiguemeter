//
//  RecommendModel.swift
//  FatigueMeter
//
//  Created by vikseriq on 07.05.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class RecommendModel {
    
    static var variants: [Recommendation] = []
    static let fatigueGoodLevel: Double = 16
    static let fatigueCriticalLevel: Double = 60
    
    class func getByFatigue(fatigue: Double) -> Recommendation {
        if variants.count == 0 {
            loadRecommendations()
        }
        
        var f = 0
        if fatigue > 999 {
            f = 999
        } else {
            f = Int(floor(fatigue))
        }
        for v in variants {
            if v.range.startIndex <= f && v.range.endIndex >= f {
                return v
            }
        }
        return variants.last!
    }
    
    class func loadRecommendations() {
        variants = []
        
        let plistPath   = NSBundle.mainBundle().pathForResource("Values", ofType: "plist")
        let pDict       = NSDictionary(contentsOfFile: plistPath!)
        let pRecoms     = pDict!.valueForKey("Recommendations") as! Array<AnyObject>
        for pRecom in pRecoms {
            let pr = pRecom as! Dictionary<String, AnyObject>
            let rc = Recommendation(
                title:  pr["Title"] as? String ?? "",
                text:   pr["Text"] as? String ?? "",
                color:  UIColor(rgba: (pr["Color"] as? String ?? "#FF0000")),
                file:   pr["File"] as? String ?? "",
                range:  (Int(pr["RangeFrom"] as? Int ?? 999))..<(Int(pr["RangeTo"] as? Int ?? 999))
            )
            variants.append(rc)
        }
    }
    
    class func getLongtermTexts(rc: Recommendation, fatigue: Double) -> [String] {
        var texts = [String]()
        
        if let plistPath = NSBundle.mainBundle().pathForResource("long_" + rc.file, ofType: "plist"){
            if let pDict = NSDictionary(contentsOfFile: plistPath) as? Dictionary<String, String> {
                var expl = pDict["Explanation"] ?? ""
                var recs = pDict["Recommend"] ?? ""
                let half = Double(rc.range.endIndex - rc.range.startIndex) / 2
                if fatigue > half {
                    expl = pDict["Explanation2"] ?? expl
                    recs = pDict["Recommend2"] ?? recs
                }
                
                if expl.characters.count > 0 {
                    texts.append(expl)
                }
                if recs.characters.count > 0 {
                    texts.append(recs)
                }
            }
        }
        
        return texts
    }
    
    class func updateRecommendLastCount(event: TrialEvent) {
        var n = 0
        for result in event.results {
            if result.fatigue >= RecommendModel.fatigueGoodLevel {
                n++
            }
        }
        FMSettings.recommendLastCount = n
    }
    
}

struct Recommendation {
    var title: String
    var text: String
    var color: UIColor
    var file: String
    var range: Range<Int>
}