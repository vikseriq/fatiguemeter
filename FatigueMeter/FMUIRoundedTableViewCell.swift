//
//  FMUIRoundedTableViewCell.swift
//  FatigueMeter
//
//  Created by vikseriq on 02.03.16.
//  Copyright © 2016 vikseriq. All rights reserved.
//

import UIKit

class FMUIRoundedTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewBase.layer.cornerRadius = 4
        selectedBackgroundView = UIView(frame: viewBase.frame)
        selectedBackgroundView?.backgroundColor = viewBase.tintColor
        selectedBackgroundView?.layer.cornerRadius = 4
    }
    
    @IBOutlet weak var viewBase: UIView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageIcon: UIImageView!
    
    func setupView(title: String, image: UIImage){
        labelTitle.text = title
        imageIcon.image = image
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        selectedBackgroundView?.frame = viewBase.frame
    }

}
