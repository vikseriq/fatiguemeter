//
//  FMUIBadgeLabel.swift
//  FatigueMeter
//
//  Created by vikseriq on 07.05.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

@IBDesignable class FMUIBadgeLabel: UILabel {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setupView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    func setupView(){
        self.layer.cornerRadius = bounds.width / 2.0
        self.layer.backgroundColor = UIColor(rgba: "#F2CC2F").CGColor
        self.textAlignment = NSTextAlignment.Center
        self.font = UIFont(name: "HelveticaNeue", size: 14)
    }

}
