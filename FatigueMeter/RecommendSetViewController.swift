//
//  RecommendSetViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 31.08.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class RecommendSetViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelSubtitle: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var tableSet: UITableView!
    @IBOutlet weak var textRecommed: UITextView!
    @IBOutlet weak var constraintSubtitle: NSLayoutConstraint!

    let reusableRoundedCell = "cellRounded"
    var textCommon: UITextView!
    var rows: [TrialSingleResult] = []
    var trials: [TrialConfig] = []
    var subtitleConstraint: CGFloat!
    var subtitleHeight: CGFloat!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        var trialResult = FMStateful.getData() as? TrialEvent
        if trialResult == nil {
            var res = TrialEvent.fetchLastActualDay()
            if res.count > 0 {
                trialResult = TrialEvent(managed: res[0])
            }
        }
        
        if let result = trialResult {
            let formatter = NSDateFormatter()
            formatter.locale = FMUtility.locale
            formatter.dateStyle = NSDateFormatterStyle.MediumStyle
            formatter.timeStyle = NSDateFormatterStyle.MediumStyle
            labelTime.text = formatter.stringFromDate(result.date)
            
            let rc = RecommendModel.getByFatigue(result.fatigue)
            
            labelTitle.text = rc.title
            labelTitle.textColor = rc.color
            labelSubtitle.text = rc.text
            
            setLongDescription(RecommendModel.getLongtermTexts(rc, fatigue: result.fatigue))
            
            rows = []
            for single in result.results {
                if single.fatigue < RecommendModel.fatigueGoodLevel {
                    continue
                }
                rows.append(single)
                trials.append(single.trialType.fetchConfig())
            }
        } else {
            labelTitle.text = "No recommendations"
            labelSubtitle.text = "Recommendations are formed in accordance with your personal test perfomance. Please give the test more than a shot so as to recieve adequate health recommendations."
            labelTime.text = ""
            tableSet.hidden = true
        }
        
        labelSubtitle.sizeToFit()
        subtitleConstraint = constraintSubtitle.constant
        subtitleHeight = labelSubtitle.frame.height + subtitleConstraint
        
        tableSet.registerNib(UINib(nibName: "FMUIRoundedTableViewCell", bundle: nil), forCellReuseIdentifier: reusableRoundedCell)
    }
    
    override func viewDidAppear(animated: Bool) {
        FMStateful.setState(.RecommendSet)
    }
    
    func setLongDescription(blocks: [String]) {
        let attrTitle = [NSFontAttributeName: UIFont.boldSystemFontOfSize(18), NSForegroundColorAttributeName: UIColor.whiteColor()]
        let attrNormal = [NSFontAttributeName: FMUtility.systemFont(16), NSForegroundColorAttributeName: UIColor.whiteColor()]
        
        let attrText: NSMutableAttributedString = NSMutableAttributedString()
        
        if let expl = blocks.first {
            attrText.appendAttributedString(NSAttributedString(string: "Explanation\n", attributes: attrTitle))
            attrText.appendAttributedString(NSAttributedString(string: expl + "\n\n", attributes: attrNormal))
        }
        
        if let recs = blocks.last {
            attrText.appendAttributedString(NSAttributedString(string: "Recommendations\n", attributes: attrTitle))
            attrText.appendAttributedString(NSAttributedString(string: recs, attributes: attrNormal))
        }
        
        textRecommed.attributedText = attrText
        // Unsize TextView
        textRecommed.removeFromSuperview()
        textRecommed.frame = CGRectMake(0, 0, tableSet.frame.width, 0)
        // Calculate prefered height
        textRecommed.sizeToFit()
        
        let gap: CGFloat = 10
        let view = UIView(frame: CGRectMake(0, 0, tableSet.frame.width, textRecommed.frame.height))
        view.addSubview(textRecommed)
        textRecommed.frame = view.bounds
        
        // Adjust layout via constraints
        let constraints = [
            NSLayoutConstraint(item: textRecommed, attribute: .Leading, relatedBy: NSLayoutRelation.Equal,
                toItem: view, attribute: .Leading, multiplier: 1, constant: gap),
            NSLayoutConstraint(item: textRecommed, attribute: .Trailing, relatedBy: NSLayoutRelation.Equal,
                toItem: view, attribute: .Trailing, multiplier: 1, constant: -gap),
            NSLayoutConstraint(item: textRecommed, attribute: .Top, relatedBy: NSLayoutRelation.Equal,
                toItem: view, attribute: .Top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: textRecommed, attribute: .Bottom, relatedBy: NSLayoutRelation.Equal,
                toItem: view, attribute: .Bottom, multiplier: 1, constant: 0)
        ]
        view.addConstraints(constraints)
        view.layoutSubviews()

        // And set as footer
        tableSet.tableFooterView = view
    }
    
    func handleDumpingTitle(offsetTop: CGFloat){
        let offsetMin: CGFloat = 18
        let offsetMax: CGFloat = offsetMin + 64
        var scaling: CGFloat = labelSubtitle.bounds.height
        if offsetTop < offsetMin {
            scaling = 1
        } else if offsetTop < offsetMax {
            scaling = 1 - (offsetTop - offsetMin) / (offsetMax - offsetMin)
        } else {
            scaling = 0
        }
        self.constraintSubtitle.constant = self.subtitleConstraint - self.subtitleHeight * (1 - scaling)
        self.labelSubtitle.alpha = max(0, scaling)
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        handleDumpingTitle(scrollView.contentOffset.y)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rows.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let trial = trials[indexPath.row]
        let cell = tableView.dequeueReusableCellWithIdentifier(reusableRoundedCell) as! FMUIRoundedTableViewCell
        cell.backgroundColor = UIColor.clearColor()
        cell.setupView(trial.fullTitle, image: UIImage(named: trial.iconName)!)
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        FMStateful.setData(rows[indexPath.row])
        MenuViewController.unwrap(self)?.performStateChange(.RecommendItem)
    }
    
}
