//
//  TrialSequenceInputViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 15.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class TrialSequenceInputViewController: TrialBaseViewController, FMNumpadDelegate {
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        trialType = TrialType.Sequence
        needCountdown = false
        needSoundableClick = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        viewNumpad.delegate = self
    }
    
    @IBOutlet weak var viewNumpad: TrialSequenceNumpadView!
    @IBOutlet weak var viewFlow: UIView!
    @IBOutlet weak var labelLLL: UILabel!
    @IBOutlet weak var labelLL: UILabel!
    @IBOutlet weak var labelL: UILabel!
    @IBOutlet weak var labelCurrent: UILabel!
    @IBOutlet weak var labelR: UILabel!
    @IBOutlet weak var labelRR: UILabel!
    @IBOutlet weak var labelRRR: UILabel!
    @IBOutlet weak var labelLeft: UILabel!
    
    var sequenceAsc: [String] = []
    var sequencePad: [String] {
        get {
            return sequenceAsc
        }
    }
    var currentIndex = 0
    var timeStart: CFTimeInterval = 0

    
    func reset(){
        sequenceAsc = []
        for i in 1...20 {
            sequenceAsc.append(String(i))
        }
        sequenceAsc.append("A")
        sequenceAsc.append("B")
        sequenceAsc.append("C")
        sequenceAsc.append("D")
        sequenceAsc.append("E")
        
        currentIndex = 0
        moveChain()
        
        viewNumpad.hidden = true
        viewNumpad.alpha = 1
        viewFlow.hidden = true
        viewFlow.alpha = 1
    }
    
    override func runTrial() {
        super.runTrial()
        reset()
        
        viewNumpad.cleanUp()
        viewNumpad.setupView()
        viewNumpad.hidden = false
        viewFlow.hidden = false
        
        timeStart = CFAbsoluteTimeGetCurrent()
    }
    
    override func finishTrial() {
        viewNumpad.hidden = true
        viewFlow.hidden = true
        super.finishTrial()
    }
    
    func buttonPressed(value: String) -> Bool {
        let timeHit = CFAbsoluteTimeGetCurrent()
        clickSound()
        if value == sequenceAsc[currentIndex] {
            currentIndex++
            moveChain()
            if currentIndex >= sequenceAsc.count {
                trialResults = [timeHit - timeStart]
                UIView.animateWithDuration(0.5, animations: {
                    self.viewNumpad.alpha = 0
                }, completion: { (Bool) -> Void in
                    self.finishTrial()
                })
            }
            return true
        } else {
            return false
        }
    }
    
    func moveChain(){
        let left = sequenceAsc.count - currentIndex
        labelLeft.text = left > 0 ? "\(left) left" : "Done"
        if left <= 0 {
            return
        }
        
        labelLLL.text = currentIndex > 2 ? sequenceAsc[currentIndex - 3] : ""
        labelLL.text = currentIndex > 1 ? sequenceAsc[currentIndex - 2] : ""
        labelL.text = currentIndex > 0 ? sequenceAsc[currentIndex - 1] : ""
        
        labelCurrent.text = sequenceAsc[currentIndex]
        
        labelR.text = currentIndex < sequenceAsc.count - 1 ? sequenceAsc[currentIndex + 1] : ""
        labelRR.text = currentIndex < sequenceAsc.count - 2 ? sequenceAsc[currentIndex + 2] : ""
        labelRRR.text = currentIndex < sequenceAsc.count - 3 ? sequenceAsc[currentIndex + 3] : ""
    }
    
}
