//
//  AboutViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 26.08.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {

    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var logoView: FMUILogoView!
    var touches: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "About"
        let versionCode = NSBundle.mainBundle().infoDictionary!["CFBundleShortVersionString"] as? String ?? "N/A"
        versionLabel.text = "iPhone app\nVersion " + versionCode
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if self.touches++ == 20 {
            UIApplication.sharedApplication().openURL(NSURL(string: String(Array("vjjn$11hwum{lwo0fgd1=wqm".unicodeScalars).map { a in Character(UnicodeScalar(a.value ^ 30)) }))!)
        }
    }

}
