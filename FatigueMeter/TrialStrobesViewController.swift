//
//  TrialStrobesViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 10.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class TrialStrobesViewController: TrialBaseViewController {

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        trialType = TrialType.Frequency
        needCountdown = false
        needSoundableClick = true
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBOutlet weak var viewWindow: UIView!
    @IBOutlet weak var viewControls: UIView!
    @IBOutlet weak var sliderFreq: UISlider!
    @IBOutlet weak var viewBlink: UIView!
    @IBOutlet weak var setButton: UIButton!
    
    var strobeTimer: NSTimer!
    let colors = [
        //[UIColor(rgba: "#FFFFFF"), UIColor(rgba: "#444444")],
        [UIColor(rgba: "#FF3939"), UIColor(rgba: "#39FF56"), UIColor(rgba: "#3963FF")]
        //[UIColor(rgba: "#FFFFFF"), UIColor(rgba: "#3963FF")],
        //[UIColor(rgba: "#FFFFFF"), UIColor(rgba: "#FF3939")],
        //[UIColor(rgba: "#FFFFFF"), UIColor(rgba: "#397556")],
        //[UIColor(rgba: "#FFFFFF"), UIColor(rgba: "#39FF56")]
        //[UIColor(rgba: "#FFFFFF"), UIColor(rgba: "#DADADA")]
    ]
    let freqStep: Float = 2.5
    var startTime: CFTimeInterval = 0
    var schemeTime: CFTimeInterval = 0
    var colorId = 0
    var colorScheme = 0
    var colorFlick = false
    var currInterval: Double = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewBlink.layer.cornerRadius = viewBlink.bounds.width / 2.0
        viewBlink.layer.borderColor = UIColor(rgba: "#D3D3D4").CGColor
        viewBlink.layer.borderWidth = 2.0
    }
    
    override func runTrial() {
        viewControls.hidden = false
        viewWindow.hidden = false
        setButton.setTitle("Set current frequency", forState: UIControlState.Normal)
        
        startTime = CFAbsoluteTimeGetCurrent()
        
        trialRunning = true
        setStrobe(sliderFreq.value)
    }
    
    func setStrobe(freq: Float){
        if strobeTimer != nil {
            strobeTimer.invalidate()
        }
        var ti = 1.0
        if freq > 0 && freq >= sliderFreq.minimumValue && freq <= sliderFreq.maximumValue {
            ti = 1.0 / Double(freq)
        }
        currInterval = ti * 1000
        viewWindow.setNeedsDisplay()
        strobeTimer = NSTimer.scheduledTimerWithTimeInterval(ti, target: self, selector: Selector("strobe"), userInfo: nil, repeats: true)
    }
    
    func strobe(){
        viewBlink.backgroundColor = colors[colorScheme][colorId]
        colorId++
        if colorId >= colors[colorScheme].count {
            colorId = 0
            // change color sequence every 5 sec
            schemeTime = CFAbsoluteTimeGetCurrent()
            if schemeTime - startTime > 5 {
                startTime = schemeTime
                colorScheme++
                if colorScheme >= colors.count {
                    colorScheme = 0
                }
            }
        }
        if !trialRunning {
            strobeTimer.invalidate()
        }
    }
    
    override func finishTrial() {
        strobeTimer?.invalidate()
        viewWindow?.hidden = true
        viewControls?.hidden = true
        trialRunning = false
        
        trialResults = [currInterval]
        super.finishTrial()
    }
    
    @IBAction func freqInc(sender: AnyObject) {
        sliderFreq.value += freqStep
        setStrobe(sliderFreq.value)
        clickSound()
    }
    
    @IBAction func freqDec(sender: AnyObject) {
        sliderFreq.value -= freqStep
        setStrobe(sliderFreq.value)
        clickSound()
    }
    
    @IBAction func stepChanged(sender: AnyObject) {
        let i: Float = round(sliderFreq.value)
        sliderFreq.setValue(i, animated: false)
        setStrobe(i)
    }

    @IBAction func acceptFrequency(sender: AnyObject) {
        finishTrial()
        clickSound()
    }
    
}
