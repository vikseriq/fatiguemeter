//
//  FMUIRoundPill.swift
//  FatigueMeter
//
//  Created by vikseriq on 03.03.16.
//  Copyright © 2016 vikseriq. All rights reserved.
//

import UIKit

class FMUIRoundPill: UIView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func didMoveToSuperview() {
        layer.borderColor = UIColor.whiteColor().CGColor
        layer.borderWidth = 1
        layer.cornerRadius = bounds.width * 0.5
        layer.backgroundColor = self.backgroundColor?.CGColor
    }
    
}
