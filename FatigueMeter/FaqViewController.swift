//
//  FaqViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 25.08.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class FaqViewController: UITableViewController {

    var data: [FaqModel]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        data = []
        
        let plistPath = NSBundle.mainBundle().pathForResource("Values", ofType: "plist")
        let pDict = NSDictionary(contentsOfFile: plistPath!)
        if let pFaqs = pDict!.valueForKey("FAQ") as? Array<AnyObject> {
            for i in 0..<pFaqs.count {
                let pFaq = pFaqs[i] as! Dictionary<String, AnyObject>
                data.append(FaqModel(id: i, title: pFaq["Question"] as? String, text: pFaq["Answer"] as? String))
            }
        }
        
        tableView.tableFooterView = UIView(frame: CGRectZero)
        tableView.tintColor = UIColor(rgba: "#6BBD82")
    }
    
    override func viewWillAppear(animated: Bool) {
        FMStateful.setState(.Faq)
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cellFaq")!
        cell.textLabel?.text = data[indexPath.row].title
        cell.backgroundColor = UIColor.clearColor()
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        FMStateful.setData(data[indexPath.row])
        MenuViewController.unwrap(self)?.performStateChange(.FaqItem)
    }

}

class FaqModel {
    var id: Int
    var title: String
    var text: String
    
    init(id: Int, title: String?, text: String?){
        self.id = id
        self.title = title ?? ""
        self.text = text ?? ""
    }
    
}
