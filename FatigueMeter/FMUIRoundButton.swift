//
//  FMUIRoundButton.swift
//  FatigueMeter
//
//  Created by vikseriq on 10.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

@IBDesignable class FMUIRoundButton: UIButton {

    @IBInspectable var borderColor: UIColor = UIColor(rgba: "#000000") {
        didSet {
            setupView()
        }
    }
    
    @IBInspectable var borderRadius: CGFloat = 4 {
        didSet {
            setupView()
        }
    }

    @IBInspectable var borderWidth: CGFloat = 1 {
        didSet {
            setupView()
        }
    }
    
    @IBInspectable var roundBorder: Bool = false {
        didSet {
            setupView()
        }
    }
    
    private func setupView(){
        
        self.layer.borderWidth = self.borderWidth
        self.layer.borderColor = self.borderColor.CGColor
        if roundBorder {
            let m = min(self.layer.bounds.width, self.layer.bounds.height)
            self.layer.cornerRadius = m / 2
        } else {
            self.layer.cornerRadius = self.borderRadius
        }
        
        self.setNeedsDisplay()
    }
    
}
