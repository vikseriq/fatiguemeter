//
//  FMUINavigationBar.swift
//  FatigueMeter
//
//  Created by vikseriq on 05.05.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class FMUINavigationBar: UINavigationBar {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    class func applyAppearance(){
        UIApplication.sharedApplication().setStatusBarStyle(.LightContent, animated: false)
        
        if let backImageSrc = UIImage(named: "back"){
            let backImage = backImageSrc.resize(11, height: 18)
            UINavigationBar.appearance().backIndicatorImage = backImage
            UINavigationBar.appearance().backIndicatorTransitionMaskImage = backImage
            
        }
        
        // NavigationBar
        let noImage = UIImage()
        UINavigationBar.appearance().tintColor = FMUtility.colorWhite
        UINavigationBar.appearance().shadowImage = noImage
        UINavigationBar.appearance().setBackgroundImage(noImage, forBarMetrics: UIBarMetrics.Default)
        UINavigationBar.appearance().backgroundColor = FMUtility.gradientColorDark
        UINavigationBar.appearance().barTintColor = FMUtility.gradientColorDark
        UINavigationBar.appearance().translucent = false
        UINavigationBar.appearance().titleTextAttributes = [
            NSForegroundColorAttributeName: FMUtility.colorWhite,
            NSFontAttributeName: FMUtility.systemFont(14)
        ]
        
        // BarButtonItem, like back button
        UIBarButtonItem.appearance().setTitleTextAttributes([
            NSForegroundColorAttributeName: FMUtility.colorWhite,
            NSFontAttributeName: FMUtility.systemFont(14)
            ], forState: .Normal)
        
    }

}
