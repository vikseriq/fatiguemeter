//
//  TrialFingerPathView.swift
//  FatigueMeter
//
//  Created by vikseriq on 14.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

@IBDesignable class TrialFingerPathView: FMUIGradientView {

    var controller: FMFingerPathDelegate!
    
    let colorWhite = UIColor(red: 1.000, green: 1.000, blue: 1.000, alpha: 0.85)
    let colorCircle = UIColor(rgba: "#5CA486")
    let bezierWidth: CGFloat = 280
    let bezierHeight: CGFloat = 200
    let circleRadius: CGFloat = 13
    let circleInnerRadius: CGFloat = 7
    let circleFollowRadius: CGFloat = 32
    let circleStyloRadius: CGFloat = 32
    let styloCrossRadius: CGFloat = 24
    let signs: [[CGFloat]] = [
        [1, 0], [-1, 0], [0, 1], [0, -1], // general axes
        [0.71, 0.71], [-0.71, 0.71], [0.71, -0.71], [-0.71, -0.71] // diagonals sin(45) = cos(45) = 0.71
    ]
    var previewImageViews: [UIImageView]!
    
    var bezierPath: UIBezierPath!
    var bezierOffset: CGPoint!
    var circler: CGRect!
    var startPoint: CGPoint = CGPoint(x: 20, y: 65)
    var finalPoint: CGPoint = CGPoint(x: 250, y: 110)
    var fingerOffset: CGPoint = CGPoint()
    var styloOffset: CGPoint = CGPoint()
    var styloPoint: CGPoint = CGPoint()
    
    var followTS: CFTimeInterval!
    var overlapTS: CFTimeInterval!
    var timeTotal: Double = 0
    var timeOverlap: Double = 0
    
    var viewActive: Bool = false {
        didSet {
            if self.viewActive {
                self.setNeedsDisplay()
            }
        }
    }
    var circleFollow: Bool = false
    var lastFollowPoint: CGPoint!
    var overlap: PathOverlapPhase = .Inner
    
    override func drawRect(rect: CGRect) {
        if !viewActive {
            return
        }
        
        if bezierPath == nil {
            // init rect with curve
            let height = min(self.frame.height, bezierHeight) - circleStyloRadius
            bezierOffset = CGPoint(x: (rect.width - bezierWidth) / 2, y: (rect.height - 2.4 * height) / 2)
            let rectDrawing = CGRectMake(bezierOffset.x, bezierOffset.y, bezierWidth, bezierHeight)
            
            initBezierPath(rectDrawing)
            startPoint.x += bezierOffset.x
            startPoint.y += bezierOffset.y
            finalPoint.x += bezierOffset.x
            finalPoint.y += bezierOffset.y
            circler = CGRectMake(startPoint.x, startPoint.y, circleRadius, circleRadius)
            styloOffset = CGPoint(x: 0, y: height * 1.2)
            
            previewImageViews = []
            var imageView = UIImageView(image: UIImage(named: "path_preview_top"))
            imageView.alpha = 1
            imageView.frame = CGRectMake(rectDrawing.minX + 25, rectDrawing.minY + 18, imageView.frame.size.width, imageView.frame.size.height)
            previewImageViews.append(imageView)
            
            imageView = UIImageView(image: UIImage(named: "path_preview_bottom"))
            imageView.alpha = 1
            imageView.frame = CGRectMake(rectDrawing.minX + 70, rectDrawing.minY + styloOffset.y + 48,
                imageView.frame.size.width, imageView.frame.size.height)
            previewImageViews.append(imageView)
            
            for imageView in previewImageViews {
                self.addSubview(imageView)
            }
        }
        colorWhite.setFill()
        bezierPath.fill()
        
        
        // trial logic
        if circler.origin.x >= finalPoint.x && circler.origin.y <= finalPoint.y {
            overlap = .Finished
            circleFollow = false
        }
        
        // add to total time
        if circleFollow {
            if followTS == nil {
                followTS = CFAbsoluteTimeGetCurrent()
            }
        } else {
            if followTS != nil {
                timeTotal += CFAbsoluteTimeGetCurrent() - followTS
                followTS = nil
            }
        }
        
        // add to overlapping time
        if circleFollow && (overlap == .OnBoard || overlap == .Locked) {
            if overlapTS == nil {
                overlapTS = CFAbsoluteTimeGetCurrent()
            }
        } else {
            if overlapTS != nil {
                timeOverlap += CFAbsoluteTimeGetCurrent() - overlapTS
                overlapTS = nil
            }
        }
        
        if overlap == .Finished {
            if timeTotal == 0 {
                timeTotal = 1.0
                timeOverlap = 1.0
            }
            controller.moveFinished(timeTotal, overlapTime: timeOverlap)
            return
        }
        
        // drawing logic
        styloPoint = CGPoint(x: circler.minX + styloOffset.x, y: circler.minY + styloOffset.y)
        let context = UIGraphicsGetCurrentContext()
        
        // stylo lines
        var color = !circleFollow ? UIColor(rgba: "#FFFFFF10") : UIColor(rgba: "#70707050")
        let styloBorder: CGFloat = 1.0
        color.setFill()
        
        // stylo path
        // NB: by the way we can preinitialize path and just move offset with styloPoint. But in common likely no performance wins
        // because Baziers anyway recalculating
        let bottomRadius = circleStyloRadius + styloBorder * 4
        let topRadius = circleRadius + 2 * styloBorder
        let topJoinerOffset = circleRadius * 0.3
        let ovalBottom = UIBezierPath(ovalInRect: CGRectMake(styloPoint.x - bottomRadius, styloPoint.y - bottomRadius, 2 * bottomRadius, 2 * bottomRadius))
        let ovalTop = UIBezierPath(ovalInRect: CGRectMake(circler.minX - topRadius, circler.minY - topRadius, 2 * topRadius, 2 * topRadius))
        let ovalPath = UIBezierPath()
        ovalPath.moveToPoint(CGPointMake(styloPoint.x - bottomRadius, styloPoint.y))
        ovalPath.addLineToPoint(CGPointMake(styloPoint.x + bottomRadius, styloPoint.y))
        ovalPath.addLineToPoint(CGPointMake(circler.minX + topJoinerOffset, circler.minY - topJoinerOffset))
        ovalPath.addLineToPoint(CGPointMake(circler.minX - topJoinerOffset, circler.minY - topJoinerOffset))
        ovalPath.closePath()
        ovalPath.appendPath(ovalBottom.bezierPathByReversingPath())
        ovalPath.appendPath(ovalTop.bezierPathByReversingPath())
        
        // fill gradient
        CGContextSaveGState(context)
        let colors = circleFollow ? [UIColor(rgba: "#FFFFFFAA").CGColor, UIColor(rgba: "#BEBEBE88").CGColor]
            : [UIColor(rgba: "#FFFFFF33").CGColor, UIColor(rgba: "#FFFFFF33").CGColor]
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let colorLocation: [CGFloat] = [0.0, 1.0]
        let gradientStylo = CGGradientCreateWithColors(colorSpace, colors, colorLocation)
        ovalPath.addClip()
        CGContextDrawLinearGradient(context, gradientStylo, CGPointMake(styloPoint.x, styloPoint.y + bottomRadius), CGPointMake(circler.minX, circler.minY - circleRadius * 2), CGGradientDrawingOptions.DrawsAfterEndLocation
        
        )
        CGContextRestoreGState(context)

        // stylo cross
        UIColor.whiteColor().setFill()
        CGContextAddArc(context, styloPoint.x, styloPoint.y, circleStyloRadius, 0, CGFloat(M_PI * 2), 1)
        CGContextFillPath(context)
        
        drawCrossPath(CGRectMake(styloPoint.x - styloCrossRadius, styloPoint.y - styloCrossRadius, styloCrossRadius * 2, styloCrossRadius * 2))
        
        // point
        switch overlap {
        case .Inner:
            color = colorCircle
        case .OnBoard:
            color = UIColor.orangeColor()
        case .Locked:
            color = UIColor.redColor()
        default:
            break
        }
        if !circleFollow {
            color = colorCircle
        }
        color = color.colorWithAlphaComponent(0.85)
        color.setFill()
        
        CGContextAddArc(context, circler.minX, circler.minY, circler.width, 0, CGFloat(M_PI * 2), 1)
        CGContextFillPath(context)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        guard let touch = touches.first else { return }
        var point = touch.locationInView(self)
        point = CGPoint(x: point.x - styloOffset.x, y: point.y - styloOffset.y)
        
        circleFollow = false
        let prevOrigin = circler.origin
        
        let willFollow = abs(prevOrigin.x - point.x) < circleFollowRadius && abs(prevOrigin.y - point.y) < circleFollowRadius
        if willFollow {
            checkBounds()
            circler.origin = prevOrigin
            if overlap != .Outer {
                fingerOffset = CGPoint(x: circler.origin.x - point.x, y: circler.origin.y - point.y)
                circleFollow = true
            }
        }
        
        setNeedsDisplay()
        
        if previewImageViews != nil && previewImageViews[0].alpha == 1 {
            UIView.animateWithDuration(1.0, animations: { () -> Void in
                for imageView in self.previewImageViews {
                    imageView.alpha = 0
                }
            })
        }
        
        controller.playClickSound()
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        guard let touch = touches.first else { return }
        var point = touch.locationInView(self)
        point = CGPoint(x: point.x + fingerOffset.x - styloOffset.x, y: point.y + fingerOffset.y - styloOffset.y)
        
        var reFollow = false
        if !circleFollow {
            if lastFollowPoint != nil && abs(lastFollowPoint.x - point.x) < 15 && abs(lastFollowPoint.y - point.y) < 15 {
                if abs(circler.minX - point.x) <= circleStyloRadius && abs(circler.minY - point.y) <= circleStyloRadius {
                    reFollow = true
                }
            } else {
                return
            }
        }
        
        if abs(circler.minX - point.x) > circleFollowRadius * 0.75 || abs(circler.minY - point.y) > circleFollowRadius * 0.75 {
            // block fast movements
            return
        }
        
        let prevOrigin = circler.origin
        circler.origin = point
        checkBounds()
        if !circleFollow && reFollow && overlap != .Outer {
            circleFollow = true
        }
        if overlap == .Outer {
            circler.origin = prevOrigin
            if circleFollow {
                lastFollowPoint = circler.origin
            }
            circleFollow = false
        }
        
        setNeedsDisplay()
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        circleFollow = false
        setNeedsDisplay()
    }
    
    override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
        circleFollow = false
        setNeedsDisplay()
    }
    
    func checkBounds(){
        var _p: CGPoint = CGPointMake(0, 0)
        
        let bInside = bezierPath.containsPoint(circler.origin)
        
        var bBorder = true
        for i in 0..<signs.count {
            _p.x = circler.origin.x + signs[i][0] * circleRadius
            _p.y = circler.origin.y + signs[i][1] * circleRadius
            bBorder = bBorder && bezierPath.containsPoint(_p)
            if !bBorder {
                break
            }
        }
        
        var bLock = true
        for i in 0..<signs.count {
            _p.x = circler.origin.x + signs[i][0] * circleInnerRadius
            _p.y = circler.origin.y + signs[i][1] * circleInnerRadius
            bLock = bLock && bezierPath.containsPoint(_p)
            if !bLock {
                break
            }
        }
        
        if !bInside {
            overlap = .Outer
        } else if !bLock {
            overlap = .Locked
        } else if !bBorder {
            overlap = .OnBoard
        } else {
            overlap = .Inner
        }
        
    }
    
    func initBezierPath(frame: CGRect) {
        bezierPath = UIBezierPath()
        bezierPath.moveToPoint(CGPointMake(frame.minX + 14.5, frame.minY + 79.5))
        bezierPath.addCurveToPoint(CGPointMake(frame.minX + 9.5, frame.minY + 30.5), controlPoint1: CGPointMake(frame.minX - 6.5, frame.minY + 73.5), controlPoint2: CGPointMake(frame.minX - 1.75, frame.minY + 48.75))
        bezierPath.addCurveToPoint(CGPointMake(frame.minX + 62.5, frame.minY + 0.5), controlPoint1: CGPointMake(frame.minX + 15.54, frame.minY + 20.71), controlPoint2: CGPointMake(frame.minX + 33.5, frame.minY - 3.5))
        bezierPath.addCurveToPoint(CGPointMake(frame.minX + 104.5, frame.minY + 32.5), controlPoint1: CGPointMake(frame.minX + 87.54, frame.minY + 3.95), controlPoint2: CGPointMake(frame.minX + 99.87, frame.minY + 21.38))
        bezierPath.addCurveToPoint(CGPointMake(frame.minX + 85.5, frame.minY + 113.5), controlPoint1: CGPointMake(frame.minX + 114.5, frame.minY + 56.5), controlPoint2: CGPointMake(frame.minX + 83.5, frame.minY + 95.5))
        bezierPath.addCurveToPoint(CGPointMake(frame.minX + 120.5, frame.minY + 108.5), controlPoint1: CGPointMake(frame.minX + 87.5, frame.minY + 131.5), controlPoint2: CGPointMake(frame.minX + 121.5, frame.minY + 132.5))
        bezierPath.addCurveToPoint(CGPointMake(frame.minX + 125.5, frame.minY + 32.5), controlPoint1: CGPointMake(frame.minX + 119.5, frame.minY + 84.5), controlPoint2: CGPointMake(frame.minX + 111.75, frame.minY + 59))
        bezierPath.addCurveToPoint(CGPointMake(frame.minX + 192.5, frame.minY + 14.5), controlPoint1: CGPointMake(frame.minX + 139.25, frame.minY + 6), controlPoint2: CGPointMake(frame.minX + 170.5, frame.minY + 2.5))
        bezierPath.addCurveToPoint(CGPointMake(frame.minX + 208.5, frame.minY + 85.5), controlPoint1: CGPointMake(frame.minX + 214.5, frame.minY + 26.5), controlPoint2: CGPointMake(frame.minX + 217.5, frame.minY + 59.5))
        bezierPath.addCurveToPoint(CGPointMake(frame.minX + 230.5, frame.minY + 106.5), controlPoint1: CGPointMake(frame.minX + 199.5, frame.minY + 111.5), controlPoint2: CGPointMake(frame.minX + 215.5, frame.minY + 122.5))
        bezierPath.addCurveToPoint(CGPointMake(frame.minX + 267.5, frame.minY + 96.5), controlPoint1: CGPointMake(frame.minX + 245.5, frame.minY + 90.5), controlPoint2: CGPointMake(frame.minX + 253.5, frame.minY + 82.5))
        bezierPath.addCurveToPoint(CGPointMake(frame.minX + 253.5, frame.minY + 137.5), controlPoint1: CGPointMake(frame.minX + 281.5, frame.minY + 110.5), controlPoint2: CGPointMake(frame.minX + 267.5, frame.minY + 125.5))
        bezierPath.addCurveToPoint(CGPointMake(frame.minX + 181.5, frame.minY + 137.5), controlPoint1: CGPointMake(frame.minX + 239.5, frame.minY + 149.5), controlPoint2: CGPointMake(frame.minX + 208.5, frame.minY + 166.5))
        bezierPath.addCurveToPoint(CGPointMake(frame.minX + 175.5, frame.minY + 60.5), controlPoint1: CGPointMake(frame.minX + 154.5, frame.minY + 108.5), controlPoint2: CGPointMake(frame.minX + 172.5, frame.minY + 75.5))
        bezierPath.addCurveToPoint(CGPointMake(frame.minX + 156.5, frame.minY + 68.5), controlPoint1: CGPointMake(frame.minX + 178.5, frame.minY + 45.5), controlPoint2: CGPointMake(frame.minX + 152.3, frame.minY + 34.92))
        bezierPath.addCurveToPoint(CGPointMake(frame.minX + 152.5, frame.minY + 132.5), controlPoint1: CGPointMake(frame.minX + 158.5, frame.minY + 84.5), controlPoint2: CGPointMake(frame.minX + 160.69, frame.minY + 112.47))
        bezierPath.addCurveToPoint(CGPointMake(frame.minX + 96.5, frame.minY + 167.5), controlPoint1: CGPointMake(frame.minX + 147.79, frame.minY + 144.02), controlPoint2: CGPointMake(frame.minX + 128.5, frame.minY + 169.5))
        bezierPath.addCurveToPoint(CGPointMake(frame.minX + 49.5, frame.minY + 125.5), controlPoint1: CGPointMake(frame.minX + 67.39, frame.minY + 165.68), controlPoint2: CGPointMake(frame.minX + 55.22, frame.minY + 140.74))
        bezierPath.addCurveToPoint(CGPointMake(frame.minX + 68.5, frame.minY + 54.5), controlPoint1: CGPointMake(frame.minX + 37.5, frame.minY + 93.5), controlPoint2: CGPointMake(frame.minX + 65.5, frame.minY + 68.5))
        bezierPath.addCurveToPoint(CGPointMake(frame.minX + 42.5, frame.minY + 48.5), controlPoint1: CGPointMake(frame.minX + 71.5, frame.minY + 40.5), controlPoint2: CGPointMake(frame.minX + 48.5, frame.minY + 30.5))
        bezierPath.addCurveToPoint(CGPointMake(frame.minX + 14.5, frame.minY + 79.5), controlPoint1: CGPointMake(frame.minX + 36.5, frame.minY + 66.5), controlPoint2: CGPointMake(frame.minX + 35.5, frame.minY + 85.5))
        bezierPath.closePath()

        bezierPath.lineJoinStyle = CGLineJoin.Round;
    }
    
    func drawCrossPath(frame: CGRect) {
        let color = UIColor(red: 0.275, green: 0.741, blue: 0.514, alpha: 1.000);
        color.setFill()
        let crossPath = UIBezierPath()
        crossPath.moveToPoint(CGPointMake(frame.minX + 0.99837 * frame.width, frame.minY + 0.50160 * frame.height))
        crossPath.addLineToPoint(CGPointMake(frame.minX + 0.80696 * frame.width, frame.minY + 0.60975 * frame.height))
        crossPath.addLineToPoint(CGPointMake(frame.minX + 0.80696 * frame.width, frame.minY + 0.53431 * frame.height))
        crossPath.addLineToPoint(CGPointMake(frame.minX + 0.53435 * frame.width, frame.minY + 0.53431 * frame.height))
        crossPath.addLineToPoint(CGPointMake(frame.minX + 0.53435 * frame.width, frame.minY + 0.80291 * frame.height))
        crossPath.addLineToPoint(CGPointMake(frame.minX + 0.61092 * frame.width, frame.minY + 0.80291 * frame.height))
        crossPath.addLineToPoint(CGPointMake(frame.minX + 0.50116 * frame.width, frame.minY + 0.99150 * frame.height))
        crossPath.addLineToPoint(CGPointMake(frame.minX + 0.39140 * frame.width, frame.minY + 0.80291 * frame.height))
        crossPath.addLineToPoint(CGPointMake(frame.minX + 0.46796 * frame.width, frame.minY + 0.80291 * frame.height))
        crossPath.addLineToPoint(CGPointMake(frame.minX + 0.46796 * frame.width, frame.minY + 0.53431 * frame.height))
        crossPath.addLineToPoint(CGPointMake(frame.minX + 0.19537 * frame.width, frame.minY + 0.53432 * frame.height))
        crossPath.addLineToPoint(CGPointMake(frame.minX + 0.19537 * frame.width, frame.minY + 0.60976 * frame.height))
        crossPath.addLineToPoint(CGPointMake(frame.minX + 0.00396 * frame.width, frame.minY + 0.50160 * frame.height))
        crossPath.addLineToPoint(CGPointMake(frame.minX + 0.19537 * frame.width, frame.minY + 0.39345 * frame.height))
        crossPath.addLineToPoint(CGPointMake(frame.minX + 0.19537 * frame.width, frame.minY + 0.46890 * frame.height))
        crossPath.addLineToPoint(CGPointMake(frame.minX + 0.46796 * frame.width, frame.minY + 0.46890 * frame.height))
        crossPath.addLineToPoint(CGPointMake(frame.minX + 0.46796 * frame.width, frame.minY + 0.20029 * frame.height))
        crossPath.addLineToPoint(CGPointMake(frame.minX + 0.39140 * frame.width, frame.minY + 0.20029 * frame.height))
        crossPath.addLineToPoint(CGPointMake(frame.minX + 0.50116 * frame.width, frame.minY + 0.01172 * frame.height))
        crossPath.addLineToPoint(CGPointMake(frame.minX + 0.61092 * frame.width, frame.minY + 0.20029 * frame.height))
        crossPath.addLineToPoint(CGPointMake(frame.minX + 0.53435 * frame.width, frame.minY + 0.20029 * frame.height))
        crossPath.addLineToPoint(CGPointMake(frame.minX + 0.53435 * frame.width, frame.minY + 0.46890 * frame.height))
        crossPath.addLineToPoint(CGPointMake(frame.minX + 0.80698 * frame.width, frame.minY + 0.46888 * frame.height))
        crossPath.addLineToPoint(CGPointMake(frame.minX + 0.80698 * frame.width, frame.minY + 0.39344 * frame.height))
        crossPath.addLineToPoint(CGPointMake(frame.minX + 0.99837 * frame.width, frame.minY + 0.50160 * frame.height))
        crossPath.closePath()
        crossPath.miterLimit = 4;
        
        crossPath.usesEvenOddFillRule = true;
        
        crossPath.fill()
    }


}

enum PathOverlapPhase {
    case Inner, OnBoard, Locked, Outer, Finished
}

protocol FMFingerPathDelegate {
    func moveFinished(totalTime: Double, overlapTime: Double)
    func playClickSound()
}

