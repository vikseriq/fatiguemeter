//
//  TrialSoundViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 10.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit
import AVFoundation
import Foundation

class TrialSoundViewController: TrialBaseViewController {

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        trialType = TrialType.Sound
        needSoundableClick = true
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBOutlet weak var imageSound: UIImageView!
    @IBOutlet weak var labelTotal: UILabel!
    @IBOutlet weak var labelWait: UILabel!
    
    var delayTimer: NSTimer!
    var dismissTimer: NSTimer!
    var trialActive = false
    let maxTimes = 3
    var soundPlayer: AVAudioPlayer!
    var soundTimes: [Double] = []
    var startTime: CFTimeInterval = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func reset(){
        trialActive = false
        imageSound?.alpha = 0
        labelWait?.hidden = true
        labelTotal?.text = "0 of \(maxTimes)"
        if delayTimer != nil {
            delayTimer.invalidate()
        }
        if dismissTimer != nil {
            dismissTimer.invalidate()
        }
        soundPlayer = nil
        soundTimes = []
        
        trialRunning = false
    }

    override func runTrial() {
        if !trialRunning {
            return
        }
        super.runTrial()
        reset()
        trialRunning = true
        if soundMode == .Enabled {
            let soundPath = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("snd", ofType: "wav")!)
            do {
                try soundPlayer = AVAudioPlayer(contentsOfURL: soundPath)
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        scheduleNext(0.0)
    }
    
    func scheduleNext(extraDelay: Double){
        let delay = FMUtility.random(0.8, to: 1.6) + extraDelay
        
        if soundTimes.count >= maxTimes {
            finishTrial()
            return
        }
        labelWait.hidden = false
        
        delayTimer?.invalidate()
        delayTimer = NSTimer.scheduledTimerWithTimeInterval(delay, target: self, selector: Selector("playSound"), userInfo: nil, repeats: false)
    }
    
    func playSound(){
        if !trialRunning {
            return
        }
        trialActive = true
        labelWait.hidden = true
        
        if soundMode == .Enabled {
            soundPlayer.prepareToPlay()
            soundPlayer.play()
            vibro()
        } else if soundMode == .Vibro {
            vibro()
        }
        
        startTime = CFAbsoluteTimeGetCurrent()
        UIView.animateWithDuration(1, animations: {
            self.imageSound.alpha = 1
        })
        dismissTimer = NSTimer.scheduledTimerWithTimeInterval(2.0, target: self, selector: Selector("soundTimeout"), userInfo: nil, repeats: false)
    }
    
    func vibro(){
        if trialActive {
             AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        }
    }
    
    func stopSound(byTimeout: Bool){
        trialActive = false
        soundTimes.append(CFAbsoluteTimeGetCurrent() - startTime)
        dismissTimer?.invalidate()
        imageSound.alpha = 0
        
        if soundMode == .Enabled {
            soundPlayer.stop()
        }
        
        labelTotal.text = "\(soundTimes.count) of \(maxTimes)"
        
        scheduleNext(0.0)
    }
    
    func soundTimeout(){
        if !trialRunning {
            return
        }
        if trialActive {
            stopSound(false)
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if trialActive {
            stopSound(false)
        } else if trialRunning {
            scheduleNext(curbTouches())
        }
        clickSound()
    }
    
    override func finishTrial() {
        trialResults = [soundTimes.average() * 1000]
        super.finishTrial()
    }
    
    override func stopTrial() {
        super.stopTrial()
        reset()
    }
    

}
