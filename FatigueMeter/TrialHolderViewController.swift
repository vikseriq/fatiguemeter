//
//  TrialHolderViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 09.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class TrialHolderViewController: UIViewController, FMTrialFinishDelegate, FMPreviewDelegate, FMStateBackgroundDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        frameForTrial = CGRectMake(0, trialBar.bounds.height, self.view.bounds.width, self.view.bounds.height - trialBar.bounds.height)
        trialBar.hidden = true
        totalTime = 0
        Flurry.logEvent("trialSession", timed: true)
        
        if TrialEvent.fetchTotalEvents() == 0 {
            barLeft.enabled = false
        }
        
        // fill trials set
        let maxTrials = 8
        trialSet = []
        for (var i = 0; i < maxTrials; trialSet.append(++i)){}
        // shuffle trials for non-newbee
        if (TrialEvent.fetchTotalEvents() > 0){
            trialSet.sortInPlace {
                (_,_) in arc4random() < arc4random()
            }
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        previewHidden = !FMSettings.previewOn
        
        FMStateful.machine.backgroundDelegate = self
        if presentTrial == nil {
            startTrials()
        }
    }
    
    override func viewDidDisappear(animated: Bool) {
        FMStateful.machine.backgroundDelegate = nil
        if presentTrial != nil {
            presentTrial.viewDidDisappear(animated)
        }
    }
    
    func stateBackgroundChanged(inBackground: Bool) {
        if inBackground {
            breakTrial()
        } else {
            if CFAbsoluteTimeGetCurrent() - startTime < 60 * 5 {
                restartTrial()
            } else {
                Flurry.logEvent("trialStop", withParameters: ["trialID": trialIndex, "trialType": currentType.key(), "actor": "timeout"])
                dismissTrial()
            }
        }
    }
    
    var presentTrial: TrialBaseViewController!
    var previewPopup: TrialPreviewViewController!
    var frameForTrial: CGRect!
    
    @IBOutlet weak var trialBar: UIView!
    @IBOutlet weak var barTitle: UILabel!
    @IBOutlet weak var barSubtitle: UILabel!
    @IBOutlet weak var barLeft: UIButton!
    @IBOutlet weak var barRight: UIButton!

    var trialSet: [Int] = []
    var trialIndex = 0
    var currentType: TrialType = TrialType.Stub
    var previewHidden = false
    var previewOnGuide = false
    var totalTime: CFTimeInterval = 0
    var startTime: CFTimeInterval = 0
    var replacementLock: Bool = false
    
    @IBAction func leftButton(sender: AnyObject) {
        Flurry.logEvent("trialStop", withParameters: ["trialID": trialIndex, "trialType": currentType.key(), "actor": "user"])
        dismissTrial()
    }
    
    @IBAction func rightButton(sender: AnyObject) {
        if replacementLock {
            return
        }
        Flurry.logEvent("trialRestart", withParameters: ["trialID": trialIndex, "trialType": currentType.key()])
        restartTrial()
    }
    
    func restartTrial(){
        previewOnGuide = true
        replaceTrial(trialSet[trialIndex], animated: false)
    }
    
    func trialFinished() {
        if AppDelegate.debug {
            print("Result: \(presentTrial.trialType.key()): ")
            for val in presentTrial.trialResults {
                print("\(val) ")
            }
            print("\n")
        }
        TrialResultModel.sharedInstance.pushResult(currentType, results: presentTrial.trialResults)
        nextTrial()
    }
    
    func nextTrial(){
        if ++trialIndex >= trialSet.count {
            FMStateful.setState(.TrialCompleted)
            TrialResultModel.sharedInstance.commitTrial()
            let deltaTime = CFAbsoluteTimeGetCurrent() - totalTime
            let total = FMSettings.incTrials()
            Flurry.logEvent("trials", withParameters: ["total": total])
            Flurry.endTimedEvent("trialSession", withParameters: ["fatigue": TrialResultModel.sharedInstance.lastEvent.fatigue, "time": deltaTime])
            
            let vc = self.storyboard!.instantiateViewControllerWithIdentifier("Processing")
            vc.view.frame = CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y, self.view.bounds.width, self.view.bounds.height)
            view.addSubview(vc.view)
            addChildViewController(vc)
            vc.didMoveToParentViewController(self)
            return
        } else {
            replaceTrial(trialSet[trialIndex], animated: true)
        }
    }
    
    func breakTrial(){
        if presentTrial != nil {
            presentTrial.stopTrial()
        }
    }
    
    func dismissTrial(){
        breakTrial()
        FMStateful.setState(.TrialCompleted)
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func loadTrial(trialId: Int) -> TrialBaseViewController {
        var newTrial: TrialBaseViewController!
        switch (trialId){
        case 2:
            newTrial = TrialSoundViewController(nibName: "TrialSoundViewController", bundle: nil)
            currentType = TrialType.Sound
        case 3:
            newTrial = TrialColorsViewController(nibName: "TrialColorsViewController", bundle: nil)
            currentType = TrialType.Colors
        case 4:
            newTrial = TrialMuscleViewController(nibName: "TrialMuscleViewController", bundle: nil)
            currentType = TrialType.Muscle
        case 5:
            newTrial = TrialStrobesViewController(nibName: "TrialStrobesViewController", bundle: nil)
            currentType = TrialType.Frequency
        case 6:
            newTrial = TrialFingerPathViewController(nibName: "TrialFingerPathViewController", bundle: nil)
            currentType = TrialType.Path
        case 7:
            newTrial = TrialRecentMemoryViewController(nibName: "TrialRecentMemoryViewController", bundle: nil)
            currentType = TrialType.Memory
        case 8:
            newTrial = TrialSequenceInputViewController(nibName: "TrialSequenceInputViewController", bundle: nil)
            currentType = TrialType.Sequence
        default:
            newTrial = TrialReactionViewController(nibName: "TrialReactionViewController", bundle: nil)
            currentType = TrialType.Light
        }
        newTrial.finishHandler = self
        return newTrial
    }
    
    func setupTrial(trialType: TrialType){
        let tc = trialType.fetchConfig()
        barTitle.text = tc.title
        barSubtitle.text = "Test \(trialIndex + 1) of \(trialSet.count)"
        startTime = CFAbsoluteTimeGetCurrent()
    }
    
    func startTrials(){
        trialBar.hidden = false
        presentTrial = loadTrial(trialSet[trialIndex])
        setupTrial(presentTrial.trialType)
        
        self.addChildViewController(presentTrial)
        presentTrial.view.frame = frameForTrial
        presentTrial.view.hidden = true
        self.view.addSubview(presentTrial.view)
        showPreview()
    }
    
    func replaceTrial(newTrialId: Int, animated: Bool){
        replacementLock = true
        let newTrial = loadTrial(newTrialId)
        
        if presentTrial.trialRunning {
            presentTrial.stopTrial()
        }
        presentTrial.willMoveToParentViewController(nil)
        addChildViewController(newTrial)
        
        if previewPopup != nil {
            previewPopup.view.removeFromSuperview()
            previewPopup.removeFromParentViewController()
            previewPopup = nil
        }
        
        newTrial.view.frame = presentTrial.view.frame
        newTrial.view.hidden = true
        let endFrame = CGRectMake(0, 0, self.view.bounds.width, self.view.bounds.height)
        
        let t: Double = animated ? 0.5 : 0
        let opts = animated ? UIViewAnimationOptions.TransitionFlipFromLeft : UIViewAnimationOptions.TransitionNone
        
        transitionFromViewController(presentTrial, toViewController: newTrial, duration: t,
            options: opts,
            animations: {
                self.presentTrial.view.frame = endFrame
                self.setupTrial(newTrial.trialType)
            }, completion: {
                (Bool) in
                self.presentTrial.removeFromParentViewController()
                self.presentTrial = newTrial
                self.presentTrial.view.hidden = true
                self.showPreview()
            }
        )
    }
    
    func showPreview(){
        replacementLock = false
        if !previewHidden || previewOnGuide {
            let tc = presentTrial.trialType.fetchConfig()
            // disable countdown if preview shown
            presentTrial.needCountdown = false
            // force previews on restarts
            previewOnGuide = false
            
            previewPopup = TrialPreviewViewController(nibName: "TrialPreviewViewController", bundle: nil)
            previewPopup.callerVC = self
            previewPopup.trialConfig = tc
            
            previewPopup.view.frame = frameForTrial
            previewPopup.view.alpha = 0
            previewPopup.viewWillAppear(true)
            view.addSubview(previewPopup.view)
            addChildViewController(previewPopup)
            previewPopup.didMoveToParentViewController(self)
            UIView.animateWithDuration(0.5, animations: {
                self.previewPopup.view.alpha = 1
            })
        } else {
            previewClosed()
        }
    }
    
    func previewClosing() {
        UIView.animateWithDuration(0.5, animations: {
            self.previewPopup.view.alpha = 0
        }, completion: { (Bool) -> Void in
            self.previewPopup.viewWillDisappear(true)
            self.previewPopup.view.removeFromSuperview()
            self.previewPopup.removeFromParentViewController()
            self.previewPopup.didMoveToParentViewController(nil)
            self.previewPopup = nil
            self.presentTrial.view.hidden = false
            self.presentTrial.view.alpha = 0
            UIView.animateWithDuration(0.5, animations: {
                self.presentTrial.view.alpha = 1
            })
            self.previewClosed()
        })
    }
    
    func previewClosed() {
        presentTrial.view.hidden = false
        presentTrial.didMoveToParentViewController(self)
        presentTrial.beginTrial()
    }

}

protocol FMTrialFinishDelegate {
    
    func trialFinished()
    
}