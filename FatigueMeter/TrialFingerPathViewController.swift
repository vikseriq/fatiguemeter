//
//  TrialFingerPathViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 13.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class TrialFingerPathViewController: TrialBaseViewController, FMFingerPathDelegate {

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        trialType = TrialType.Path
        needCountdown = false
        needSoundableClick = true
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    @IBOutlet weak var viewPath: TrialFingerPathView!
    @IBOutlet weak var labelResults: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewPath.controller = self
    }
    
    override func runTrial() {
        super.runTrial()
        viewPath.viewActive = true
        labelResults.hidden = true
    }
    
    func moveFinished(totalTime: Double, overlapTime: Double) {
        if trialRunning {
            viewPath.viewActive = false
            clickSound()
            
            let result: Double = 1.0 - overlapTime / totalTime
            trialResults = [result * 100]
            finishTrial()
        }
    }
    
    func playClickSound() {
        clickSound()
    }

}
