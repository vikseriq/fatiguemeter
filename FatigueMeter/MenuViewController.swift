//
//  MenuViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 24.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    let menus: [MenuVariant] = [
        MenuVariant(toState: .Home,          title: "Home",            icon: "menu_home",            eventKey: nil),
        MenuVariant(toState: .Calendar,      title: "My progress",     icon: "menu_progress",        eventKey: nil),
        MenuVariant(toState: .SubscriptionLanding, title: "Advanced",    icon: "menu_achievements",    eventKey: "showRecommend"),
        MenuVariant(toState: .RecommendLast, title: "Recommendations", icon: "menu_recommendations", eventKey: "showRecommend"),
        MenuVariant(toState: .Help,          title: "About tests",     icon: "menu_about_tests",     eventKey: "showHelp"),
        MenuVariant(toState: .Faq,           title: "FAQ",             icon: "menu_faq",             eventKey: "showFAQ"),
        MenuVariant(toState: .Settings,      title: "Settings",        icon: "menu_settings",        eventKey: "showSettings")
    ]
    
    @IBOutlet weak var menuTable: UITableView!
    @IBOutlet weak var menuTableHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuTable.tableFooterView = UIView(frame: CGRectZero)
        menuTable.rowHeight = 48
        menuTableHeight.constant = (menuTable.rowHeight + 1.0) * CGFloat(menus.count) - 4.0
    }
    
    func updateMenuView() {
        menuTable?.reloadData()
    }
    
    @IBAction func fireRun(sender: UIButton) {
        performStateChange(.TrialGo)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menus.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("menuCell") as! MenuCell
        cell.setupData(menus[indexPath.row])
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        if let key = menus[indexPath.row].eventKey {
            Flurry.logEvent(key, withParameters: ["source": "menu"])
        }
        performStateChange(menus[indexPath.row].toState)
    }
    
    func loadController(name: String) -> UIViewController {
        let names = name.componentsSeparatedByString(".")
        if names.count == 2 {
            let story = UIStoryboard(name: names.first!, bundle: nil)
            return story.instantiateViewControllerWithIdentifier(names.last!)
        } else {
            return self.storyboard!.instantiateViewControllerWithIdentifier(name)
        }
    }
    
    func pushController(name: String){
        let nc = revealViewController().frontViewController as! NaviViewController
        nc.pushViewController(loadController(name), animated: true)
    }
    
    func popController(){
        let nc = revealViewController().frontViewController as! NaviViewController
        nc.popViewControllerAnimated(true)
    }
    
    func popToRootController(){
        let nc = revealViewController().frontViewController as! NaviViewController
        nc.popToRootViewControllerAnimated(true)
    }
    
    func presentController(name: String){
        let nc = revealViewController().frontViewController as! NaviViewController
        nc.presentViewController(loadController(name), animated: true, completion: nil)
    }
    
    func setController(name: String, addMenu: Bool){
        let nc = revealViewController().frontViewController as! NaviViewController
        let c = loadController(name)
        nc.setViewControllers([c], animated: false)
        if addMenu {
            NaviViewController.showMenu(c)
        }
    }
    
    func setController(name: String){
        setController(name, addMenu: true)
    }
    
    func placeHome(){
        setController("Home")
    }
    
    func flipToHome(){
        FMStateful.setState(.Load2Home)
        let nc = revealViewController().frontViewController as! NaviViewController
        let c = loadController("Home")
        nc.setViewControllers([c], animated: true)
    }
    
    func performStateChange(toState: FMStates){
        let fromState = FMStateful.now()
        FMStateful.setState(toState)
        var preserveMenu = false
        if let rvc = revealViewController(){
            if fromState != toState {
                switch toState {
                case .Tour:
                    presentController("IntroPages")
                    preserveMenu = true
                case .TrialGo:
                    presentController("TrialHolder")
                    if fromState == .ProfileSetup {
                        NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("placeHome"), userInfo: nil, repeats: false)
                    }
                    preserveMenu = true
                case .Settings:
                    switch fromState {
                    case .Profile:
                        popController()
                    case .Home2Detail:
                        pushController("SettingsTable")
                    default:
                        setController("SettingsTable")
                    }
                case .AccountLogin:
                    if !AccountModel.isActive() {
                        pushController("Account.Login")
                    } else {
                        pushController("Account.Profile")
                    }
                case .Faq:
                    setController("Faq")
                case .FaqItem:
                    pushController("FaqItem")
                case .Help:
                    setController("Help")
                case .HelpTrial:
                    pushController("HelpTrial")
                case .MoreRun:
                    setController("MoreRun", addMenu: false)
                case .SubscriptionLanding:
                    setController("Account.SubscriptionLanding")
                case .Load2Home:
                    setController("Home", addMenu: false)
                case .Home:
                    setController("Home")
                case .RecommendLast:
                    if fromState == .Home2Detail {
                        pushController("RecommendSet")
                    } else {
                        setController("RecommendSet")
                    }
                case .RecommendSet:
                    pushController("RecommendSet")
                case .RecommendItem:
                    pushController("RecommendItem")
                case .Calendar:
                    if fromState == .Home2Detail {
                        pushController("Calendar")
                    } else {
                        setController("Calendar")
                    }
                case .ProgressDay:
                    pushController("DayResult")
                case .ProgressLast:
                    setController("TableResult")
                case .ProgressDetail:
                    pushController("TableResult")
                case .Profile:
                    pushController("ProfileWizard")
                case .ProfileSetup:
                    setController("ProfileWizard", addMenu: false)
                default:
                    break
                }
            }
            let nc = rvc.frontViewController as! NaviViewController
            var frame = nc.navigationBar.frame;
            frame.origin.y = 20
            nc.navigationBar.frame = frame
            if nc.menuToggled && !preserveMenu {
                nc.toggleMenu()
            }
        }
    }
    
    class func unwrap(sender: UIViewController) -> MenuViewController? {
        if let menu = sender.revealViewController()?.rearViewController as? MenuViewController {
            return menu
        }
        return nil
    }
    
}

struct MenuVariant {
    var toState: FMStates
    var title: String
    var icon: String
    var eventKey: String?
}

class MenuCell: UITableViewCell {
    
    var badgeCount: Int = 0
    
    var imageAccessory: UIImageView!
    var badgeAccessory: UIView!
    var badgeLabel: UILabel!
    
    var data: MenuVariant!
    
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    
    func setupData(data: MenuVariant){
        self.data = data
        labelName.text = data.title
        icon.image = UIImage(named: data.icon)
        
        //setAccessory()
        setNeedsDisplay()
    }
    
    func setAccessory(){
        if badgeCount == 0 {
            imageAccessory = UIImageView(image: UIImage(named: "accessory_white"))
            imageAccessory.contentMode = UIViewContentMode.ScaleAspectFit
            imageAccessory.frame = CGRectMake(0, 0, 10, 15)
            accessoryView = imageAccessory
        } else {
            let badgeHeight: CGFloat = 22
            badgeAccessory = UIView(frame: CGRectMake(0, 0, 22, badgeHeight))
            badgeAccessory.layer.cornerRadius = badgeHeight / 2
            badgeAccessory.layer.backgroundColor = UIColor(rgba: "#FFBD00").CGColor
            badgeLabel = UILabel(frame: badgeAccessory.frame)
            badgeLabel.text = String(badgeCount)
            badgeLabel.font = UIFont(name: "System-Thin", size: 25)
            badgeLabel.textColor = UIColor.whiteColor()
            badgeLabel.textAlignment = NSTextAlignment.Center
            badgeAccessory.addSubview(badgeLabel)
            accessoryView = badgeAccessory
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
}

