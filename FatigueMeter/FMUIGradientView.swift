//
//  FMUIGradientView.swift
//  FatigueMeter
//
//  Created by vikseriq on 06.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

@IBDesignable class FMUIGradientView: UIView {

    @IBInspectable var startColor: UIColor = FMUtility.gradientColorDark {
        didSet {
            setupView()
        }
    }

    @IBInspectable var endColor: UIColor = FMUtility.gradientColorLight {
        didSet {
            setupView()
        }
    }
    
    @IBInspectable var translucent: Bool = false {
        didSet {
            setupView()
        }
    }
    
    private func setupView(){
        let colors: Array = [startColor.CGColor, endColor.CGColor]
        if translucent {
            gradientLayer.colors = [UIColor.clearColor().CGColor]
            backgroundColor = UIColor.clearColor()
            return
        }
        gradientLayer.colors = colors
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 1)
        
        self.setNeedsDisplay()
    }
    
    var gradientLayer: CAGradientLayer {
        return layer as! CAGradientLayer
    }
    
    override class func layerClass()->AnyClass{
        return CAGradientLayer.self
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
}
