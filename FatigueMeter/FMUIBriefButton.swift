//
//  FMUIBriefButton.swift
//  FatigueMeter
//
//  Created by vikseriq on 25.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

@IBDesignable
class FMUIBriefButton: UIButton {

    @IBInspectable var briefImage: UIImage = UIImage(named: "menu")! {
        didSet {
            setupView()
        }
    }
    
    @IBInspectable var briefSubtitle: String = "Subtitle"{
        didSet {
            //setupView()
        }
    }

    @IBInspectable var briefDate: String = "Apr 14, 2015"{
        didSet {
            setupView()
        }
    }

    var labelDate: UILabel!
    var labelSubtitle: UILabel!
    var labelTitle: UILabel!
    var imgView: UIImageView!
    
    func initView(){
        let xGap: CGFloat = 48
        
        imgView = UIImageView(frame: CGRectMake(14, 14, 27, 30))
        imgView.contentMode = UIViewContentMode.ScaleAspectFit
        imgView.clipsToBounds = true
        
        let font = FMUtility.systemFont(11)
        
        labelSubtitle = UILabel(frame: CGRectMake(xGap, 0, self.bounds.width, 20))
        labelSubtitle.textColor = UIColor(rgba: "#73818C")
        labelSubtitle.font = font
        
        labelTitle = UILabel(frame: CGRectMake(xGap, (self.bounds.height - 20 * 1.5) / 2, self.bounds.width - xGap - 5, 20))
        labelTitle.font = titleLabel!.font
        labelTitle.textColor = titleLabel!.textColor
        labelTitle.lineBreakMode = NSLineBreakMode.ByTruncatingMiddle
        titleLabel!.hidden = true
        titleLabel!.alpha = 0
        
        setTitle(titleLabel?.text ?? "", color: titleLabel?.textColor ?? UIColor.whiteColor())
        
        labelDate = UILabel(frame: CGRectMake(xGap, self.bounds.height - 20 * 1.5, self.bounds.width, 20))
        labelDate.textColor = UIColor.whiteColor()
        labelDate.font = font
        
        self.addSubview(imgView)
        //self.addSubview(labelSubtitle)
        self.addSubview(labelTitle)
        self.addSubview(labelDate)
    }
    
    func setTitle(title: String, color: UIColor){
        labelTitle.text = title
        labelTitle.textColor = color
    }
    
    func setupView(){
        imgView.image = briefImage.tintWithColor(UIColor.whiteColor(), blendMode: .Overlay)
        //labelSubtitle.text = briefSubtitle
        labelDate.text = briefDate
        
        setNeedsDisplay()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initView()
        setupView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
        setupView()
    }
    
}
