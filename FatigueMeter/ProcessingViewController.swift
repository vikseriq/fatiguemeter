//
//  ProcessingViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 25.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class ProcessingViewController: UIViewController {

    @IBOutlet weak var logoView: FMUILogoView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        logoView.userInteractionEnabled = false
        logoView.setPosition(0)
    }
    
    override func didMoveToParentViewController(parent: UIViewController?) {
        let total = TrialEvent.fetchTotalEvents()
        
        if total == 2 {
            // calc up fatigue by first two tests
            let resultModel = TrialResultModel()
            var events = [TrialEvent]()
            let lastData = TrialEvent.fetchLatest(count: 2)
            for eventManaged in lastData {
                events.append(TrialEvent(managed: eventManaged))
            }
            
            for i in 0...(events.count-1) {
                resultModel.calculateFatigue(&events, currentIndex: i, startIndex: 0)
            }
            for event in events {
                event.writeChanges()
            }
        }
        
        let toState: FMStates = .RecommendLast
        let rvc = self.presentingViewController as! SWRevealViewController
        MenuViewController.unwrap(rvc.frontViewController)?.performStateChange(toState)
    }
    
    override func viewDidAppear(animated: Bool) {
        logoView.runAnimation(2.0)
        NSTimer.scheduledTimerWithTimeInterval(2.0, target: self, selector: Selector("close"), userInfo: nil, repeats: false)
    }
    
    func close(){
        dismissViewControllerAnimated(true, completion: nil)
    }
    
}
