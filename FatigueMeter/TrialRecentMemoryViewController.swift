//
//  TrialRecentMemoryViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 15.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit
import AVFoundation

class TrialRecentMemoryViewController: TrialBaseViewController {
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        trialType = TrialType.Memory
        needCountdown = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBOutlet weak var viewRemember: UIView!
    @IBOutlet weak var labelRemember: UILabel!
    @IBOutlet weak var buttonRemembered: FMUIRoundButton!
    
    @IBOutlet weak var viewEquations: UIView!
    @IBOutlet weak var labelUU: UILabel!
    @IBOutlet weak var labelU: UILabel!
    @IBOutlet weak var labelC: UILabel!
    @IBOutlet weak var labelD: UILabel!
    @IBOutlet weak var labelDD: UILabel!
    
    
    @IBOutlet weak var viewRecall: UIView!
    @IBOutlet weak var labelLeft: UILabel!
    
    @IBOutlet weak var labelResult: UILabel!
    @IBOutlet var textHid: UITextField!
    
    let maskChar: Character = "•"
    
    var mode: RecentMemoryMode = .Init
    var magicNumber = 0
    var magicString: String {
        get {
            return String(magicNumber)
        }
    }
    var rememberTimer: NSTimer!
    var equStart: CFTimeInterval = 0
    var equTime: Double = 0
    var signs: [EquationOperand] = [.Plus, .Minus, .Mul]
    var equations: [[Int]]!
    var currentEq = 0
    var calcVal: Int?
    var recalled = 0
    var speech: AVSpeechSynthesizer = AVSpeechSynthesizer()
    
    func reset(){
        viewEquations.hidden = true
        viewRecall.hidden = true
        viewRemember.hidden = true
        labelResult.hidden = true
        labelResult.alpha = 1
        labelResult.textColor = UIColor.whiteColor()
        magicNumber = 0
        calcVal = nil
        rememberTimer?.invalidate()
    }
    
    override func runTrial() {
        super.runTrial()
        reset()
        
        magicNumber = Int(FMUtility.random(100000...999999))
        labelRemember.text = magicString
        viewRemember.hidden = false
        
        if FMSettings.soundMode == FMSettings.SoundMode.Enabled {
            var expandedString = ""
            for char in magicString.characters {
                expandedString.append(char)
                expandedString.append("." as Character)
                expandedString.append(" " as Character)
            }
            
            let speechPrefix: AVSpeechUtterance = AVSpeechUtterance(string: " ")
            speech.speakUtterance(speechPrefix)
            
            let speaking: AVSpeechUtterance = AVSpeechUtterance(string: expandedString)
            speaking.rate = AVSpeechUtteranceMinimumSpeechRate
            speech.speakUtterance(speaking)
        }
            
        rememberTimer = NSTimer.scheduledTimerWithTimeInterval(6.0, target: self, selector: Selector("gotoEquations"), userInfo: nil, repeats: false)
    }
    
    @IBAction func btnRemembered(sender: AnyObject) {
        rememberTimer?.invalidate()
        gotoEquations()
    }
    
    
    func gotoEquations(){
        if !trialRunning {
            return
        }
        speech.stopSpeakingAtBoundary(AVSpeechBoundary.Immediate)
        mode = .Calc
        viewRemember.hidden = true
        viewEquations.alpha = 1
        viewEquations.hidden = false
        
        setupEquations()
        
        equTime = 0
        textHid.text = ""
        textHid.becomeFirstResponder()
        equStart = CFAbsoluteTimeGetCurrent()
    }
    
    func setupEquations(){
        signs.sortInPlace {
            (_,_) in arc4random() < arc4random()
        }
        equations = []
        var a = 0, b = 0
        for sign in signs {
            switch sign {
            case .Plus:
                a = Int(FMUtility.random(2...20))
                b = Int(FMUtility.random(2...20))
                equations.append([max(a, b), min(a, b), a + b, sign.rawValue])
            case .Minus:
                a = Int(FMUtility.random(2...20))
                b = Int(FMUtility.random(2...20))
                equations.append([max(a, b), min(a, b), abs(a - b), sign.rawValue])
            case .Mul:
                a = Int(FMUtility.random(2...5))
                b = Int(FMUtility.random(2...5))
                equations.append([a, b, a * b, sign.rawValue])
            }
        }
        
        currentEq = 0
        updateCalc()
    }
    
    func formatEqu(index: Int) -> String {
        var eqVal: String = ""
        if index < 0 || index >= equations.count {
            return eqVal
        }
        if index < currentEq {
            eqVal = String(equations[index][2])
        } else if index == currentEq {
            eqVal = calcVal != nil ? String(calcVal!) : "?"
        } else {
            eqVal = "?"
        }
        return String(format: "%d %@ %d = %@", equations[index][0], EquationOperand(rawValue: equations[index][3])!.toString(), equations[index][1], eqVal)
    }
    
    func updateCalc(){
        labelUU.text = formatEqu(currentEq - 2)
        labelU.text = formatEqu(currentEq - 1)
        labelC.text = formatEqu(currentEq)
        labelD.text = formatEqu(currentEq + 1)
        labelDD.text = formatEqu(currentEq + 2)
    }
    
    func checkCalc(){
        let timeHit = CFAbsoluteTimeGetCurrent()
        let text = textHid.text!
        if text.characters.count > 2 {
            textHid.text = text.substringToIndex(text.startIndex.advancedBy(1))
        }
        
        calcVal = Int(textHid.text!)
        updateCalc()
        if calcVal != nil && calcVal == equations[currentEq][2] {
            textHid.text = ""
            currentEq++
            calcVal = nil
            if currentEq == equations.count {
                equTime = timeHit - equStart
                UIView.animateWithDuration(0.5, animations: {
                        self.viewEquations.alpha = 0.5
                    }, completion: {
                        (Bool) -> Void in
                        self.gotoRecall()
                    }
                )
                return
            } else {
                updateCalc()
            }
        }
    }
    
    func gotoRecall(){
        if !trialRunning {
            return
        }
        mode = .Recall
        viewEquations.hidden = true
        viewRecall.hidden = false
        labelResult.hidden = false
        checkRecall()
        
        textHid.text = ""
        textHid.becomeFirstResponder()
    }
    
    func checkRecall(){
        let inputed = textHid.text!
        let remain = 6 - inputed.characters.count
        labelLeft.text = "\(remain) left"
        labelResult.text = inputed + String(count: remain, repeatedValue: maskChar)
        if remain == 0 {
            for i in 0..<magicString.characters.count {
                if magicString[i] as Character == inputed[i] as Character {
                    recalled++
                }
            }
            finishTrial()
        }
    }
    
    override func finishTrial() {
        textHid.resignFirstResponder()
        let inputed = labelResult.text!
        if inputed != magicString {
            // if not corect - detailed check char by char
            let attrOk = [NSForegroundColorAttributeName: UIColor.whiteColor()]
            let attrFail = [NSForegroundColorAttributeName: UIColor.redColor()]
            let result: NSMutableAttributedString = NSMutableAttributedString()
            for i in 0..<inputed.characters.count {
                let equal = inputed[i] as Character == magicString[i] as Character
                result.appendAttributedString(NSAttributedString(string: inputed[i], attributes: equal ? attrOk : attrFail))
            }
            labelResult.attributedText = result
        }
        viewRecall.hidden = true
        
        trialResults = [Double(recalled), equTime]
        super.finishTrial()
    }
    
    @IBAction func textInputed(sender: AnyObject) {
        if mode == .Calc {
            checkCalc()
        } else if mode == .Recall {
            checkRecall()
        }
    }
    
    override func stopTrial() {
        super.stopTrial()
        speech.stopSpeakingAtBoundary(AVSpeechBoundary.Immediate)
    }
    
}

enum RecentMemoryMode {
    case Init, Calc, Recall, Finish
}

enum EquationOperand: Int {
    case Plus = 1, Minus, Mul
    
    func toString() -> String {
        switch self {
        case .Plus:
            return "+"
        case .Minus:
            return "-"
        case .Mul:
            return "x"
        }
    }
}