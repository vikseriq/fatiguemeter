//
//  TrialBaseViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 09.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit
import AVFoundation

class TrialBaseViewController: UIViewController {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    var trialType: TrialType!
    var trialRunning: Bool = false
    var needCountdown: Bool = true
    var needSoundableClick: Bool = false
    var finishHandler: FMTrialFinishDelegate?
    var trialResults: [Double] = []
    var curbCounter: Int = 0
    var soundMode = FMSettings.soundMode
    
    func beginTrial(){
        trialRunning = true
        if needCountdown {
            showCountdown()
        } else {
            runTrial()
        }
        
        if needSoundableClick && soundMode != .Enabled {
            needSoundableClick = false
        }
    }
    
    func finishTrial(){
        trialRunning = false
        showSuccess()
    }
    
    func stopTrial(){
        trialRunning = false
        finishHandler = nil
        toggleSubviews(false)
    }
    
    func runTrial() {
        trialRunning = true
        toggleSubviews(true)
    }
    
    private let countdownTag = 0x010
    
    func showCountdown(){
        toggleSubviews(false)
        var lbls: [UILabel] = [
            makeBigLabel("3", size: 120),
            makeBigLabel("2", size: 120),
            makeBigLabel("1", size: 120),
            makeBigLabel("Go!", size: 120)
        ]
        var j: Double = 0
        let k: CGFloat = 1/8
        for i in 0..<lbls.count {
            lbls[i].transform = CGAffineTransformScale(lbls[i].transform, 2.5, 2.5)
            lbls[i].alpha = 0
            lbls[i].tag = countdownTag
            self.view.addSubview(lbls[i])
            
            UIView.animateWithDuration(1.0, delay: j, options: [], animations: {
                lbls[i].alpha = 1
                lbls[i].transform = CGAffineTransformScale(lbls[i].transform, k, k)
                
                }, completion: {
                    (finished: Bool) -> Void in
                    UIView.animateWithDuration(0.5, animations: {
                        lbls[i].alpha = 0
                    }, completion: nil)
                }
            )

            j += 0.5
        }
        NSTimer.scheduledTimerWithTimeInterval(2.75, target: self, selector: Selector("runTrial"), userInfo: nil, repeats: false)
    }
    
    func curbTouches() -> Double {
        curbCounter++
        if curbCounter % 10 == 0 {
            let lblWarning = makeBigLabel("Please stop\ntouching\nthe screen", size: 42)
            lblWarning.alpha = 0
            self.view.addSubview(lblWarning)
            
            UIView.animateWithDuration(1.0, animations: { () -> Void in
                lblWarning.alpha = 1
            }, completion: { (Bool) -> Void in
                UIView.animateWithDuration(1.0, animations: { () -> Void in
                    lblWarning.alpha = 0
                    }, completion: { (Bool) -> Void in
                        lblWarning.removeFromSuperview()
                })
            })
            
            return 3.0
        }
        return 0.0
    }
    
    func clickSound(){
        if needSoundableClick {
            AudioServicesPlaySystemSound(1104)
        }
    }
    
    private func toggleSubviews(visible: Bool = true){
        var i: Int = 0
        while i < self.view.subviews.count {
            let v = self.view.subviews[i]
            v.alpha = visible ? 1 : 0
            if v.tag == countdownTag {
                v.removeFromSuperview()
                i--
            }
            i++
        }
    }
    
    private func makeBigLabel(title: String, size: CGFloat = 40) -> UILabel {
        let lbl: UILabel = UILabel(frame: CGRectMake(0, 0, self.view.bounds.width, self.view.bounds.height))
        lbl.text = title
        lbl.numberOfLines = 0
        lbl.textColor = UIColor.whiteColor()
        lbl.backgroundColor = UIColor.clearColor()
        lbl.font = UIFont(name: "HelveticaNeue-Thin", size: size)
        lbl.textAlignment = NSTextAlignment.Center
        return lbl
    }
    
    func showSuccess(){
        let lbl = makeBigLabel("Success!")
        lbl.alpha = 0
        self.view.addSubview(lbl)
        UIView.animateWithDuration(0.75, animations: {
            lbl.alpha = 1
            }
        )
        if finishHandler != nil {
            NSTimer.scheduledTimerWithTimeInterval(2.0, target: self, selector: "trialCompleted", userInfo: nil, repeats: false)
        }
    }
    
    func trialCompleted(){
        if finishHandler != nil {
            finishHandler?.trialFinished()
        }
    }
    
}
