//
//  ProfileSetupViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 10.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class ProfileSetupViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate, FMUIPickerStateDelegate {

    let imperial = FMSettings.units == FMSettings.MeasureUnits.Imperial
    let profileKeys = ["gender", "age", "height", "weight"]
    let notSelected = "Not selected"
    var titles: [String] = []
    var values: [[String]] = []
    var picker: FMUIPickerView!
    var pickerTouched: Bool = false
    var keepResponder: Bool = false
    var currentRow: Int = 0
    var tablePath: NSIndexPath?
    var scrollTop: CGFloat = 0
    var profile: ProfileModel!
    var weightHK: Double = 0
    var heightHK: Double = 0
    
    @IBOutlet weak var tableParams: UITableView!
    @IBOutlet weak var textHid: UITextField!
    @IBOutlet weak var actionButton: FMUIRoundButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        picker = FMUIPickerView()
        picker.delegate = self
        picker.stateDelegate = self
        picker.assignToTextField(textHid)
        
        tableParams.estimatedRowHeight = 44
        tableParams.rowHeight = 44
        
        // populating table values & row titles
        values = [["Male", "Female"]]
        titles.append("Gender")
        
        var ar: [String] = []
        var j, j_max: Double
        
        for i in ProfileModel.rangeAges[0]...ProfileModel.rangeAges[1] {
            ar.append("\(i)")
        }
        values.append(ar)
        titles.append("Age")
        
        if imperial {
            // height in feet
            ar = []
            j = ProfileModel.rangeHeight.first!
            j_max = ProfileModel.rangeHeight.last!
            while j <= j_max {
                ar.append(String(format: "%.0f' %.0f\"", floor(j), floor(j % 1 * 12)))
                j += 1 / 12
            }
            values.append(ar)
            titles.append("Height, ft")
            // weight in pounds
            ar = []
            j = ProfileModel.rangeWeight.first!
            j_max = ProfileModel.rangeWeight.last!
            while j <= j_max {
                ar.append(String(format: "%.0f", j))
                j++
            }
            ar.append("Oh, really?")
            values.append(ar)
            titles.append("Weight, lbs")
        } else {
            // height in meters
            ar = []
            j = ProfileModel.rangeHeight.first! * ProfileModel.ft2m
            j_max = ProfileModel.rangeHeight.last! * ProfileModel.ft2m
            while j <= j_max {
                ar.append(String(format: "%.2f", j))
                j += 0.01
            }
            values.append(ar)
            titles.append("Height, m")
            // weight in kg
            ar = []
            j = round(ProfileModel.rangeWeight.first! * ProfileModel.lbs2kg * 2) / 2
            j_max = round(ProfileModel.rangeWeight.last! * ProfileModel.lbs2kg * 2) / 2
            while j <= j_max {
                ar.append(String(format: "%.1f", j))
                j += 0.5
            }
            ar.append("Oh, really?")
            values.append(ar)
            titles.append("Weight, kg")
        }
        
        profile = ProfileModel()
    }
    
    override func viewWillAppear(animated: Bool) {
        switch FMStateful.now(){
        case .Profile:
            actionButton.setTitle("Save my profile", forState: UIControlState.Normal)
        case .ProfileSetup:
            actionButton.setTitle("Run test", forState: UIControlState.Normal)
        default:
            break
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        if FMStateful.now() == .ProfileSetup {
            if let nc = self.navigationController {
                NaviViewController.setTitle(nc, title: "Profile setup")
            }
        }
        
        if #available(iOS 8.0, *) {
            FMHealthManager.sharedInstance?.authorize({ (error) -> Void in
                if error.characters.count > 0 {
                    return
                } else {
                    if let profileInfo = FMHealthManager.sharedInstance?.readProfileData(){
                        if profileInfo.age >= ProfileModel.rangeAges.first {
                            self.profile?.age = profileInfo.age
                        }
                        self.profile?.gender = profileInfo.sex == 2 ? 1 : 0
                        self.saveFromHK()
                    }
                    FMHealthManager.sharedInstance?.readWeight({ (weight) -> Void in
                        if let w_kg = weight {
                            let w = w_kg / ProfileModel.lbs2kg
                            if w > ProfileModel.rangeWeight.last || w < ProfileModel.rangeWeight.first {
                                return
                            }
                            self.weightHK = w
                            self.profile?.weight = self.weightHK
                            self.saveFromHK()
                        }
                    })
                    FMHealthManager.sharedInstance?.readHeight({ (height) -> Void in
                        if let h_m = height {
                            let h = h_m / ProfileModel.ft2m
                            if h > ProfileModel.rangeHeight.last || h < ProfileModel.rangeHeight.first {
                                return
                            }
                            self.heightHK = h
                            self.profile?.height = self.heightHK
                            self.saveFromHK()
                        }
                    })
                }
            })
        } else {
            
        }
        checkIsFilled()
    }
    
    func askForHK(){
        if #available(iOS 8.0, *) {
            let alertController = UIAlertController(title: "Health data", message: "FatigueMeter can read your actual body parameters right from Health app – just allow access.", preferredStyle: .Alert)
            alertController.addAction(UIAlertAction(title: "Open Health", style: .Default, handler: { action in
                Flurry.logEvent("accessAlert", withParameters: ["provider": "health", "desidion": "granted"])
            }))
            alertController.addAction(UIAlertAction(title: "Later", style: .Cancel, handler: { action in
                Flurry.logEvent("accessAlert", withParameters: ["provider": "health", "desidion": "later"])
            }))
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    func saveFromHK(){
        NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
            self.profile?.save()
            self.refreshTable()
            self.checkIsFilled()
        })
    }
    
    func checkIsFilled(){
        if profile == nil
            || profile.age == 0
            || profile.height == 0
            || profile.weight == 0
        {
            actionButton.enabled = false
        } else {
            actionButton.enabled = true
        }
    }
    
    func refreshTable(){
        tableParams.beginUpdates()
        tableParams.reloadData()
        tableParams.endUpdates()
        tableParams.setNeedsDisplay()
    }
    
    @IBAction func fireAction(sender: AnyObject) {
        // check and save w/h to HK
        if #available(iOS 8.0, *) {
            if let healthManager = FMHealthManager.sharedInstance {
                if weightHK != profile.weight {
                    healthManager.writeWeight(profile.weight)
                }
                if heightHK != profile.height {
                    healthManager.writeHeight(profile.height)
                }
            }
        }
        
        // go out
        switch FMStateful.now(){
        case .Profile:
            MenuViewController.unwrap(self)!.performStateChange(.Settings)
        case .ProfileSetup:
            // app loaded & profile not filled in yet
            Flurry.logEvent("profileCompleted")
            MenuViewController.unwrap(self)!.performStateChange(.TrialGo)
        default:
            // test cancelled on first run
            MenuViewController.unwrap(self)!.performStateChange(.Home)
            break
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("profileCell") as! ProfileSetupCell
        
        cell.labelKey.text = titles[indexPath.row]
        var val = profile.stringForKey(profileKeys[indexPath.row])
        if val.characters.count == 0 || val[0] == "0" {
            val = notSelected
        }
        cell.labelValue.text = val
        cell.labelValue.highlighted = val == notSelected
        cell.highlighted = indexPath == tablePath
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // when cell changed and previous picker-selection not ended - trigger to finish
        if indexPath != tablePath && textHid.isFirstResponder() && pickerTouched {
            pickerTouched = false
            keepResponder = true
            pickerView(picker, dismissWithValue: true)
            tableView.selectRowAtIndexPath(indexPath, animated: true, scrollPosition: UITableViewScrollPosition.None)
        }
        // update path pointers
        currentRow = indexPath.row
        tablePath = indexPath
        
        picker.reloadAllComponents()
        // find selected value in values array or select central value
        let pos: Int? = values[currentRow].indexOf(profile.stringForKey(profileKeys[currentRow]))
        let pickerRow = pos != nil ? pos! : Int(values[currentRow].count / 3)
        picker.selectRow(pickerRow, inComponent: 0, animated: false)
        
        // calculating offset
        let cellRect = tableView.rectForRowAtIndexPath(indexPath)
        let cellsHeight = cellRect.height * CGFloat(tableView.frame.height / view.frame.height < 0.4 ? 2 : 1)
        scrollTop = tableView.frame.origin.y + cellRect.minY - cellsHeight
        if scrollTop <= 0 {
            scrollTop = 0
        }
        // show picker-keyboard and scroll up view
        textHid.becomeFirstResponder()
        scrollView.setContentOffset(CGPointMake(0, scrollTop), animated: true)
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return values[currentRow].count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return values[currentRow][row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickerTouched = true
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(picker: FMUIPickerView, dismissWithValue: Bool) {
        if dismissWithValue {
            profile.setValue(values[currentRow][picker.selectedRowInComponent(0)], forKey: profileKeys[currentRow])
            profile.save()
        }
        if let path = tablePath {
            tableParams.deselectRowAtIndexPath(path, animated: false)
        }
        
        if keepResponder {
            keepResponder = false
        } else {
            textHid.resignFirstResponder()
            scrollView.setContentOffset(CGPointMake(0, 0), animated: true)
        }
        refreshTable()
        checkIsFilled()
    }
    
}

class ProfileSetupCell: UITableViewCell {
    
    @IBOutlet weak var labelKey: UILabel!
    @IBOutlet weak var labelValue: UILabel!

    
}
