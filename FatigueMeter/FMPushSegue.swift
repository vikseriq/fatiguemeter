//
//  FMPushSegue.swift
//  FatigueMeter
//
//  Created by vikseriq on 18.08.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class FMPushSegue: UIStoryboardSegue {

    override func perform() {
        self.sourceViewController.navigationController!.pushViewController(self.destinationViewController as UIViewController, animated: true)
    }
    
}
