//
//  FMUICalendarView.swift
//  FatigueMeter
//
//  Created by vikseriq on 18.05.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class FMUICalendarView: UIView {
    
    let colorPrimary = UIColor(rgba: "#ffffff80")
    let colorActive = UIColor(rgba: "#ffffffdd")
    let colorWhite = UIColor.whiteColor()
    let colorDisabled = UIColor(rgba: "#CDCDD0")
    let calendarFlags: NSCalendarUnit = [.Year, .Month, .Day, .WeekdayOrdinal]
    let calendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
    let now = NSDate()
    let gapX: CGFloat = 15
    
    var delegate: FMUICalendarViewDelegate!
    var dataSource: FMUICalendarViewDataSource!
    var reportMonth = NSDate()
    var formatter = NSDateFormatter()
    var globalY: CGFloat = 0
    var viewGrid: UIView!
    var labelYear: UILabel!
    var buttonNext: UIButton!
    var buttonPrev: UIButton!
    
    func setupView(){
        globalY = 0
        
        // Year and month title
        labelYear = UILabel(frame: CGRectMake(0, globalY, frame.width, 54))
        labelYear.font = UIFont.boldSystemFontOfSize(16)
        labelYear.textColor = colorWhite
        labelYear.textAlignment = .Center
        addSubview(labelYear)
        
        // Month switches
        let buttonHeight: CGFloat = 54
        let buttonWidth: CGFloat = frame.width * 0.16
        let buttonFont = UIFont.systemFontOfSize(16)
        
        buttonPrev = UIButton(frame: CGRectMake(gapX, globalY, buttonWidth, buttonHeight))
        buttonPrev.titleLabel?.font = buttonFont
        buttonPrev.setTitleColor(colorActive, forState: UIControlState.Normal)
        buttonPrev.setTitleColor(colorPrimary, forState: UIControlState.Highlighted)
        buttonPrev.addTarget(self, action: Selector("prevMonth"), forControlEvents: UIControlEvents.TouchUpInside)
        addSubview(buttonPrev)
        
        buttonNext = UIButton(frame: CGRectMake(frame.width - 2 * gapX - buttonWidth, globalY, buttonWidth, buttonHeight))
        buttonNext.titleLabel?.font = buttonFont
        buttonNext.setTitleColor(colorActive, forState: UIControlState.Normal)
        buttonNext.setTitleColor(colorPrimary, forState: UIControlState.Highlighted)
        buttonNext.addTarget(self, action: Selector("nextMonth"), forControlEvents: UIControlEvents.TouchUpInside)
        addSubview(buttonNext)
        
        globalY = globalY + buttonHeight
        
        // Weekdays
        let weekdayNames = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
        let weekdayFont = FMUtility.systemFont(12)
        formatter.locale = FMUtility.locale
        let firstWeekday = calendar.firstWeekday - 1
        
        let viewWeekdaysRow = UIView(frame: CGRectMake(gapX, globalY, frame.width - gapX * 2, 25))
        viewWeekdaysRow.backgroundColor = UIColor(rgba: "#FFFFFF30")
        
        let wdWidth = viewWeekdaysRow.frame.width / CGFloat(weekdayNames.count)
        for i in 0..<weekdayNames.count {
            let labelWd = UILabel(frame: CGRectMake(CGFloat(i) * wdWidth, 0, wdWidth, viewWeekdaysRow.frame.height))
            labelWd.font = weekdayFont
            labelWd.textColor = colorWhite
            labelWd.textAlignment = NSTextAlignment.Center
            labelWd.text = weekdayNames[(i + firstWeekday) % weekdayNames.count]
            labelWd.bounds.offsetInPlace(dx: labelWd.frame.width, dy: 0)
            viewWeekdaysRow.addSubview(labelWd)
        }
        viewWeekdaysRow.layer.cornerRadius = 4
        addSubview(viewWeekdaysRow)
        globalY = globalY + viewWeekdaysRow.bounds.height
        
        // very strange, that frame.height greater that screen height (buggy me?), so detect 4/4s here
        let cellHeight: CGFloat = UIScreen.mainScreen().bounds.height > 480 ? 64 : 54
        
        viewGrid = UIView(frame: CGRectMake(gapX, globalY, frame.width - gapX * 2, 6 * cellHeight))
        
        backgroundColor = UIColor.clearColor()
    }
    
    func showMonth(month: NSDate, offsetMonth: Int){
        let monthComponents = calendar.components(calendarFlags, fromDate: month)
        monthComponents.month += offsetMonth
        reportMonth = calendar.dateFromComponents(monthComponents)!
        var activeDays: [Int] = []
        
        if dataSource != nil {
            activeDays = dataSource.calendarView(self, activeDaysForMonth: reportMonth)
            var dates = dataSource.calendarView(self, dateRange: reportMonth)
            dates.till = dates.till.laterDate(now)
            dates.from = dates.from.earlierDate(now)
            var mc = calendar.components(calendarFlags, fromDate: dates.from)
            buttonPrev.hidden = mc.year == monthComponents.year && mc.month == monthComponents.month
            mc = calendar.components(calendarFlags, fromDate: dates.till)
            buttonNext.hidden = mc.year == monthComponents.year && mc.month == monthComponents.month
        }
        
        formatter.dateFormat = "MMMM yyyy"
        labelYear.text = formatter.stringFromDate(reportMonth)
        formatter.dateFormat = "MMM"
        
        monthComponents.month--
        buttonPrev.setTitle(formatter.stringFromDate(calendar.dateFromComponents(monthComponents)!), forState: UIControlState.Normal)
        
        monthComponents.month += 2
        buttonNext.setTitle(formatter.stringFromDate(calendar.dateFromComponents(monthComponents)!), forState: UIControlState.Normal)
        
        showMonthGrid(viewGrid, month: reportMonth, selectedDays: activeDays)
        addSubview(viewGrid)
        
        setNeedsDisplay()
    }
    
    func createMonthGrid(view: UIView){
        // Days grid
        let rows = 6
        let cols = 7
        let dayWidth = view.frame.width / CGFloat(cols)
        let dayHeight = view.frame.height / CGFloat(rows)
        
        for row in 0..<rows {
            for col in 0..<cols {
                let label = FMUICalendarCellView(frame: CGRectMake(CGFloat(col) * dayWidth, CGFloat(row) * dayHeight, dayWidth, dayHeight))
                label.hidden = true
                label.tag = row * cols + col
                label.addTarget(self, action: Selector("onDaySelected:"), forControlEvents: UIControlEvents.TouchUpInside)
                view.addSubview(label)
            }
        }
    }
    
    func showMonthGrid(view: UIView, month: NSDate, selectedDays: [Int]){
        // Create month grid
        if view.subviews.count == 0 {
            createMonthGrid(view)
        }
        
        let curDay = calendar.components(calendarFlags, fromDate: now)
        // selected month components
        let viewDay = calendar.components(calendarFlags, fromDate: month)
        // first day of month
        viewDay.day = 1
        let monthStart = calendar.dateFromComponents(viewDay)
        // get weekday number of 1st
        let dayOfWeek =  calendar.components(NSCalendarUnit.Weekday, fromDate: monthStart!).weekday
        // total days
        let daysRange = calendar.rangeOfUnit(NSCalendarUnit.Day, inUnit: NSCalendarUnit.Month, forDate: month)
        
        let startTag = dayOfWeek - 1
        let endTag = daysRange.length
        
        for subview in view.subviews {
            guard let label = subview as? FMUICalendarCellView else {
                continue
            }
            // hide overranged cells
            if label.tag < startTag || label.tag > endTag {
                label.hidden = true
                continue
            }
            // fill cell
            let day = label.tag - startTag + 1
            label.isCurrent = (curDay.year == viewDay.year && curDay.month == viewDay.month && curDay.day == day)
            label.isActive = false
            for i in selectedDays {
                if i == day {
                    label.isActive = true
                    break
                }
            }
            label.text = "\(day)"
            label.hidden = false
        }
    }
    
    func nextMonth(){
        showMonth(reportMonth, offsetMonth: 1)
    }
    
    func prevMonth(){
        showMonth(reportMonth, offsetMonth: -1)
    }
    
    func onDaySelected(sender: FMUICalendarCellView){
        if !sender.isActive {
            return
        }
        let day = Int(sender.text) ?? 1
        let dateComp = calendar.components(calendarFlags, fromDate: reportMonth)
        dateComp.day = day
        if let d = delegate {
            d.calendarView(self, didSelectDate: calendar.dateFromComponents(dateComp)!)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func didMoveToSuperview() {
        setupView()
    }

}

protocol FMUICalendarViewDelegate {
    func calendarView(calendarView: FMUICalendarView, didSelectDate date: NSDate)
}

protocol FMUICalendarViewDataSource {
    func calendarView(calendarView: FMUICalendarView, activeDaysForMonth monthDate: NSDate) -> [Int]
    func calendarView(calendarView: FMUICalendarView, dateRange defaultDate: NSDate) -> (from: NSDate, till: NSDate)
}
