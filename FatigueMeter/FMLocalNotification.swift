//
//  FMLocalNotification.swift
//  FatigueMeter
//
//  Created by vikseriq on 08.05.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class FMLocalNotification: NSObject {
   
    static var launchedFromNotification: Bool = false
    static var permissionRequestCount = 0
    
    static func sheduleNext() -> Bool {
        requestPermissions()
        if !hasPermissions(){
            return false
        }
        
        var texts = [String]()
        let plistPath = NSBundle.mainBundle().pathForResource("Values", ofType: "plist")
        let pDict = NSDictionary(contentsOfFile: plistPath!)
        if let pTexts = pDict!.valueForKey("Notifications") as? Array<String> {
            for text in pTexts {
                texts.append(text)
            }
        }
        
        var dates: [Double] = []
        
        // calculate fire date
        let weekdays = FMSettings.alertWeekdays
        let timeFrom = Double(FMSettings.alertTimeFrom)
        let timeTo = Double(FMSettings.alertTimeTo)
        var count = FMSettings.alertCount
        
        // intervaling
        var deltaTime = (timeTo - timeFrom) / Double(count > 1 ? count - 1 : 1)
        if deltaTime < 60 {
            deltaTime = 60
            count = 1
        }
        
        let now = NSDate()
        let calendar = NSCalendar.currentCalendar()
        var date = NSDate().midnight().dateByAddingTimeInterval(timeFrom)
        
        if weekdays.count > 0 {
            // looking for matching weekdays
            // loop all 7 days
            for _ in 0..<7 {
                date = date.dateByAddingTimeInterval(24 * 3600)
                // generate date
                let components = calendar.components(NSCalendarUnit.Weekday, fromDate: date)
                let wday = components.weekday - 1
                if weekdays.indexOf(wday) != nil {
                    // setting up times
                    for i in 0..<count {
                        dates.append(date.timeIntervalSince1970 + Double(i) * deltaTime)
                    }
                }
            }
        } else {
            // current/next day planning, like an Alarm app
            var startTime = date.timeIntervalSince1970
            var _t: NSTimeInterval
            
            for _ in 0..<count {
                _t = startTime
                if _t <= now.timeIntervalSince1970 {
                    _t += (24 * 3600)
                }
                dates.append(_t)
                startTime = startTime + deltaTime
            }
        }
        
        // setup notifications
        for date in dates {
            let notification = UILocalNotification()
            notification.alertAction = nil
            notification.alertBody = texts[FMUtility.random(0..<texts.count)]
            notification.fireDate = NSDate(timeIntervalSince1970: date)
            notification.applicationIconBadgeNumber = 1
            if weekdays.count > 0 {
                notification.repeatInterval = NSCalendarUnit.Weekday
            }
            UIApplication.sharedApplication().scheduleLocalNotification(notification)
        
            if AppDelegate.debug {
                print("Sheduled to \(notification.fireDate), total \(UIApplication.sharedApplication().scheduledLocalNotifications!.count)")
            }
        }
        return true
    }
    
    static func requestPermissions(){
        FMLocalNotification.permissionRequestCount++
        
        if #available(iOS 8.0, *){
            if let settings = UIApplication.sharedApplication().currentUserNotificationSettings(){
                if settings.types.rawValue == 0 {
                    UIApplication.sharedApplication().registerUserNotificationSettings(UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil))
                }
            }
        }
    }
    
    static func hasPermissions() -> Bool {
        if #available(iOS 8.0, *) {
            // on iOS 8+ check is permissions granted
            if let settings = UIApplication.sharedApplication().currentUserNotificationSettings(){
                if settings.types.rawValue == 0 {
                    return false
                }
            }
        }
        return true
    }
    
    static func resetBadge(){
        if hasPermissions() {
            UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        }
    }
    
    static func removeAll(){
        resetBadge()
        UIApplication.sharedApplication().cancelAllLocalNotifications()
    }
    
    static func getNextFireDate() -> NSDate {
        let date = NSDate().timeIntervalSince1970
        let maxDate = NSDate(timeIntervalSinceNow: 3600 * 24 * 360).timeIntervalSince1970
        var minDate = maxDate
        if let scheduled = UIApplication.sharedApplication().scheduledLocalNotifications {
            for notification in scheduled {
                let fire = notification.fireDate!.timeIntervalSince1970
                if fire > date && fire < minDate {
                    minDate = fire
                }
            }
        }
        return NSDate(timeIntervalSince1970: minDate == maxDate ? 0 : minDate)
    }
    
}
