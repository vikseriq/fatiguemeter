//
//  AccountLoginViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 01.06.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class AccountLoginViewController: UIViewController {

    @IBOutlet weak var emailText: FMUIRowView!
    @IBOutlet weak var passwordText: FMUIRowView!
    @IBOutlet weak var loginButton: FMUIRoundButton!
    
    var processIndicator: FMUIProcessIndicator!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailText.addTarget(passwordText, action: Selector("becomeFirstResponder"), forControlEvents: UIControlEvents.EditingDidEndOnExit)
        passwordText.addTarget(self, action: Selector("resignFirstResponder"), forControlEvents: UIControlEvents.EditingDidEndOnExit)
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationItem.title = "FatigueMeter online"
        self.emailText.value = AccountModel.sharedInstance.login
    }
    
    override func viewDidAppear(animated: Bool) {
        processIndicator = FMUIProcessIndicator(view: self.view)
    }
    
    func showIndicator(){
        self.processIndicator!.show()
    }
    
    func dismissIndicator(){
        self.processIndicator!.dismiss()
    }

    @IBAction func laterFired(sender: AnyObject) {
        MenuViewController.unwrap(self)?.popController()
    }
    
    @IBAction func facebookFired(sender: AnyObject) {
        showIndicator()
        AccountModel.sharedInstance.fbLogin { (error) -> () in
            NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                self.dismissIndicator()
                
                if let e = error {
                    self.alertError(e)
                } else {
                    MenuViewController.unwrap(self)?.popToRootController()
                }
            })
            
        }
    }
    
    static func processOAuthLogin(params: [String: String]){
        if params.count < 2 {
            return
        }
        if let controller = FMStateful.getData() as? AccountLoginViewController {
            controller.showIndicator()
            AccountModel.sharedInstance.auth(params["login"] ?? "", oAuthToken: params["token"] ?? "", callback: { (error, values) -> () in
                controller.dismissIndicator()
                
                if let e = error {
                    controller.alertError(e)
                } else {
                    MenuViewController.unwrap(controller)?.popToRootController()
                }
            })
        }
    }
    
    @IBAction func loginFired(sender: AnyObject) {
        let login = emailText.getValue()
        if login.characters.count < 3 {
            emailText.shakeBox()
            return
        }
        let password = passwordText.getValue()
        if password.characters.count < 3 {
            passwordText.shakeBox()
            return
        }
        
        view.endEditing(true)
        showIndicator()
        loginButton.enabled = false
        
        AccountModel.sharedInstance.auth(login, password: password) { (error, values) -> () in
            NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                self.dismissIndicator()
                
                if let e = error {
                    self.loginButton.enabled = true
                    self.alertError(e)
                } else {
                    MenuViewController.unwrap(self)?.popToRootController()
                }
            })
        }
    }
    
    func alertError(message: String){
        let title = "Login failed"
        let button = "Ok"
        
        if #available(iOS 8.0, *) {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
            alertController.addAction(UIAlertAction(title: button, style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
        } else {
            let alertView = UIAlertView(title: title, message: message, delegate: nil, cancelButtonTitle: button)
            alertView.alertViewStyle = .Default
            alertView.show()
        }
    }
    
}
