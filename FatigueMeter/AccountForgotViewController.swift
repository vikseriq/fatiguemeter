//
//  AccountForgotViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 29.07.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class AccountForgotViewController: UIViewController {

    @IBOutlet weak var loginText: FMUIRowView!
    @IBOutlet weak var actionButton: FMUIRoundButton!
    var processIndicator: FMUIProcessIndicator!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginText.addTarget(self, action: "becomeFirstResponder", forControlEvents: UIControlEvents.EditingDidEndOnExit)
    }
    
    override func viewWillAppear(animated: Bool) {
        self.loginText.value = AccountModel.sharedInstance.login
    }
    
    override func viewDidAppear(animated: Bool) {
        processIndicator = FMUIProcessIndicator(view: self.view)
    }
    
    @IBAction func actionFired(sender: AnyObject) {
        self.becomeFirstResponder()
        let login = loginText.getValue()
        if login.characters.count < 3 {
            loginText.shakeBox()
            return
        }
        
        view.endEditing(true)
        processIndicator.show()
        
        AccountModel.sharedInstance.forgot(login, callback: { (error, values) -> () in
            NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                self.processIndicator?.dismiss()
                
                if let e = error {
                    self.showAlert(e, onButton: { () -> () in })
                } else {
                    self.showAlert("Message with reset instructions sent to your email", onButton: { () -> () in
                        MenuViewController.unwrap(self)?.popController()
                    })
                }
            })
        })
    }
    
    func showAlert(message: String, onButton: () -> ()){
        let title = "Password reset"
        let button = "Dismiss"
        
        if #available(iOS 8.0, *) {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
            alertController.addAction(UIAlertAction(title: button, style: UIAlertActionStyle.Default, handler: { action in
                onButton()
            }))
            self.presentViewController(alertController, animated: true, completion: nil)
        } else {
            let alertView = UIAlertView(title: title, message: message, delegate: nil, cancelButtonTitle: button)
            alertView.alertViewStyle = .Default
            alertView.show()
            onButton()
        }
    }
    
}
