//
//  AppDelegate.swift
//  FatigueMeter
//
//  Created by vikseriq on 01.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    #if DEBUG
    static let debug: Bool = true
    #else
    static let debug: Bool = false
    #endif
    static let apiPath: String = "http://ormanbay.com/app.php"
    static let iOS7 = floor(NSFoundationVersionNumber) <= floor(NSFoundationVersionNumber_iOS_7_1)

    static var widgetAction: Int = 0
    static let kWidgetActionNone = 0x00
    static let kWidgetActionRun = 0x01
    static var naviViewController: NaviViewController?
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        let defaultSettings = NSDictionary(contentsOfFile: NSBundle.mainBundle().pathForResource("Settings", ofType: "plist")!)
        NSUserDefaults.standardUserDefaults().registerDefaults(defaultSettings as! [String: AnyObject])
        
        FMStateful.setState(.New)
        
        if !AppDelegate.debug {
            Flurry.setDebugLogEnabled(false)
            Flurry.setSessionReportsOnPauseEnabled(true)
            Flurry.setCrashReportingEnabled(true)
            Flurry.startSession("R4C9T2NC9SHKSXRFGXJG")
        }
        
        if let _ = launchOptions?[UIApplicationLaunchOptionsLocalNotificationKey] as? UILocalNotification {
            FMLocalNotification.launchedFromNotification = true
        }
        
        FMUINavigationBar.applyAppearance()
        
        return true
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        var params: [String: String] = Dictionary()
        guard let components = NSURLComponents(URL: url, resolvingAgainstBaseURL: false) else {
            return true
        }
        if let parts = components.query?.componentsSeparatedByString("&") {
            for part in parts {
                var kv = part.componentsSeparatedByString("=")
                if kv.count > 1 {
                    params[kv[0]] = kv[1] ?? ""
                }
            }
        }
        
        if let _ = params["run"] {
            if FMStateful.now() != .TrialGo {
                AppDelegate.widgetAction = AppDelegate.kWidgetActionRun
                if let navi = AppDelegate.naviViewController {
                    navi.runTrial()
                }
            }
        }
        return true
    }
    
    func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {
        application.applicationIconBadgeNumber = 0
    }

    func applicationWillResignActive(application: UIApplication) {
        FMStateful.machine.background = true
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        FMStateful.machine.background = false
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "by.newsite.app.hola" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1] 
        }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("Fatigue", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
        }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("Fatigue.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "FAT_ERROR", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
        }
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext()
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
        }()
    
    func saveContext () {
        if self.managedObjectContext.hasChanges {
            do {
                try self.managedObjectContext.save()
            } catch {
                NSLog("Unresolved error \(error)")
            }
        }
    }
    
}

