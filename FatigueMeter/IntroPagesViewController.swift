//
//  IntroPagesViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 03.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class IntroPagesViewController: UIViewController {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBOutlet weak var pageControl: UIPageControl!
    var slides: [IntroPageViewController] = []
    @IBOutlet weak var slideView: UIView!
    var currentPage = 0
    var previousPage = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // gestures
        let swipeLeft: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: Selector("onSwipe:"))
        swipeLeft.direction = .Left
        let swipeRight: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: Selector("onSwipe:"))
        swipeRight.direction = .Right
        
        self.view.addGestureRecognizer(swipeLeft)
        self.view.addGestureRecognizer(swipeRight)
        
        if slides.count == 0 {
            let plistPath = NSBundle.mainBundle().pathForResource("Values", ofType: "plist")
            let pDict = NSDictionary(contentsOfFile: plistPath!)
            if let pIntro = pDict!.valueForKey("IntroPages") as? Array<AnyObject> {
                for i in 0..<pIntro.count {
                    let subpage = self.storyboard?.instantiateViewControllerWithIdentifier("IntroPageViewController") as! IntroPageViewController
                    let pageData = pIntro[Int(i)] as! Dictionary<String, String>
                    subpage.screenId = i
                    subpage.header = pageData["Title"]!
                    subpage.image = pageData["Image"]!
                    subpage.desc = pageData["Description"]!
                    subpage.gradientA = UIColor(rgba: pageData["ColorA"]!)
                    subpage.gradientB = UIColor(rgba: pageData["ColorB"]!)
                    
                    slides.append(subpage)
                }
            }
        }
        
        pageControl.numberOfPages = slides.count
        currentPage = -1
        previousPage = -1
    }
    
    override func viewWillAppear(animated: Bool) {
        showPage(0)
    }
    
    override func viewDidAppear(animated: Bool) {
        UIApplication.sharedApplication().setStatusBarHidden(true, withAnimation: UIStatusBarAnimation.Fade)
    }
    
    func close(viewed: Bool){
        UIApplication.sharedApplication().statusBarHidden = false
        FMSettings.skipIntro = true
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func closeByButton(sender: AnyObject) {
        close(false)
    }
    
    @IBAction func pageClick(sender: AnyObject) {
        showPage(pageControl.currentPage)
    }
    
    func onSwipe(sender: UISwipeGestureRecognizer){
        if (sender.direction == .Left){
            currentPage++
        } else if (sender.direction == .Right) {
            currentPage--
        }
        showPage(currentPage)
    }
    
    func showPage(index: Int){
        currentPage = index
        if currentPage < 0 {
            currentPage = 0
        }
        if currentPage >= slides.count {
            close(true)
            return
        }
        let finalFrame: CGRect = CGRectMake(0, 0, slideView.frame.width, slideView.frame.height)
        
        if (previousPage >= 0 && currentPage != previousPage){
            let k: CGFloat = currentPage < previousPage ? -1 : 1
            
            slides[currentPage].view.frame = CGRectMake(slideView.frame.width * k, 0,
                slideView.frame.width, slideView.frame.height)
            
            UIView.animateWithDuration(0.3, animations: {
                self.slides[self.currentPage].view.frame = finalFrame
            })
        } else {
            slides[self.currentPage].view.frame = finalFrame
        }
        
        if slides[currentPage].placed {
            slideView.bringSubviewToFront(slides[currentPage].view)
        } else {
            slideView.addSubview(slides[currentPage].view)
            slides[currentPage].placed = true
            slides[currentPage].didMoveToParentViewController(self)
            slideView.bringSubviewToFront(slides[currentPage].view)
        }
        
        previousPage = currentPage
        pageControl.currentPage = currentPage
    }
}
