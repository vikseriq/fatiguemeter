//
//  FMUIBadgedButton.swift
//  FatigueMeter
//
//  Created by vikseriq on 07.05.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class FMUIBadgedButton: UIButton {

    @IBInspectable var badgeNumber: Int = 0 {
        didSet {
            setupView()
        }
    }
    
    var badge: FMUIBadgeLabel!
    var chevron: UIImageView!
    
    func initView(){
        // badge
        let bw: CGFloat = 24
        let bx = bounds.width - contentEdgeInsets.right - bw
        let by = (bounds.height - bw) / 2.0
        badge = FMUIBadgeLabel(frame: CGRectMake(bx, by, bw, bw))
        badge.textColor = UIColor.whiteColor()
        self.addSubview(badge)
        // chevron
        let image = UIImage(named: "accessory_white")!
        chevron = UIImageView(image: image.tintWithColor(UIColor.whiteColor(), blendMode: CGBlendMode.Multiply))
        chevron.tintColor = FMUtility.colorActive
        chevron.contentMode = UIViewContentMode.ScaleAspectFit
        chevron.frame = CGRectMake(frame.width - contentEdgeInsets.right - 15, (frame.height - 15) / 2.0, 10, 15)
        chevron.hidden = true
        self.addSubview(chevron)
    }
    
    func setupView(){
        if badge == nil {
            initView()
        }
        self.imageView?.contentMode = UIViewContentMode.ScaleAspectFit
        if badgeNumber == 0 {
            badge.hidden = true
        } else if badgeNumber > 0 {
            badge.text = "\(badgeNumber)"
            badge.hidden = false
        } else {
            badge.hidden = true
            badge.removeFromSuperview()
            chevron.hidden = false
        }
        
        self.setNeedsDisplay()
    }

}
