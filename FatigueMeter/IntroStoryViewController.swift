//
//  IntroStoryViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 03.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit
import Foundation

class IntroStoryViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(animated: Bool) {
        FMStateful.setState(.Loading)
        
        if FMSettings.skipIntro {
            FMStateful.setState(.Load2Home)
            performSegueWithIdentifier("StoryShowHome", sender: self)
        } else {
            FMStateful.setState(.TourOnce)
            performSegueWithIdentifier("IntroPagesOpen", sender: self)
        }
    }

}
