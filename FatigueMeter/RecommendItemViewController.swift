//
//  RecommendItemViewController.swift
//  FatigueMeter
//
//  Created by vikseriq on 31.08.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class RecommendItemViewController: UIViewController {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var textContent: UITextView!
    
    var trialData: TrialSingleResult!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let data = FMStateful.getData() as? TrialSingleResult {
            trialData = data
        }
        
        if let data = trialData {
            let trialConfig = data.trialType.fetchConfig()
            let rc = RecommendModel.getByFatigue(data.fatigue)
            labelTitle.text = trialConfig.title
            let key = trialConfig.key
            let stub = "No special recommendations for " + key
            
            if let plistPath = NSBundle.mainBundle().pathForResource("short_" + rc.file, ofType: "plist"){
                if let pDict = NSDictionary(contentsOfFile: plistPath) as? Dictionary<String, String> {
                    let trialSet = pDict[key] ?? stub
                    textContent.text = trialSet
                }
                
                NaviViewController.showDisclosure(self, callback: { () -> Void in
                    FMStateful.setData(self.trialData.trialType.rawValue)
                    MenuViewController.unwrap(self)?.performStateChange(.HelpTrial)
                })
            } else {
                textContent.text = stub
            }
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        FMStateful.setState(.RecommendItem)
    }
    
}