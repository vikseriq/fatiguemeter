//
//  FMUIRadialGradientView.swift
//  FatigueMeter
//
//  Created by vikseriq on 04.05.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class FMUIRadialGradientView: UIView {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    func setupView(){
        let colors: CFArray = [UIColor.whiteColor().CGColor, UIColor.blackColor().CGColor]
        gradient = CGGradientCreateWithColors(CGColorSpaceCreateDeviceRGB(), colors, nil)
    }
    
    var gradient: CGGradient!
    @IBInspectable var startRadius: CGFloat = 1
    
    override func drawRect(rect: CGRect) {
        let radius: CGFloat = rect.width * 0.9
        let center = CGPoint(x: rect.midX, y: rect.midY)
        
        CGContextDrawRadialGradient(UIGraphicsGetCurrentContext(), gradient, center, startRadius, center, radius, CGGradientDrawingOptions.DrawsBeforeStartLocation)
    }


}
