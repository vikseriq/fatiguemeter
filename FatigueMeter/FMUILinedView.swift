//
//  FMUILinedView.swift
//  FatigueMeter
//
//  Created by vikseriq on 25.04.15.
//  Copyright (c) 2015-2016 vikseriq. All rights reserved.
//

import UIKit

class FMUILinedView: UIView {
    
    @IBInspectable var lineMode: Int = 0 {
        didSet {
            setupView()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupView(){
        let color = UIColor(rgba: "#909DA4").CGColor
        
        if lineMode == 0 {
            let mw: CGFloat = 1
            let gap: CGFloat = 10
            let layer = CALayer()
            layer.frame = CGRectMake((self.bounds.width - mw) / 2.0, gap, mw, self.bounds.height - gap)
            layer.backgroundColor = color
            self.layer.addSublayer(layer)
        } else if lineMode == 1 {
            let w: CGFloat = 1.0
            let top = CALayer()
            let bottom = CALayer()
            top.frame = CGRectMake(0, 0, self.bounds.width, w)
            bottom.frame = CGRectMake(0, self.bounds.height - w, self.bounds.width, w)
            top.backgroundColor = color
            bottom.backgroundColor = color
            self.layer.addSublayer(top)
            self.layer.addSublayer(bottom)
        } else {
            let w: CGFloat = 1.0
            let top = CALayer()
            top.frame = CGRectMake(0, w, self.bounds.width, w)
            top.backgroundColor = UIColor(rgba: "#D3D3D4").CGColor
            self.layer.addSublayer(top)
        }
    }

}
